<!DOCTYPE html>
<!--www.codingflicks.com--> 
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Transparent Login Form HTML CSS</title>
	<link href="style.css" rel="stylesheet">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="img/logo/logo.png" rel="icon">
    <title>RuangAdmin - Login</title>
    <!-- <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/ruang-admin.min.css" rel="stylesheet"> -->

    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/ruang-admin.min.css" rel="stylesheet">
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
    <style>
        e:hover {
            background-color:red;
        }
    </style>
</head>
<body>
    <!-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">BARAKA</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Link</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
            </ul>
        </div>
    </nav> -->
	<div class="form-box">
		<div class="header-text">
		    <p><h3  class="text-white">B A R A K A</h3></p>
		</div>
        <div class="form-group" style="margin-top:-20px">
            <input class="form-control" placeholder="Masukan Nomor VA" type="text" id="va"> 
        </div>
        <div class="form-group">
            <input class="form-control" placeholder="Tanggal Lahir Anda (DDMMYYYY)" type="password" id="tgl"> 
        </div>
        <div class="row">
            <div class="col col-md-12">
                <button class="btn btn-danger" id="btnone" onclick="loginOne(this)">Masuk Wisatawan</button>
            </div>
        </div>
        <div class="row" style="margin-top:-30px">
            <div class="col col-md-6">
                <a class="nav-link" href="https://api.whatsapp.com/send?phone=6281219721010">
                    <button style="background-color:green">Admin Layanan Tabungan</button>
                </a>
            </div>
            <div class="col col-md-6">
                <a class="nav-link" href="https://api.whatsapp.com/send?phone=6281219721122">
                    <button style="background-color:red">Admin Layanan Pelunasan</button>
                </a>
            </div>
        </div>
        <div class="row" style="margin-top:-10px;padding:10px">
            <div class="col d-flex justify-content-center">
                <div class="text-white float-center" onclick="getLogin()">Pilihan Akses</div>
            </div>
        </div>
        
	</div>
    <!-- modal -->
    <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Pilihan Login</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" role="tab" data-toggle="tab">Agent</a>
                </li>
                <!-- <li role="presentation"><a href="#contact" role="tab" data-toggle="tab">Admin Baraka</a>
                </li> -->
            </ul>

            <div class="container-fluid" >
                <div class="tab-content">
                    <div id="home" role="tabpanel" class="tab-pane fade in active">
                        <!-- <div class="alert alert-info">Agent</div> -->
                        <div class="form-group"  style="margin-top:20px">
                            <input id = "_id" name="id" required class="form-control" id="exampleInputEmail" aria-describedby="emailHelp"
                            placeholder="Masukan ID Agent">
                        </div>
                        <div class="form-group">
                            <input id = "_nama" type="text" required class="form-control" name="nama" aria-describedby="emailHelp"
                            placeholder="Masukan Nama">
                        </div>
                        <div class="form-group">
                            <input type="number" required class="form-control" id="_hp" placeholder="Masukan No.Hp">
                        </div>
                        <div class="form-group">
                              <input type="password" required class="form-control" id="_password" placeholder="Masukan Password">
                        </div>
                        <div class="form-group">
                              <button class="btn btn-primary" id="btnAgn" onclick="loginAgn(this)">Login</button>
                              <p class="float-right"><i class="fa fa-arrow-right text-primary" onclick="gotoAdmin()"></i></p>
                        </div>
                    </div>
                    <!-- <div id="contact" role="tabpanel" class="tab-pane fade">
                            <div class="row" style="margin-top:20px">
                                <div class="col">
                                    <div class="form-group">
                                        <input type="number" required class="form-control" id="hpAdm" placeholder="Masukan No.Hp">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" required class="form-control" id="pwdAdm" placeholder="Masukan Password">
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" id="btnadmin" onclick="loginAdm(this)">Login Admin</button>
                                    </div>
                                </div>
                            </div>
                    </div> -->
                </div>
            </div>
        </div>
        </div>
    </div>
    </div>


    <div class="modal fade" id="mdlAdm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Pilihan Login</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid" >
                <div class="row" style="margin-top:20px">
                    <div class="col">
                        <div class="form-group">
                            <input type="number" required class="form-control" id="hpAdm" placeholder="Masukan No.Hp">
                        </div>
                        <div class="form-group">
                            <input type="password" required class="form-control" id="pwdAdm" placeholder="Masukan Password">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" id="btnadmin2" onclick="loginAdm(this)">Login Admin</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    </div>

</body>
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="js/ruang-admin.min.js"></script>
<!-- Page level plugins -->
<script src="vendor/datatables/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
    function krik(v){
        console.log(v.replace("-",""));
    }
    function getLogin(){
        $(`#modalLogin`).modal('show')
        console.log('oi');
    }
    function gotoAdmin(){
        $(`#modalLogin`).modal('toggle')
        $(`#mdlAdm`).modal(`show`)
    }
    function loginAgn(v){
        let _id = $(`#_id`).val()
        let _nama = $(`#_nama`).val()
        let _hp = $(`#_hp`).val()
        let _password = $(`#_password`).val()
        // if(_id.length < 1 || _nama.length < 1 || _hp.length < 1 || _password.length < 1){
        //     return
        // }
        $(`#${v.id}`).html(`Mohon Tunggu...`)
        $.ajax({
            type:'POST',
            url:'login_be.php',
            data:{
                td : 1,
                id : _id,
                nama : _nama,
                hp : _hp,
                password : _password
            },
            success: function(data){
                $(`#${v.id}`).html(`Login`)
                console.log(data);
                if(data == "200"){
                    window.location = `index.php`
                }else{
                    console.log(data);
                    alert(`Gagal`)
                }
            }
        })

    }
    function loginAdm(v){
        let hpAdm = $(`#hpAdm`).val()
        let pwdAdm = $(`#pwdAdm`).val()
        if(hpAdm.length < 1 || pwdAdm.length < 1){
            return
        }
        $(`#${v.id}`).html(`Mohon Tunggu ...`)
        $.ajax({
            type:'POST',
            url:'login_be.php',
            data:{
                tp : 1,
                hpAdm : hpAdm,
                pwdAdm : pwdAdm
            },
            success: function(data){
                $(`#${v.id}`).html(`Login`)
                console.log(data);
                if(data == "500"){
                    alert(`Gagal`)
                }else{
                    window.location = `index.php`
                }
            }
        })
    }   
    function loginOne(v){
        let va = $(`#va`).val()
        let pwd = $(`#tgl`).val()
        if(va.length < 1 || pwd.length < 1){
            return
        }

        if(va.includes("-") || va.includes(" ")){
            alert(`Tidak boleh include spasi atau - `)
            return
        }

        $(`#${v.id}`).html(`Mohon Tunggu ...`)
        $.ajax({
            type:'POST',
            url:'cetakdirect.php',
            data:{
                tp : 1,
                va : va,
                tgl : pwd
            },
            success: function(data){
                $(`#${v.id}`).html(`Masuk Wisatawan`)
                console.log(data);
                if(data == "500"){
                    alert(`Gagal`)
                }else{
                    window.location = `index.php`
                }
            }
        })
    }
</script>
</html>
