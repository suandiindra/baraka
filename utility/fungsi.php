<?php
    function getCountTable($tbl,$con){
        $data = 0;
        $qr = "select count(*)+1 as cnt,uuid() as id from transaksi_point where m_agent_id = '$tbl' and is_claim <> 'X'";
        $res = mysqli_query($con,$qr);
        $dr = mysqli_fetch_array($res);
        $data = $dr;
        return $data;
    }
    function queryJson($con,$qr){
        error_reporting(0);
        ini_set('display_errors', 0);
        $res = mysqli_query($con,$qr);
        // echo $qr;
        if(mysqli_num_rows($res) > 0){
            while($da = mysqli_fetch_array($res)){
                $row [] = $da;
            }
            $obj = new stdClass();
            $obj -> data = $row;
            $data = json_encode($obj);
            return $data;
        }else{
            $obj = new stdClass();
            $obj -> data = [];
            $data = json_encode($obj);
            return $data;
        }
    }
    function transApprove($con){
        $data = 0;
        $qr = "select count(*)+1 as cnt,uuid() as id from transaksi_agent";
        $res = mysqli_query($con,$qr);
        $dr = mysqli_fetch_array($res);
        $data = $dr['cnt'];
        return sprintf("%05s", $data);
    }

    function transApprovePin($con){
        $data = 0;
        $qr = "select count(*)+1 as cnt,uuid() as id from transaksi_agent";
        $res = mysqli_query($con,$qr);
        $dr = mysqli_fetch_array($res);
        $data = $dr['cnt'];
        return sprintf("%05s", $data);
    }
    function notice($pesan){
        return "<script>alert('$pesan')</script>";
    }
    function uniq(){
        return (rand(1,1000000000000)) ;
    }

    function getcutoff($con){
        $nw = date("d");
        // $nw = date_create("2013-03-15");
        $sel = "select * from cut_off";
        $res = mysqli_query($con,$sel);
        $dt = mysqli_fetch_array($res);
        $tgl = $dt['periode']; //tgl 16
        $tgl2 = $dt['periode_awal']; //tgl 01
        $per = $dt['periode'];
        $per2 = $dt['periode_awal'];
        if($nw >= $tgl){
            $nxtm = strtotime("next month");
            $tgl = date("Y-m", $nxtm);
            $tgl = $tgl."-0".$per2;
        }else{
            $tgl = date("Y-m");
            $tgl = $tgl."-".$per;
        }
        return $tgl;
    }
    
    function ceknamabarang($con,$id){
        $sel = "select * from m_barang where m_barang_id = '$id'";
        $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        return $dt['nama_barang'];
    }

    function cekbarangpo($con,$qr){
        $sel = $qr;
        $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        return $dt['qty'];
    }
    function cekUrutanAgent($con){
        $sel = "select count(*)+1 as number from jemaah";
        $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        return "JB".sprintf("%04s", $dt['number']);
    }

    function cekUrutanResi($con){
        $sel = "select count(*)+1 as number from jemaah where nomor_resi like '%JB%'";
        $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        return "JB".sprintf("%08s", $dt['number']);
    }
    function cekUrutanResi2($con){
        $sel = "select count(*)+1 as number from jemaah where nomor_resi like '%RJM%'";
        $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        return "RJM".sprintf("%08s", $dt['number']);
    }
    function idwisatawan($con,$id,$nomor){
        $sel = "select count(*)+1 as number from jemaah where m_agent_id = '$id'";
        $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        $sel_agent = "select SUBSTR(nomor_agent,4,4) as key_ from m_agent where m_agent_id = '$id'";
        $dt1 = mysqli_fetch_array(mysqli_query($con,$sel_agent));

        return "JU".$nomor."-".sprintf("%04s", $dt['number']);
    }

    function cekdetailpo($con,$qr){
        
        $sel = "select count(*) as jml from t_po_detail where t_po_id = '$qr'";
        echo $sel;
        // $dt = mysqli_fetch_array(mysqli_query($con,$sel));
        // return $dt['jml'];
    }

    function cekstatusPO($con,$nopo){
        $sel0 = "select sum(cast(qty_gr as float))as qty from t_gr a
        inner join t_gr_detail b on a.t_gr_id = b.t_gr_id
        where t_po_id = '$nopo'";
        $res = mysqli_query($con,$sel0);
        $dt0 = mysqli_fetch_array($res);
        $qtyGR  = $dt0['qty'];


        $sel1 = "select sum(cast(qty as float)) as qty_po from t_po a
        inner join t_po_detail b on a.t_po_id = b.t_po_id
        where a.t_po_id = '$nopo'";
        $res = mysqli_query($con,$sel1);
        $dt1 = mysqli_fetch_array($res);
        $qtyPO  = $dt1['qty_po'];
        if((float) $qtyGR < (float) $qtyPO){
            $status = "OUTSTANDING";
        }else{
            $status = "COMPLETE";
        }

        return $status;
    }

    function querytodataset($con,$seq){
        $res = mysqli_query($con,$seq);
        $dt1 = mysqli_fetch_array($res);
        // echo $dt1['t_mutasi_id'];
        return $dt1;
    }
    function console($log){
        // $log = "ss";
        echo "<script>console.log('$log');</script>";
        // echo $log;
    }
    
    function cekSisa($con,$kode,$batch,$rak,$jml){
        $data = 0;
        $qr = "select last_stok - $jml as sisa from t_stok 
        where m_barang_id = '$kode' and batch = '$batch' and m_rak_id = '$rak'";
        $res = mysqli_query($con,$qr);
        $dr = mysqli_fetch_array($res);
        $data = $dr['sisa'];
        return $data;
    }

    function nomorGR($con,$tipe){
        $data = 0;
        $qr = "select count(*)+1 as jml from t_gr 
        where CONCAT(year(createdate),month(createdate)) = CONCAT(year(now()),month(now()))
        and tipe_gr = '$tipe'";
        $res = mysqli_query($con,$qr);
        $dr = mysqli_fetch_array($res);
        $count = $dr['jml'];

        $var_depan = "";
        if($tipe == "Purchase Order"){
            $var_depan = "WH-VCM";
        }elseif($tipe == "Gudang Sample"){
            $var_depan = "WH-SPL";
        }elseif($tipe == "Retur Customer"){
            $var_depan = "WH-RTRC";
        }else{
            $var_depan = "?";
        }
        $month = date('m');
        $year = date('yy');
        $month = romanNumber($month);
        // $month = date("F", mktime(0, 0, 0, $month, 10));
        $month = strtoupper(substr($month,0,3));

        $data = sprintf("%05d",$count)."/".$var_depan."/".$month."/".$year;
        

        return $data;
    }

    function romanNumber($numb){
        $no = 0;
        switch ($numb){
            case "01":
                $no = "I";
            break;
            case "02":
                $no = "II";
            break;
            case "03":
                $no = "III";
            break;
            case "04":
                $no = "IV";
            break;
            case "05":
                $no = "V";
            break;
            case "06":
                $no = "VI";
            break;
            case "07":
                $no = "VII";
            break;
            case "08":
                $no = "VIII";
            break;
            case "09":
                $no = "IX";
            break;
            case "10":
                $no = "X";
            break;
            case "11":
                $no = "XI";
            break;
            case "11":
                $no = "XII";
            break;
            default : 
            $no = "0";
            break;
        }
        return $no;
    }

    function format($v){
        $res = "";
        if (strpos($v,'.')){
            $res = number_format($v,2);
        }else{
            $res = $v;
        }

        return $res;
    }

?>