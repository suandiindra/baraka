<?php
  include('utility/config.php');
  include('utility/fungsi.php');
  session_start();
  if(empty($_SESSION['nomor_agent'])){
    echo "<script>window.location='login.php'</script>";
  }else{
    $_user = $_SESSION["m_agent_id"];
    $_nama = $_SESSION["nama"];
  }

  // $log = "select *,DATE_FORMAT(waktu,'%d-%M-%Y  %H:%i') as wt
  // from history_login where m_agent_id = '$_user'";

  $log = "select *,DATE_FORMAT(waktu,'%d-%M-%Y  %H:%i') as wt from 
  (
    select *,@rownum := @rownum + 1 AS rank from history_login
    ,(SELECT @rownum := 0) r
    where m_agent_id = '$_user'
    ORDER BY waktu desc
  )dd
  where rank = 2";
  // echo $log;
  $result = mysqli_query($con,$log);
  $datax = mysqli_fetch_array($result);

  $sel  = "update transaksi_point set kode_status = 'WT2',status = 'On Proses'  
  where cut_off = DATE_FORMAT(now(), '%Y-%m-%d') and kode_status = 'WT1'
  and is_claim = 'N'";
  $data = mysqli_query($con,$sel);
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">
  <title>RuangAdmin - DataTables</title>
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/ruang-admin.min.css" rel="stylesheet">
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <script>
    function startTime() {
      var today = new Date();
      var h = today.getHours();
      var m = today.getMinutes();
      var s = today.getSeconds();

      var day = today.getDay();
      m = checkTime(m);
      s = checkTime(s);

      const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
      ];

      
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = monthNames[today.getMonth() + 0]; //String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();

      var todayz = dd + '-' + mm + '-' + yyyy;

      document.getElementById('txt').innerHTML =
      todayz+ " "+ h + ":" + m ;
      var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
      if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
      return i;
    }
    </script>
</head>

<body onload="startTime()" id="page-top">
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="./">
        <div class="sidebar-brand-icon">
          <!-- <img src="img/logo/icons.png"> -->
          <!-- <img src="img/logo.jpg"> -->
        </div>
        <div class="sidebar-brand-text mx-3" style=""><?php echo $_SESSION['nomor_agent'] ?></div>
      </a>
      <hr class="sidebar-divider my-0">
      <li class="nav-item active">
        <a class="nav-link" href="./">
          <span>PT. Baraka Insan Mandiri</span></a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Features
      </div>
      <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBootstrap"
          aria-expanded="true" aria-controls="collapseBootstrap">
          <i class="far fa-fw fa-window-maximize"></i>
          <span>Bootstrap UI</span>
        </a>
        <div id="collapseBootstrap" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Bootstrap UI</h6>
            <a class="collapse-item" href="alerts.html">Alerts</a>
            <a class="collapse-item" href="buttons.html">Buttons</a>
            <a class="collapse-item" href="dropdowns.html">Dropdowns</a>
            <a class="collapse-item" href="modals.html">Modals</a>
            <a class="collapse-item" href="popovers.html">Popovers</a>
            <a class="collapse-item" href="progress-bar.html">Progress Bars</a>
          </div>
        </div>
      </li> -->
      <li class="nav-item">
        <?php
          $param = "";
          if($_SESSION['role'] == "Agent"){
            $id = $_SESSION['m_agent_id'];
            $param = "&id=$id";
          }

          if($_SESSION['role'] !== "jamaah"){
        ?>
        <a class="nav-link" href="./?go=agent<?php echo $param; ?>">
          <i class="fa fa-user-circle"></i>
          <span>Agent Profiles</span>
        </a>
      </li>
      <?php
          }
      // echo $_SESSION['role']." xxx";
        if($_SESSION['role'] == "1"){
          ?>
            <li class="nav-item">
              <a class="nav-link" href="./?go=listadmin<?php echo $param; ?>">
                  <i class="fa fa-user"></i>
                  <span>Admin Baraka</span>
                </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="./?go=listtabungan<?php echo $param; ?>">
                  <i class="fa fa-info"></i>
                  <span>Transaksi Tabungan</span>
                </a>
            </li>
          <?php
        }
      ?>
      
      <?php
          if($_SESSION['role'] !== "jamaah"){
      ?>

      <?php
        if($_SESSION['role'] == "Agent"){
      ?>
      <li class="nav-item">
        <a class="nav-link" href="./?go=newagent">
          <i class="fa fa-id-card"></i>
          <span>Input New Agent</span>
        </a>
      </li>
      <?php
        }
      ?>
      <li class="nav-item">
        <a class="nav-link" href="./?go=stok_pin">
          <i class="fa fa-bookmark" aria-hidden="true"></i>
          <span>Stock PIN</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./?go=pembelian">
          <i class="fa fa-shopping-cart" aria-hidden="true"></i>
          <span>Pembelian PIN</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./?go=wisatawan">
          <i class="fa fa-users" aria-hidden="true"></i>
          <span>Form Wisatawan</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./?go=point">
          <i class="fa fa-gift" aria-hidden="true"></i>
          <span>Point & Bonus</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./?go=info">
          <i class="fa fa-info" aria-hidden="true"></i>
          <span>&nbsp Update Informasi</span>
        </a>
      </li>
      <?php
        }

        if($_SESSION['role'] == "jamaah"){
      ?>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Pelayanan
      </div>
      
      <li class="nav-item">
        <a class="nav-link" id="admin_terpadu">
        <i class="fas fa-fw fa-window-maximize"></i>
          <span>Admin Terpadu</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="cancel_tabungan">
        <i class="fas fa-fw fa-window-maximize"></i>
          <span>Cancel Tabungan</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="manasik_terpadu">
        <i class="fas fa-fw fa-window-maximize"></i>
          <span>Manasik Terpadu</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="perlengkapan">
        <i class="fas fa-fw fa-window-maximize"></i>
          <span>Pengambilan Perlengkapan</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="purna_kepulangan">
        <i class="fas fa-fw fa-window-maximize"></i>
          <span>Purna Kepulangan</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="pasport">
        <i class="fas fa-fw fa-window-maximize"></i>
          <span>Kolektif Pasport</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./?go=password">
        <i class="fas fa-fw fa-window-maximize"></i>
          <span>Ganti Password</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="./?go=info">
          <i class="fa fa-info" aria-hidden="true"></i>
          <span>&nbsp Update Informasi</span>
        </a>
      </li>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        <!-- Layanan Join US Baraka -->
      </div>
      <li>
      <div class="btn-group" style="display:none">
              <button type="button" class="btn btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Attendance
              </button>
              <div class="dropdown-menu" style="background-color:#37AAE4">
                <a class="dropdown-item d-flex align-items-center text-white" href="https://api.whatsapp.com/send?phone=6282125001130">
                NON KNS
                </a>
                <a class="dropdown-item d-flex align-items-center text-white" href="https://api.whatsapp.com/send?phone=6282125001130">
                KNS
                </a>
                <a class="dropdown-item d-flex align-items-center text-white" href="https://api.whatsapp.com/send?phone=6282125001130">
                KLE
                </a>
                <a class="dropdown-item d-flex align-items-center text-white" href="https://api.whatsapp.com/send?phone=6282125001130">
                Daftar Agent
                </a>
              </div>
            </div>
      </li>
      <?php
        }

        if($_SESSION['role'] !== "jamaah"){
      ?>
      <hr class="sidebar-divider">
      <!-- tambahan baru -->
      <li class="nav-item">
        <a class="nav-link" id="mbt1" >
        <i class="fas fa-fw fa-window-maximize"></i>
          <span>Info Program paket Tenda MBT</span>
        </a>
      </li>
      <li class="nav-item">
        <!-- <a class="nav-link" href="https://api.whatsapp.com/send?phone=6282125001130"> -->
        <a class="nav-link" id="tabnikah" >
        <i class="fas fa-fw fa-window-maximize"></i>
          <span>Info Prog Tab Nikah Bund CTL</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="ju1">
        <i class="fas fa-fw fa-window-maximize"></i>
          <span>Informasi Program Join Us</span>
        </a>
      </li>

      <!-- tambahan -->
      <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable" aria-expanded="true"
          aria-controls="collapseTable">
          <i class="fas fa-fw fa-window-maximize"></i>
          <span>Paket MBT</span>
        </a>
        <div id="collapseTable" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded" style="background-color:white">
            <h4 class="collapse-header">Paket MBT</h6>
            <a class="collapse-item" id="mbt1" >Tunai MBT</a>
            <a class="collapse-item" id="mbt2" >Tab Nikah MBT. (Non KNS)</a>
            <div class="btn-group">
              <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Daftar Peserta KPT
              </button>
              <div class="dropdown-menu" style="background-color:#37AAE4">
                <a class="dropdown-item d-flex align-items-center" id="mbt3" >
                Join US KPT Merah
                </a>
                <a class="dropdown-item d-flex align-items-center" id="mbt4" >
                Join US KPT Kuning
                </a>
              </div>
            </div>
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable2" aria-expanded="true"
          aria-controls="collapseTable2">
          <i class="fas fa-fw fa-window-maximize"></i>
          <span>Peluang Join Us Baraka</span>
        </a>
        <div id="collapseTable2" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" id="ju6">Join Us KNS</a>
            <a class="collapse-item" id="ju7">Join Us KLE</a>
          
            <div class="btn-group">
              <button type="button" class="btn col-md-12 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Join Us PJTL
              </button>
              <div class="dropdown-menu" style="background-color:#37AAE4">
                <a class="dropdown-item d-flex align-items-center" id="ju1">
                PJTL 1
                </a>
                <a class="dropdown-item d-flex align-items-center" id="ju2">
                PJTL 2
                </a>
                <a class="dropdown-item d-flex align-items-center" id="ju3">
                PJTL 3
                </a>
                <a class="dropdown-item d-flex align-items-center" id="ju4">
                PJTL 4
                </a>
                <a class="dropdown-item d-flex align-items-center" id="ju5">
                PJTL 5
                </a>
              </div>
            </div>

            <div class="btn-group">
              <button type="button" class="btn btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Join Us Baraka Murni
              </button>
              <div class="dropdown-menu" style="background-color:#37AAE4">
                <a class="dropdown-item d-flex align-items-center" id="ju8">
                MK Merah
                </a>
                <a class="dropdown-item d-flex align-items-center" id="ju9">
                MK Silver
                </a>
                <a class="dropdown-item d-flex align-items-center" id="ju10">
                MKB
                </a>
                <a class="dropdown-item d-flex align-items-center" id="ju11">
                BB
                </a>
                <a class="dropdown-item d-flex align-items-center" id="ju12">
                Ring1A
                </a>
                <a class="dropdown-item d-flex align-items-center" id="ju13">
                Founder
                </a>
              </div>
            </div>
            <a class="collapse-item" id="ju14">Join Us TLPJ</a>
            <div class="btn-group" style="background-color:white">
              <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Kontribusi TL Baraka
              </button>
              <div class="dropdown-menu" style="background-color:#37AAE4"> 
                <a class="dropdown-item d-flex align-items-center" id="ju15">
                Kontribusi TL Baraka
                </a>
                <a class="dropdown-item d-flex align-items-center" id="ju16">
                Kontribusi TL Nasional
                </a>
              </div>
            </div>
            
            <a class="collapse-item" id="ju17">Join Us Khusus Tamu VIP</a>
            <a class="collapse-item" id="ju18">Join Us CLE</a>
            <a class="collapse-item" id="ju19">Join Us PKTL-LE</a>
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable3" aria-expanded="true"
          aria-controls="collapseTable3">
          <i class="fas fa-fw fa-window-maximize"></i>
          <span>Kemitraan Baraka</span>
        </a>
        <div id="collapseTable3" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h4 class="collapse-header">Kemitraan Baraka</h6>
            <a class="collapse-item" href="#">Kanwil Baraka</a>
            <a class="collapse-item" href="#">Cabang Baraka</a>
            <a class="collapse-item" href="#">PKTL-NLE</a>
          </div>
        </div>
      </li> -->
      <?php
        }
      ?>
      <!-- <hr class="sidebar-divider"> -->
      <li class="nav-item">
        <a class="nav-link" href="./logout.php">
        <i class="fa fa-fw fa-times"></i>
          <span>Logout</span>
        </a>
      </li>
      <div class="version" id="version-ruangadmin"></div>
    </ul>
    <!-- Sidebar -->
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <!-- TopBar -->
        <nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top">
          <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          <ul class="navbar-nav ml-auto">
            <div class="topbar-divider d-none d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <img class="img-profile rounded-circle" src="img/boy.png" style="max-width: 60px">
                <span class="ml-2 d-none d-lg-inline text-white small"><?php echo $_SESSION['nama']; ?></span>
              </a>
            </li>
          </ul>
        </nav>
        <!-- Topbar -->
        <!-- Container Fluid-->
        <Row>
          <div class="card" style="margin-bottom:10px; margin-top:-22px">
              <div class="card-body">
                  <h5><b style="color:blue">Paket Tabungan Peradaban Islam di Eropa</b></h5>
                  Selamat Datang kembali <b>Sdr. <?php echo $_nama; ?></b><br>
                  <!-- Terakhir Login : <?php echo $datax['wt'] ; ?> -->
                  <div style="float:right; color:green">   
                    <b><div id="txt"></div></b>
                  </div>
              </div>
          </div>
        </Row>
        <div class="sticky-footer bg-white" style="height:50px; margin-top:-15px; margin-bottom:15px; padding-left:50px;padding-top:15px" >
        <?php
          $sel = "select * from info_jalan";
          $result = mysqli_query($con,$sel);
          while($res = mysqli_fetch_array($result)){
          $txt = $res['nama'];
          }
        ?>
        <marquee width="100%" direction="left" height="100px">
             <?php  echo $txt ; ?>  
        </marquee>
        </div>
        <?php
        include('route.php')

        ?>
        <!---Container Fluid-->
      </div>

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>*copyright &copy; <script> document.write(new Date().getFullYear()); </script>
            </span>
          </div>
        </div>
      </footer>
      <!-- Footer -->
    </div>
  </div>

  <!-- Scroll to top -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/ruang-admin.min.js"></script>
  <!-- Page level plugins -->
  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script>
    $(document).ready(function () {
      $('#dataTable').DataTable(); // ID From dataTable 
      $('#dataTableHover').DataTable(); // ID From dataTable with Hover
      $('#dataTableHover2').DataTable(); // ID From dataTable with Hover
    });

    window.onload = function(){
      // console.log('oooooooo');
      $.ajax({
            type:'GET',
            url:'pages/info/action_info.php',
            data:{
                tlp:true
            },
            success: function(data){
              // console.log(data);
              let obj = JSON.parse(data)
              obj = obj.data[0]
              
              $('#tabnikah').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_kemitraan}`)


              $('#mbt1').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_mbt}`)
              $('#mbt2').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_mbt}`)
              $('#mbt3').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_mbt}`)
              $('#mbt4').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_mbt}`)

              $('#ju1').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju2').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju3').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju4').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju5').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju6').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju7').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju8').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju9').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju10').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju11').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju12').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju13').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju14').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju15').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju16').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju17').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju18').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)
              $('#ju19').attr('href',`https://api.whatsapp.com/send?phone=${obj.tlp_ju}`)

              $('#admin_terpadu').attr('href',`https://api.whatsapp.com/send?phone=${obj.admin_terpadu}`)
              $('#cancel_tabungan').attr('href',`https://api.whatsapp.com/send?phone=${obj.cancel_tabungan}`)
              $('#manasik_terpadu').attr('href',`https://api.whatsapp.com/send?phone=${obj.manasik_terpadu}`)
              $('#perlengkapan').attr('href',`https://api.whatsapp.com/send?phone=${obj.perlengkapan}`)
              $('#purna_kepulangan').attr('href',`https://api.whatsapp.com/send?phone=${obj.purna_kepulangan}`)
              $('#pasport').attr('href',`https://api.whatsapp.com/send?phone=${obj.pasport}`)



                // // console.log(obj);
                // $(`#tlpmbt`).val(obj.tlp_mbt)
                // $(`#tlpju`).val(obj.tlp_ju)
                // $(`#tlpkemitraan`).val(obj.tlp_kemitraan)
            }
      })
    }
    
  </script>

</body>

</html>