<?php
// session_start(); 
$path = "";

if(isset($_GET['go'])){
    $path = $_GET['go'];
}
if($path == "agent"){
    if($_SESSION['role'] == "Agent"){
        include("pages/agent/formagent.php");
    }else{
        include("pages/agent/listagent.php");
    }
}else if($path == "newagent"){
    include("pages/inputagent/newagent.php");
}else if($path == "listnewagent"){
    include("pages/inputagent/listnewagent.php");
}else if($path == "ListAgent"){
    include("pages/inputagent/listnewagent.php");
}else if($path == "addagent"){
    include("pages/agent/formagent.php");
}else if($path == "listupgrade"){
    include("pages/agent/listupgrade.php");
}else if($path == "formagent"){
    include("pages/agent/formagent.php");
}else if($path == "agentdetail"){
    include("pages/inputagent/agentdetail.php");
}else if($path == "pembelian"){
    if($_SESSION['role'] == "Agent"){
        include("pages/pembelian_pin/pembelian.php");
    }else{
        include("pages/pembelian_pin/listpembelian.php");
    }
}else if($path == "wisatawan"){
    if($_SESSION['role'] == "Agent"){
        include("pages/wisatawan/from_wisatawan.php");
    }else{
        include("pages/wisatawan/listwisatawan.php");
    }
}else if($path == "stok_pin"){
    include("pages/stok_pin/stok_pin.php");
}else if($path == "listwisatawan"){
    include("pages/wisatawan/listwisatawan.php");
}else if($path == "detail_wisatawan"){
    include("pages/wisatawan/detail_wisatawan.php");
}else if($path == "point"){
    if($_SESSION['role'] == "Agent"){
        include("pages/point/point.php");
    }else{
        include("pages/point/listpoint.php");
    }
}else if($path == "listpoint"){
    include("pages/point/listpoint.php");
}else if($path == "point_detail"){
    include("pages/point/detail_point.php");
}else if($path == "info"){
    if($_SESSION['role'] == "Agent" || $_SESSION['role'] == "jamaah"){
        include("pages/info/info.php");
    }else{
        include("pages/info/addinfo.php");
    }
}else if($path == "listinfo"){
    include("pages/info/info.php");
}else if($path == "listpoint_detail"){
    include("pages/point/listpoint_detail.php");
}else if($path == "listbonus"){
    include("pages/point/listbonus.php");
}else if($path == "listadmin"){
    include("pages/admin/listadmin.php"); 
}else if($path == "formadmin"){
    include("pages/admin/formadmin.php"); 
}else if($path == "listklaim"){
    include("pages/klaim_point/list_klaim.php"); 
}else if($path == "formklaim"){
    include("pages/klaim_point/form_klaim.php"); 
}else if($path == "nabung"){
    include("pages/nabung/formnabung.php"); 
}else if($path == "listtabungan"){
    include("pages/tabungan/listtabungan.php"); 
}else if($path == "password"){
    include("pages/password/ganti.php"); 
}else{
    // echo $_SESSION['role'];
    if($_SESSION['role'] == "Agent"){
        // include("pages/point/point.php");
    }else if($_SESSION['role'] == "jamaah"){
        // echo "c,j";
        include("pages/nabung/cetakdirect.php");

    }
}
?>