<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Calon Wisatawan</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Wisatawan</li>
        </ol>
    </div>
    <?php
        $nama_lengkap = "";
        $ktp = "";
        $alamat = "";
        $email = "";
        $hp = "";
        $bank = "";
        $tempat_lahir = "";
        $tgl_lahir = "";
        $ahliwaris = "";
        $hp_ahliwaris = "";
        $hubungan = "";   
        $tipe = "add";
        $label = "Tambahkan";
        $kota = "";
        $key = "";
        $password = "";
        $npwp = "";
        $jemaah_id = "";
        $pekerjaan = "";
        $status = "";
        $nama_ahli_waris = "";
        $jemaah_id = $_GET['id'];
        $sel = "SELECT * from jemaah where jemaah_id = '$jemaah_id'";
        $result = mysqli_query($con,$sel);
        $res = mysqli_fetch_array($result);
        if($res){
            $nama_lengkap = $res['nama_jemaah'];
            $ktp = $res['nomor_ktp'];;
            $alamat = $res['alamat'];;
            $email = $res['email'];
            $hp = $res['nomor_hp_jemaah'];
            $bank = "";
            $tempat_lahir = $res['tempat_lahir'];
            $tgl_lahir = $res['tgl_lahir'];
            $rt = $res['rt'];
            $rw = $res['rw'];
            $va = $res['no_va'];
            $ahliwaris = "";
            $hp_ahliwaris = $res['hp_ahli_waris'];;
            $hubungan = $res['hubungan'];;   
            $tipe = "add";
            $label = "Tambahkan";
            $key = "";
            $password = "";
            $kecamatan = $res['kecamatan'];
            $kelurahan = $res['kelurahan'];
            $provinsi = $res['provinsi'];
            $kodepos = $res['kode_pos'];
            $npwp = $res['npwp'];
            $pekerjaan = $res['pekerjaan'];
            $status  = $res['status'];
            $tgl_tiba  = $res['tgl_tiba'];
            $nama_ahli_waris = $res['nama_ahli_waris'];
            $kota = $res['kota'];
        }
    ?>
    <div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Form data diri calon wisatawan
                    <div style="float:right">
                       <a href="./?go=listwisatawan">List Wisatawan</a>
                    </div>
            <hr>
            </div>
            <div class="card-body" id="formInput" style = "display:block">
            <Form method="POST" action="pages/wisatawan/action_wisatawan.php" enctype="multipart/form-data">
                    <input type="hidden"  name="edt" value="edit" class="form-control">
                    <input type="hidden" id="jamaah_id"  name="jamaah_id" value = "<?php echo $jemaah_id; ?>" class="form-control">
                    <div class="overflow-auto" style="height:700px">
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nama Lengkap</label>
                        <input type="text" required  name="nama" class="form-control" value= "<?php echo $nama_lengkap ?>" >
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">NIK KTP</label>
                        <input type="text" required  name="ktp" class="form-control" value= "<?php echo $ktp ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nomor Hp</label>
                        <input type="text" required  name="hp_wisatawan" class="form-control" value= "<?php echo $hp ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Tempat Lahir</label>
                        <input type="text" required  name="tempat_lahir" class="form-control" value= "<?php echo $tempat_lahir ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Tgl Lahir</label>
                        <input type="date" required  name="tgl_lahir" class="form-control" value= "<?php echo $tgl_lahir ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Alamat</label>
                        <input type="text" required  name="alamat" class="form-control" value= "<?php echo $alamat ?>">
                    </div>
                    <div class="mb-3">
                        <Row class="col-md-5">
                            <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="inputPassword5" class="form-label">Rt</label>
                                    <input type="text" required  name="rt" class="form-control" value= "<?php echo $rt ?>">
                                </div>
                                <div class="col-sm-6">
                                    <label for="inputPassword5" class="form-label">Rw.  </label>
                                    <input type="text" required  name="rw" class="form-control" value= "<?php echo $rw ?>">
                                </div>
                            </div>
                            </div>
                        </Row>
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Kecamatan</label >
                        <input type="text" required  name="kecamatan" class="form-control" value= "<?php echo $kecamatan ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Kelurahan</label >
                        <input type="text" required  name="kabupaten" class="form-control" value= "<?php echo $kelurahan ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Kabupaten/Kota</label >
                        <input type="text" required  name="kota" class="form-control" value= "<?php echo $kota ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Provinsi</label >
                        <input type="text" required  name="provinsi" class="form-control" value= "<?php echo $provinsi ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Kode Pos</label >
                        <input type="text" required  name="kodepos" class="form-control" value= "<?php echo $kodepos ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">No Hp</label >
                        <input type="text" required  name="hp" class="form-control" value= "<?php echo $hp ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Email</label>
                        <input type="text"  name="email" class="form-control" value= "<?php echo $email ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">NPWP</label>
                        <input type="text" name="npwp" class="form-control" value= "<?php echo $npwp ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Embarkasi</label>
                        <input type="text" readonly class="form-control" value= "CGK Bandara Soekarno Hatta">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" style="color:green" class="form-label">No ID VA</label>
                        <input type="text" name ="va" placeholder = "Diisi Admin PT.Baraka" value = "<?php echo $va; ?>"  name="va" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" style="color:green" class="form-label">Tgl Tiba</label>
                        <input type="date"  name="tgl_tiba" class="form-control" value= "<?php echo $tgl_tiba ?>">
                    </div>
                    <hr>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Pekerjaan</label>
                        <select class="form-control" name="pekerjaan">
                            <option value="<?php echo $pekerjaan ?>"><?php echo $pekerjaan ?></option>
                            <option value="Wiraswasta">Wiraswasta</option>
                            <option value="PNS">PNS</option>
                            <option value="Polri">Polri</option>
                            <option value="Karyawan Swasta">Karyawan Swasta</option>
                            <option value="lain-lain">Lain-lain</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Status</label>
                        <select class="form-control" name="status">
                            <option value="<?php echo $status ?>"><?php echo $status ?></option>
                            <option value="Menikah">Menikah</option>
                            <option value="Lajang">Lajang</option>
                            <option value="Duda">Duda</option>
                            <option value="Janda">Janda</option>
                            <option value="Pelajar">Pelajar</option>
                        </select>
                    </div>
                    <hr>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Ahli Waris</label>
                        <input type="text"  name="ahliwaris" class="form-control" value= "<?php echo $nama_ahli_waris ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Hp Ahli Waris</label>
                        <input type="text"  name="hp_ahliwaris" class="form-control" value= "<?php echo $hp_ahliwaris ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Hubungan</label>
                        <select required class="form-control" name="hubungan">
                            <option value="<?php echo $hubungan ?>"><?php echo $hubungan ?></option>
                            <option value="Suami">Suami</option>
                            <option value="Istri">Istri</option>
                            <option value="Anak">Anak</option>
                            <option value="Orang Tua">Orang Tua</option>
                            <option value="Sepupu">Sepupu</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" style="color:red" class="form-label">* Ubah Foto Jika Perlu</label>
                        <input type="file" name="foto" class="form-control">
                    </div>
                <?php
                    if($_SESSION['role'] == "Agent"){

                    }else{
                ?>
                        <Button class="btn btn-warning">
                            Edit Data
                        </Button>
                <?php
                    }
                ?>
            </form>   
            </div>
            </div>
            </div>
        </div>
    </div>
</div>