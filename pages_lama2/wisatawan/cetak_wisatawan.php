<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    include_once "../../utility/fpdf17/fpdf.php";
    session_start();
    if(isset($_GET['id'])){
        $id = $_GET['id'];
    }else{
        return;
    }

    $sel = "select a.*,b.nama,b.nomor_agent as agn,DATE_FORMAT(created,'%d-%M-%Y %h:%i') as crt 
    ,DATE_FORMAT(a.tgl_lahir,'%d-%M-%Y') as born
    from jemaah a
    inner join m_agent b on a.m_agent_id = b.m_agent_id
    where jemaah_id = '$id'";
    $result = mysqli_query($con,$sel);
    $data = mysqli_fetch_array($result);

    $pdf = new FPDF('P','mm',array(210,297)); //L For Landscape / P For Portrait
    $pdf->AddPage();
    $pdf->SetX(5);
    $pdf->SetFont('Times','B',15);
    
    if($_SESSION['role'] == "Agent"){
        $pdf->Ln(15);
        $pdf->SetX(35);
        $pdf->Cell(10,8,'History Data Wisatawan Peradaban islam di Eropa');
    }else{
        $pdf->Ln(35);
        $pdf->Image("../../img/logo.jpg",10,15,185);
        $pdf->SetX(35);
        $pdf->Cell(10,8,'Data Peserta Program Tabungan Peradaban Islam di Eropa');
       
    }

    $path = "../../".$data['bukti'];
    // $pdf->Image("../../img/logo.jpg",10,25,-1150);
    // $pdf->SetX(30);
    // $pdf->SetFont('Times','',8);
    // $pdf->Cell(40,18,'PT. BARAKA INSAN MANDIRI');
    // $pdf->Ln(3);
    // $pdf->SetX(30);
    // $pdf->Cell(40,18,'Jl. Rawa Sumur III Blok DD No.13');
    // $pdf->Ln(3);
    // $pdf->SetX(30);
    // $pdf->Cell(40,18,'PULO GADUNG - JAKARTA');
    // $pdf->Ln(5);
    // $pdf->SetX(30);

    //bukti daftar 
    $pdf->Image($path,110,80,80,80);


    $pdf->Ln(0);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'____________________________________________________________________________________________________________________');
    $pdf->Ln(10);
    $pdf->SetFont('Times','B',10);
    $pdf->Cell(10,18,'*  Data Agent');
    $pdf->Ln(10);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nomor Agent');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['agn']);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nama Agent');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['nama']);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nomor Resi');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','B',12);
    $pdf->Cell(10,18,$data['nomor_resi']);

    $pdf->Ln(10);
    $pdf->SetFont('Times','B',10);
    $pdf->Cell(10,18,'*  Data Wisatawan');

    $pdf->Ln(10);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'ID Wisatawan');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['nomor_jemaah']);

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nama');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,strtoupper($data['nama_jemaah']));

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Tgl Registrasi');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['crt']);

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'No. KTP');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['nomor_ktp']);

    if($_SESSION['role'] == "Agent"){

    }else{
        $pdf->Ln(5);
        $pdf->SetFont('Times','B',9);
        $pdf->Cell(10,18,'No. VA');
        $pdf->SetX(40);
        $pdf->Cell(10,18,':');
        $pdf->SetX(45);
        $pdf->SetFont('Times','',9);
        $pdf->Cell(10,18,$data['no_va']); 
    }
     

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Tempal Tgl Lahir');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['tempat_lahir'].",  ".$data['born']);

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Jenis Kelamin');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['jenis_kelamin']);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Alamat');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['alamat']);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'RT/RW');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['rt']."/".$data['rw']);
    
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Kelurahan/Desa');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['kelurahan']);

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Kecamatan');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['kecamatan']);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Kabupaten/Kota');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['kota']);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Provinsi');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['provinsi']);
    
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Kode Pos');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['kode_pos']);  
    $pdf->Ln(5);
    // $pdf->SetFont('Times','B',9);
    // $pdf->Cell(10,18,'Provinsi');
    // $pdf->SetX(40);
    // $pdf->Cell(10,18,':');
    // $pdf->SetX(45);
    // $pdf->SetFont('Times','',9);
    // $pdf->Cell(10,18,$data['kelurahan']);
    
    $pdf->Ln(9);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'NPWP');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['npwp']);  
    
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Status');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['status']);  

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Pekerjaan');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['pekerjaan']);  

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Email');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['email']);  

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Embarkasi');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['embarkasi']);  

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'____________________________________________________________________________________________________________________');

    $pdf->Ln(10);
    $pdf->SetFont('Times','B',10);
    $pdf->Cell(10,18,'*  Data Ahli Waris');

    $pdf->Ln(10);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nama Ahli Waris');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['nama_ahli_waris']);

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nomor HP');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['hp_ahli_waris']); 

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Hubungan');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['hubungan']);  

    
    $pdf->Output();
?>


