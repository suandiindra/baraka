<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Daftar Wisatawan</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">List Wisatawan</li>
        </ol>
    </div>
    <?php
        $filterTabahan = "";
        $filter_id = "";
        if($_SESSION['role'] == "Agent"){
            $id = $_SESSION['m_agent_id'];
            $filterTabahan = $filterTabahan." and a.m_agent_id = '$id' "; 
            $filter_id = " and a.m_agent_id = '$id' "; 
        }
        
        if(isset($_POST['lihat'])){
            if($_POST['randcheck']==$_SESSION['rand']){
                $date1         = $_POST['date1'];
                $date2         = $_POST['date2'];
                $m_agent_id    = $_POST['agent'];
                if(strlen($m_agent_id) > 2){
                    $filterTabahan = $filterTabahan." and a.m_agent_id = '$m_agent_id' "; 
                }
                if(strlen($date1) > 2 && strlen($date2) > 2){
                    $filterTabahan = $filterTabahan." and DATE_FORMAT(created, '%Y-%m-%d') between '$date1' and '$date2' ";
                }
                
                
            }
        }
        
    ?>
    <div class="card" style="margin-bottom:20px;margin-top:-25px">
            <div class="card-body">
            <form action="" method="POST">
            <?php
                $rand=rand();
                $_SESSION['rand']=$rand;
            ?> 
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" /> 
                <div class="container col-12" style="margin-top:0px">
                <div class="row" style="margin-bottom:20px">
                    <div class="col-sm">
                        <label>Periode Daftar</label>
                        <input type="date" class="form-control" name="date1"/>
                    </div>
                    <div class="col-sm">
                        <label>Periode Daftar</label>
                        <input type="date" class="form-control" name="date2"/>
                    </div>
                    <div class="col-sm">
                        <label>Agent</label>
                        <select class="form-control" name="agent">
                            <option value=""></option>
                        <?php
                            $val = "select * from m_agent a where 1=1 $filter_id order by nama asc";
                            $result = mysqli_query($con,$val);
                            while($res=mysqli_fetch_array($result)){
                        ?>
                            <option value="<?php echo $res['m_agent_id'] ?>"><?php echo $res['nama'] ?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>
                    <div class="col-sm" style="margin-top:32px">
                        <Button class="btn btn-success" name="lihat" >Lihat</Button>
                        <Button class="btn btn-danger" name="export">Export</Button>
                    </div>
                </div>
                </div>
            </form>
            
            </div>
    </div>
    <?php
        $filter = "";
        if($_SESSION['role'] == "Agent"){
            $id = $_SESSION['m_agent_id'];
            $filter = " and a.m_agent_id = '$id'";
        }
            $sel = "SELECT a.*,b.nama,b.nomor_agent as no_agent 
            ,DATE_FORMAT(created,'%d-%M-%Y') as created1
            from jemaah a
            inner join m_agent b on a.m_agent_id = b.m_agent_id where a.isactive is null
            and  status_jemaah = 'Daftar' $filter $filterTabahan";

            // echo $sel;
            $result = mysqli_query($con,$sel);
    ?>
    <div class="table-responsive p-3">
        <table class="table align-items-center table-flush table-hover" id="dataTableHover">
            <thead class="thead-light">
            <?php
                if($_SESSION['role'] == "Agent"){
            ?>
                    <tr>
                        <th>No.</th>
                        <th>ID Jamaah</th>
                        <th>Nomor Agent</th>
                        <th>Tgl Daftar</th>
                        <th>Nomor Resi</th>
                        <th>Nama Agent</th>
                        <th>Calon Jamah</th>
                        <th>Nomor HP</th>
                        <th>Action</th>
                    </tr>
            <?php
                }else{
            ?>
                    <tr>
                        <th>No.</th>
                        <th>ID Jamaah</th>
                        <th>Nomor Agent</th>
                        <th>Tgl Daftar</th>
                        <th>Nomor Resi</th>
                        <th>Nomor VA</th>
                        <th>Nama Agent</th>
                        <th>Calon Jamah</th>
                        <th>Nomor HP</th>
                        <th>Action</th>
                    </tr>
            <?php
                }
            ?>
            </thead>
    <?php
        $i = 1;
            while($res = mysqli_fetch_array($result)){
    ?>
    <!-- darisini -->
    <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $res['nomor_jemaah']; ?></td>
        <td><?php echo $res['no_agent']; ?></td>
        <td><?php echo $res['created1']; ?></td>
        <td><?php echo $res['nomor_resi']; ?></td>
        <?php
             if($_SESSION['role'] == "Agent"){
        ?>

        <?php
             }else{
        ?>
                <td>
                    <?php echo $res['no_va'];?>
                </td>
        <?php
             }
        ?>
        
        <td><?php echo $res['nama']; ?></td>
        <td><?php echo $res['nama_jemaah']; ?></td>
        <td><?php echo $res['nomor_hp_jemaah']; ?></td>
        <td>
            
        <?php
            if($_SESSION['role'] == "Agent"){
        ?>
            <a href="pages/wisatawan/cetak_wisatawan_agent.php?id=<?php echo $res['jemaah_id'] ?>">
                <Button class="btn btn-success">
                    Cetak
                </Button>
            </a>
        <?php
            }else{
        ?>
            <a href="pages/wisatawan/cetak_wisatawan.php?id=<?php echo $res['jemaah_id'] ?>">
                <Button class="btn btn-success">
                    Cetak
                </Button>
            </a>
            <a href="./?go=detail_wisatawan&id=<?php echo $res['jemaah_id'] ?>">
                <Button class="btn btn-warning">
                    Info Detail
                </Button>
            </a>
        </td>
        <?php
            }
        ?>
    </tr>
    <!-- sampe sini -->
    <?php
        $i += 1;
        }
    ?>
        </table>
    </div>
    <?php
        if(isset($_POST['export'])){
            // if($_POST['randcheck']==$_SESSION['rand']){
                $date1         = $_POST['date1'];
                $date2         = $_POST['date2'];
                $m_agent_id    = $_POST['agent'];
                if(strlen($m_agent_id) > 2){
                    $filterTabahan = $filterTabahan." and a.m_agent_id = '$m_agent_id' "; 
                }
                if(strlen($date1) > 2 && strlen($date2) > 2){
                    $filterTabahan = $filterTabahan." and DATE_FORMAT(created, '%Y-%m-%d') between '$date1' and '$date2' ";
                }

                $id = str_replace("'","@",$filterTabahan);
                echo $id;
                echo "<script>window.location = './pages/wisatawan/export_wisatawan.php?q=$id'</script>";
            // }
        }
    ?>
</div>
<script>
    function export(){
        alert("ok");
    }
</script>