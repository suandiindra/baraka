<?php
    session_start();
    $_user = $_SESSION["m_agent_id"];
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    $where = str_replace("@","'",$_GET['q']);

    // echo $where;
    
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Data Calon Wisatawan.xls");
    $mydate=getdate(date("U"));
    $tgl = "$mydate[weekday], $mydate[month] $mydate[mday], $mydate[year]";
?>
<table>
    <tr>
        <td><h3><b>PT. BARAKA INSAN MANDIRI</b></h3></td>
    </tr>
    <tr>
        <td><b>Laporan Data Calon Jamaah</b></td>
    </tr>
</table>
<table border=1>
<thead class="thead-light">
    <tr>
    <th>No.</th>
    <th>ID Jamaah</th>
    <th>Nomor Agent</th>
    <th>Nama Agent</th>
    <th>Nomor Resi</th>
    <th>Nomor VA</th>
    <th>Calon Jamah</th>
    <th>Tgl Daftar</th>
    <th>Tgl Tiba</th>
    <th>No. KTP</th>
    <th>Tempat Lahir</th>
    <th>Tgl Lahir</th>
    <th>Nomor HP</th>
    <th>Email</th>
    <th>NPWP</th>
    <th>Alamat</th>
    <th>RT/RW</th>
    <th>Kecamatan</th>
    <th>Kelurahan</th>
    <th>Kode POS</th>
    <!-- <th>Kota</th> -->
    <th>Pekerjaan</th>
    <th>Status</th>
    <th>Embarkasi</th>
    <th>Ahli Waris</th>
    <th>Nomor HP</th>
    <th>Hubungan</th>
    </tr>
</thead>
<tbody>
    <?php 
        
        $sel = "SELECT a.*,b.nama,b.nomor_agent as agn,b.nama as nm_agn from jemaah a
        inner join m_agent b on a.m_agent_id = b.m_agent_id where status_jemaah <> 'Draft' $where";
        // echo $sel;
        $result = mysqli_query($con,$sel);
        $i = 1;
        while($res = mysqli_fetch_array($result)){
    ?>
    <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $res['nomor_jemaah']; ?></td>
        <td><?php echo $res['agn']; ?></td>
        <td><?php echo $res['nm_agn']; ?></td>
        <td><?php echo $res['nomor_resi']; ?></td>
        <td><?php echo "'".$res['no_va']; ?></td>
        <td><?php echo $res['nama_jemaah']; ?></td>
        <td><?php echo ($res['created']); ?></td>
        <td><?php echo $res['tgl_tiba']; ?></td>
        <td><?php echo $res['nomor_ktp']; ?></td>
        <td><?php echo $res['tempat_lahir']; ?></td>
        <td><?php echo $res['tgl_lahir']; ?></td>
        <td><?php echo "'".$res['nomor_hp_jemaah']; ?></td>
        <td><?php echo $res['email']; ?></td>
        <td><?php echo "'".$res['npwp']; ?></td>
        <td><?php echo $res['alamat']; ?></td>
        <td><?php echo $res['rt']."/".$res['rw']; ?></td>
        <td><?php echo $res['kecamatan']; ?></td>
        <td><?php echo $res['kelurahan']; ?></td>
        <td><?php echo $res['kode_pos']; ?></td>
        <td><?php echo $res['pekerjaan']; ?></td>

        <td><?php echo $res['status']; ?></td>
        <td><?php echo $res['embarkasi']; ?></td>
        <td><?php echo $res['nama_ahli_waris']; ?></td>
        <td><?php echo "'".$res['hp_ahli_waris']; ?></td>
        <td><?php echo $res['hubungan']; ?></td>
    </tr>
    <?php
        $i =  $i + 1;
        }
        
    ?>
</tbody>
</table>
<?php
    echo "<script>window.location='../../?go=detail_wisatawan'</script>";
?>
