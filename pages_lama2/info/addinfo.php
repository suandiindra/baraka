<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Informasi</li>
    </ol>
    </div>
    <?php
        $txt = "";
        $sel = "select * from info_jalan";
        $result = mysqli_query($con,$sel);
        while($res = mysqli_fetch_array($result)){
        $txt = $res['nama'];
        }
    ?>

    <div class="row" style="margin-top:-25px">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Informasi Berjalan
                    <div style="float:right">
                    </div>
                    <hr>
                <div class="mb-3">
                <Row>
                    <col>
                    <Form method="POST" action="./pages/info/action_info2.php">
                        <input type="hidden"  name="txt" class="form-control" value= "<?php  echo $txt; ?>">
                        <input type="text" required  name="infojalan" class="form-control" value= "<?php  echo $txt; ?>">
                        <Button class="btn btn-warning" style="margin-top:10px">
                            Tetapkan
                        </Button>
                    </Form>
                    </col>
                </Row>
                </div>
            </div>
        </div>
    </div>
    </div>



    <div class="row" style="margin-top:25px">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Form Update Informasi
                    <div style="float:right">
                     <b><a href="./?go=listinfo">List Informasi</a></b>
                    </div>
                    <hr>
               
            </div>

            <div class="card-body">
            <Form method="POST" action="pages/info/action_info.php" enctype="multipart/form-data">
                <input type="hidden"  name="tp" value="<?php echo $tipe?>" class="form-control">
                <input type="hidden"  name="key" value="<?php echo $key?>" class="form-control">
                <div class="overflow-auto" style="height:700px">
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Judul Informasi</label>
                    <input type="text" required  name="judul" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Lampiran</label>
                    <input type="file"  name="informasi" class="form-control" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Informasi</label>
                    <textarea required class="form-control" id="exampleFormControlTextarea1" rows="5" name = "info"></textarea>
                </div>
               
                <Button class="btn btn-success">
                    Tambahkan
                </Button>
              
            </form>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>