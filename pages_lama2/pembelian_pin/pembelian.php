<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Pembelian PIN</li>
    </ol>
    </div>
</div>
<div class="row" style="margin-top:-23px">
    <div class="col-lg-12">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><b>Pembelian PIN</b></h5>
                <p class="card-text">Feature ini digunakan untuk melakukan pembelian PIN agent</p>
            </div>
            <hr>
            <Form method="POST" action="pages/pembelian_pin/action_pembelian.php">
            <div class="card-body">
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">JUMLAH PIN</label>
                    <input type="number"  name="pin" class="form-control" placeholder="Masukan Digit PIN">
                </div>
                <Button class="btn btn-success" id="btn-beli">
                    Beli PIN
                </Button>
            </div>
            </form>
        </div>
    </div>
    </div>
    <?php
        $cek = "select *,case when kode_status = 'WT0' then '#F86290; color:#ffff' when kode_status = '0' then '#ea1958; color:#ffff' else '#659999; color:#ffff' end as color 
        ,DATE_FORMAT(tgl_topup,'%d-%M-%Y %H:%i') as tgl_topup1
        ,DATE_FORMAT(confirm_date,'%d-%M-%Y %H:%i') as confirm_date1
        from transaksi_agent 
        where m_agent_id = '$_user' order by tgl_topup desc";
        $result = mysqli_query($con,$cek);
        while($res = mysqli_fetch_array($result)){
    ?>

    <div class="col-lg-12">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header" style="background-color:<?php echo $res['color'] ?>">
                ID Transaksi : <?php echo $res['urutan'] ?>
                <div style="color:yellow;">
                    <?php echo $res['status_transaksi'] ?>
                </div>
                <?php
                    if($res['kode_status'] == "WT0"){
                ?>
                <div style="color:yellow; float:right; margin-top:-20px">
                <Form method="POST" action="pages/pembelian_pin/action_pembelian.php">
                <input type="hidden"  name="tipe" class="form-control" value= "batal">
                <input type="hidden"  name="id" class="form-control" value= "<?php echo $res['transaksi_agent_id'] ?>">
                    <Button class="btn btn-warning" id="btn-beli">
                        Batal
                    </Button>
                </form>
                </div>
                <?php
                    }else{
                ?>
                        <div style="color:yellow; float:right; margin-top:-40px">
                            <h4><b><?php  echo $res['jml_pin'] ?> Pin</b></h4>
                        </div>
                <?php
                    }
                ?>
            </div>
            <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Tgl Transaksi</label>
                        <input type="text" readonly  name="ahliwaris" class="form-control" value= " <?php echo $res['tgl_topup1'] ?>">
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nominal Transaksi</label>
                        <input type="text" readonly  name="ahliwaris" class="form-control" value= "<?php echo number_format($res['nominal_topup']) ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Tgl Konfirmasi</label>
                        <input type="text" readonly  name="ahliwaris" class="form-control" value= "<?php echo $res['confirm_date1'] ?>">
                    </div>
                </div>
                <div class="col">
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Status Transaksi</label>
                        <input type="text" readonly  name="ahliwaris" class="form-control" value= "<?php echo $res['status_transaksi'] ?>">
                    </div>
                </div>
            </div>
            <?php
            if($res['kode_status'] == "WT0"){
            ?>
               <div class="row">
                <div class="col">
                    <form action="pages/pembelian_pin/action_pembelian.php" method="POST" enctype="multipart/form-data">
                        <input type="hidden"  name="id_bukti" class="form-control" value= "<?php echo $res['transaksi_agent_id'] ?>">
                        <div class="mb-3">
                            <input type="file" name="bukti" class="form-control">
                        </div>
                        <div class="mb-3">
                            <Button class="btn btn-success" id="btn-beli">
                                Upload Bukti Transfer
                            </Button>
                        </div>
                    </form>
                </div>
              </div>
            <?php
                }
            ?>
            </div>
        </div>
        <?php
            if($res['kode_status'] == "WT0"){
        ?>
            <div class="card-footer" style="background-color:<?php echo $res['color'] ?>">
                <p style="color:yellow">
                    Pembelian PIN Menunggu konfirmasi pembayaran, <br>
                    No rek Resmi PT. BARAKA pembelian stok PIN tabungan peradaban islam di eropa BCA : 4860-5900- 05
                </p>
                <p>
                    Atau konfirmasi ke admin BARAKA 
                    <a href="https://api.whatsapp.com/send?phone=6282125001130">  Disini</a>
                </p>
            </div>
        <?php
            }
        ?>
    </div>
    </div>
    <?php
        }
    ?>
</div>