<?php
    session_start();
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    $tipe = "";
    $id_uniq = "select uuid() as id";
    $result = mysqli_query($con,$id_uniq);
    $id_uniq = mysqli_fetch_array($result);
    $id_uniq = $id_uniq["id"];
    $_user = $_SESSION["m_agent_id"];
    $_nomor_agent = $_SESSION["nomor_agent"];
    $_nama = $_SESSION["nama"];
    if(isset($_POST['pin'])){
        // echo $_SESSION["m_agent_id"]."--".$_SESSION["nama"]."--".$_SESSION["nomor_agent"];
        $jml = $_POST['pin'];
        $nomor_transaksi = rand(100000,10000);
        $nominal_topup = $jml * 2000000;

        $urutan = transApprove($con);
        $insert = "insert into transaksi_agent (transaksi_agent_id,m_agent_id,nomor_agent,nama_agent,nominal_topup,tgl_topup
        ,status_transaksi,kode_status,nomor_transaksi,jml_pin,urutan)
        values ('$id_uniq','$_user','$_nomor_agent','$_nama','$nominal_topup',now(),'Menunggu Bukti Transfer'
        ,'WT0','$nomor_transaksi',$jml,'$urutan')";
        $result = mysqli_query($con,$insert);
        if($result){
            echo "<script>alert('Berhasil')</script>";
            echo "<script>window.location='../../?go=pembelian'</script>";
        }
    }else if(isset($_POST['tipe'])){
        if($_POST['tipe'] == "batal"){
            $id = $_POST['id'];
            $del = "delete from transaksi_agent where transaksi_agent_id = '$id'";
            // echo $del;
            $result = mysqli_query($con,$del);
            if($result){
                echo "<script>alert('Berhasil')</script>";
                echo "<script>window.location='../../?go=pembelian'</script>";
            }
        }
    }else if(isset($_POST['id_bukti'])) {
        $id = $_POST['id_bukti'];
        $nama_file1 = $_FILES['bukti']['name'];
        $ukuran_file1 = $_FILES['bukti']['size'];
        $tipe_file1 = $_FILES['bukti']['type'];
        $tmp_file1 = $_FILES['bukti']['tmp_name'];
        $path1 = "../../asset/bukti_transfer/".$id."/".$nama_file1;
        if( is_dir($path1) === false )
        {
            mkdir("../../asset/bukti_transfer/".$id."/");
        }
        $pathData = "asset/bukti_transfer/".$id."/".$nama_file1;
        // echo $pathData;
        if($ukuran_file1 <= 1000000){		
            if(move_uploaded_file($tmp_file1, $path1)){
                $upd = "update transaksi_agent set bukti_topup = '$pathData' 
                ,status_transaksi = 'Menunggu Approval', kode_status = 'WT1'
                ,confirm_date = now() where transaksi_agent_id = '$id'";
                $result = mysqli_query($con,$upd);
                if($result){
                    echo "<script>alert('Berhasil')</script>";
                    echo "<script>window.location='../../?go=pembelian'</script>";
                }
            }
        }
    }else if(isset($_GET['act'])){
        if($_GET['act'] == "1"){
            $id = $_GET['id'];
            $urutan = transApprove($con);
            $upd = "update transaksi_agent set is_confirm_topup = 'Y' ,kode_status = 'WT2'
            , status_transaksi = 'Approved' where transaksi_agent_id = '$id'";

            // echo $upd;
            $result = mysqli_query($con,$upd);
            if($result){
                $cek = "select * from transaksi_agent where transaksi_agent_id = '$id'";
                $dt = mysqli_fetch_array(mysqli_query($con,$cek));
                $pin = $dt['jml_pin'];
                $m_agent_id = $dt['m_agent_id'];
                $no_agent = $dt['nomor_agent'];
                for($i = 1; $i <= $pin; $i++){
                    $sel = "select uuid() as id, SUBSTRING(replace(uuid(),'-',''),1,16) as key_";
                    $result = mysqli_query($con,$sel);
                    $uniq = mysqli_fetch_array($result);
                    $uniq_ = $uniq["id"];
                    $key_ = $uniq["key_"];
                    $resi = cekUrutanResi($con);

                    $id_wisatawan = idwisatawan($con,$m_agent_id,$no_agent);
                    $id_va = str_replace("JU","",$id_wisatawan);
                    $masuk = "insert into jemaah (jemaah_id,status_jemaah,transaksi_agent_id,m_agent_id,nomor_resi,uniq_key
                    ,nomor_agent,nomor_jemaah,no_va)
                    values ('$uniq_','Draft','$id','$m_agent_id','$resi','$key_','$_nomor_agent','$id_wisatawan','$id_va')";
                    
                    $result = mysqli_query($con,$masuk);
                    // echo $uniq_."</br>";
                }
                echo "<script>alert('Berhasil')</script>";
                echo "<script>window.location='../../?go=pembelian'</script>";
            }
        }else{
            $id = $_GET['id'];
            $upd = "update transaksi_agent set is_confirm_topup = 'N' ,kode_status = '0'
            , status_transaksi = 'Ditolak' where transaksi_agent_id = '$id'";

            // echo $upd;
            $result = mysqli_query($con,$upd);
            if($result){
                echo "<script>alert('Pembelian PIN Ditolak')</script>";
                echo "<script>window.location='../../?go=pembelian'</script>";
            }
        }
    }

?>