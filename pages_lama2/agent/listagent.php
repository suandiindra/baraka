<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Agent</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">List Agent</li>
    </ol>
</div>
<div class="row">
            <!-- DataTable with Hover -->
            <div class="col-lg-12">
              <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary"><a href="./?go=addagent">+ Data Agent</a></h6>
                </div>
                <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>No</th>
                        <th>ID Agent</th>
                        <th>Nama Agent</th>
                        <th>KTP</th>
                        <th>Alamat</th>
                        <th>No. Hp</th>
                        <th>Bank</th>
                        <th>Rekening</th>
                        <th>Ahli Waris</th>
                        <th>No. Hp</th>
                        <th>Hubungan</th>
                        <th>Last Login</th>
                        <th style="width:200px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sel = "select *,DATE_FORMAT(last_login,'%d-%M-%Y %H:%i') as last_login from m_agent where isactive <> 'N'";
                            $result = mysqli_query($con,$sel);
                            $i = 1;
                            while($res = mysqli_fetch_array($result)){
                        ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $res['nomor_agent']; ?></td>
                                <td><?php echo $res['nama']; ?></td>
                                <td><?php echo $res['ktp']; ?></td>
                                <td><?php echo $res['alamat']; ?></td>
                                <td><?php echo $res['hp']; ?></td>
                                <td><?php echo $res['bank']; ?></td>
                                <td><?php echo $res['nomor_rekening']; ?></td>
                                <td><?php echo $res['nama_ahli_waris']; ?></td>
                                <td><?php echo $res['nomor_hp']; ?></td>
                                <td><?php echo $res['hubungan']; ?></td>
                                <td><?php echo $res['last_login']; ?></td>
                                <td style="text-align:center">
                                    <!-- <a href="./?go=addagent&act=edit&id=<?php echo $res['m_agent_id']; ?>"><button class="btn btn-danger col-md-10">Edit</button></a>
                                    <a href="./pages/agent/action_agent.php?act=del&id=<?php echo $res['m_agent_id']; ?>"><button class="btn btn-warning col-md-10">Hapus</button></a> -->
                                  <table>
                                    <tr>
                                    <td  style="padding:0"><a href="./?go=addagent&act=edit&id=<?php echo $res['m_agent_id']; ?>"><button class="btn btn-danger">Edit</button></a></td>
                                    <!-- <td style="padding:0"> <a href="./pages/agent/action_agent.php?act=del&id=<?php echo $res['m_agent_id']; ?>"><button class="btn btn-warning">Hapus</button></a></td> -->
                                    </tr>
                                  </table>
                                </td>
                            </tr>
                        <?php
                            $i += 1;
                            }
                        ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
    