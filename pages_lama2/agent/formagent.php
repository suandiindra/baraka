<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Agent</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Agent Profile</li>
        </ol>
    </div>
    <?php
        $nama_lengkap = "";
        $ktp = "";
        $alamat = "";
        $email = "";
        $hp = "";
        $bank = "";
        $rekening = "";
        $ahliwaris = "";
        $hp_ahliwaris = "";
        $hubungan = "";   
        $tipe = "add";
        $label = "Tambahkan";
        $key = "";
        $password = "";
        $id_agent = "";
        if(isset($_GET["id"])){
            $id = $_GET["id"];
            $key = $id;
            $sel = "select * from m_agent where m_agent_id = '$id'";
            $result = mysqli_query($con,$sel);
            if($result){
                $tipe = "edit";
                $label = "Edit";
                $data = mysqli_fetch_array($result);
                $id_agent = $data['nomor_agent'];
                $nama_lengkap = $data['nama'];
                $ktp = $data['ktp'];
                $alamat = $data['alamat'];
                $email = $data['email'];
                $hp = $data['hp'];
                $bank = $data['bank'];
                $rekening = $data['nomor_rekening'];
                $ahliwaris = $data['nama_ahli_waris'];
                $hp_ahliwaris = $data['nomor_hp'];;
                $hubungan = $data['hubungan'];   
            }
        }
    ?>
    <div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Form data diri agent diisi dan di kelola oleh Admin
                    <hr>
            </div>

            <div class="card-body">
            <Form method="POST" action="pages/agent/action_agent.php">
                <input type="hidden"  name="tp" value="<?php echo $tipe?>" class="form-control">
                <input type="hidden"  name="key" value="<?php echo $key?>" class="form-control">
                <div class="overflow-auto" style="height:700px">
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">ID Agents</label>
                    <input type="text" maxlength="6" required  name="nomor_agent" class="form-control" value= "<?php echo $id_agent ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama Lengkap</label>
                    <input type="text" required  name="nama" class="form-control" value= "<?php echo $nama_lengkap ?>" >
                </div>
                <?php
                    if($_SESSION['role'] == "Agent"){
                        
                    }else{
                ?>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Password</label>
                    <input type="text"  name="password" class="form-control" value= "<?php echo $password ?>" >
                </div>
                <?php
                    }
                ?>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">NIK KTP</label>
                    <input type="number" required  name="ktp" class="form-control" value= "<?php echo $ktp ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Alamat</label>
                    <input type="text" required  name="alamat" class="form-control" value= "<?php echo $alamat ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">No Handphone</label >
                    <input type="text" required  name="hp" class="form-control" value= "<?php echo $hp ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Email</label>
                    <input type="text"  name="email" class="form-control" value= "<?php echo $email ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Bank</label>
                    <input type="text" required  name="bank" class="form-control" value= "<?php echo $bank ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">No Rekening</label>
                    <input type="text" required  name="rekening" class="form-control" value= "<?php echo $rekening ?>">
                </div>
                <hr>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama Ahli Waris</label>
                    <input type="text" required  name="ahliwaris" class="form-control" value= "<?php echo $ahliwaris ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">No. Handphone</label>
                    <input type="text" required  name="hp_ahliwaris" class="form-control" value= "<?php echo $hp_ahliwaris ?>">
                </div>
                <!-- <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Hubungan</label>
                    <input type="text"  name="hub" class="form-control">
                </div> -->
                <div class="form-group">
                    <label>Hubungan</label>
                   <select name="hub" required id="m_merk_id" class="form-control" >
                        <option value="<?php echo $hubungan ?>"><?php echo $hubungan ?></option>
                        <option value="Suami">Suami</option>
                        <option value="Istri">Istri</option>
                        <option value="Anak">Anak</option>
                        <option value="Saudara Kandung">Saudara Kandung</option>
                        <option value="Orang Tua">Orang Tua</option>
                        <option value="Sepupu">Sepupu</option>
                    </select>
                  </div>
                <?php
                    if($_SESSION['role'] == "Agent"){
                        
                    }else{
                ?>
                    <Button class="btn btn-success">
                            <?php echo $label; ?>
                    </Button>
                <?php
                    }
                ?>
            </form>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>