<?php
    session_start();
    $_user = $_SESSION["m_agent_id"];
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    $where = str_replace("@","'",$_GET['q']);

    // echo $where;
    
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Data Point.xls");
    $mydate=getdate(date("U"));
    $tgl = "$mydate[weekday], $mydate[month] $mydate[mday], $mydate[year]";
?>
<table>
    <tr>
        <td><h3><b>PT. BARAKA INSAN MANDIRI</b></h3></td>
    </tr>
    <tr>
        <td><b>Laporan Data Calon Jamaah</b></td>
    </tr>
</table>
<table border=1>
<thead class="thead-light">
    <tr>
        <th>ID Agent</th>
        <th>Nama Agent</th>
        <th>Bank</th>
        <th>No Rekening</th>
        <th>Point</th>
        <th>Nominal (nett)</th>
        <th>Ppn (10%)</th>
        <th>Tgl CutOff</th>
        <th>Tgl Transfer</th>
        <th>Status</th>
    </tr>
</thead>
<tbody>
    <?php 
        
        $sel = "select a.m_agent_id,b.nomor_agent,b.nama,bank,nomor_rekening,sum(point_count) as point,cut_off, sum(point_amount) as amount,sum(point_pajak) as ppn
        ,'' as cutoff,'',case when a.status = 'Point' then 'On Going' else status end as stat,tgl_pencairan from transaksi_point a
        inner join m_agent b on a.m_agent_id = b.m_agent_id $where
        group by a.m_agent_id,b.nomor_agent,b.nama,bank,nomor_rekening,cut_off,tgl_pencairan";

        // echo $sel;
        $result = mysqli_query($con,$sel);
        $i = 1;
        while($res = mysqli_fetch_array($result)){
    ?>
    <tr>
        <td><?php echo $res['nomor_agent']; ?></td>
        <td><?php echo $res['nama']; ?></td>
        <td><?php echo $res['bank']; ?></td>
        <td><?php echo "'".$res['nomor_rekening']; ?></td>  
        <td><?php echo $res['point']; ?></td>
        <td><?php echo ($res['amount']); ?></td>
        <td><?php echo ($res['ppn']); ?></td>
        <td><?php echo $res['cut_off']; ?></td>
        <td><?php echo $res['tgl_pencairan']; ?></td>
        <td><?php echo $res['stat']; ?></td>
    </tr>
    <?php
        $i =  $i + 1;
        }
        
    ?>
</tbody>
</table>
<?php
    echo "<script>window.location='../../?go=detail_wisatawan'</script>";
?>
