<?php
    session_start();
    include("../../utility/config.php");
    include("../../utility/fungsi.php");

    if(isset($_POST['option'])){
        $ok = $_POST['option'];
        $usr = $_SESSION['m_agent_id'];
        $filter = "";
        if($ok == "Gagal"){
            $filter = " and a.isactive = '0' ";
        }else if($ok == "Terpakai"){
            $filter = " and COALESCE(LENGTH(a.nama_jemaah),0) > 2 and a.isactive is null ";
        }else if($ok == "Belum terpakai"){
            $filter = " and COALESCE(LENGTH(a.nama_jemaah),0) < 2 and a.isactive is null ";
        }

        if($_SESSION['role'] == "Agent"){
            $cek = "select *,b.nomor_agent as nom,b.nama as nm,nomor_transaksi,a.nomor_resi as rs,a.isactive from jemaah a
            join m_agent b on a.m_agent_id = b.m_agent_id  
            join transaksi_agent c on c.transaksi_agent_id = a.transaksi_agent_id
            where a.m_agent_id = '$usr' $filter -- and status_jemaah = 'Draft' 
            order by convert(urutan,UNSIGNED INTEGER),a.nomor_resi asc";
        }else{
            $cek = "select *,b.nomor_agent as nom,b.nama as nm,nomor_transaksi,a.nomor_resi as rs,a.isactive from jemaah a
            join m_agent b on a.m_agent_id = b.m_agent_id  
            join transaksi_agent c on c.transaksi_agent_id = a.transaksi_agent_id
            where 1=1 $filter -- and a.m_agent_id = '1159b61a-a985-11eb-b2ac-b8ca3a5f2040'
            order by convert(urutan,UNSIGNED INTEGER),a.nomor_resi asc";
        }

        $out = "";
        $result = mysqli_query($con,$cek);
        $i = 1;
        while($res = mysqli_fetch_array($result)){
            $sts = "";
            if(strlen($res['nama_jemaah']) > 2 && $res['isactive'] == "0"){
                $sts = "Gagal Terjual";
            }if (strlen($res['nama_jemaah']) > 2 && $res['isactive'] == null){
                $sts = "Terpakai";
            }else if(strlen($res['nama_jemaah']) < 2){
               $sts = "Belum Terpakai";
            }
            // item td
            $obj = "";
            if($_SESSION['role'] == 'Agent'){

            }else{
                $obj = '<td>';
                    if(strlen($res['nama_jemaah']) > 2 && $res['isactive'] == null){
                        $obj .= "<a onclick='javascript:confirmationDelete($(this));return false;'  href='./pages/stok_pin/action_pin.php?id=".$res['jemaah_id']."'>
                        <Button class='btn btn-danger'>Reset PIN</Button>
                    </a>";
                    } $obj .="</td>";
            }
            $out = $out. "<tr>
                <td>". $i."</td>
                <td>".$res['nom']."</td>
                <td>". $res['nm']."</td>
                <td>". $res['urutan']." </td>
                <td>". $res['rs']."</td>
                <td>". $res['nomor_jemaah']."</td>
                <td>". $res['uniq_key']."</td>
                <td>".$sts."</td>
                ".$obj."
            </tr>";
            $i +=1;
        }

        // echo $cek;
        echo $out;
    }else if(isset($_POST['search'])){
        $id = $_POST['search'];

        $filter = " and a.nomor_resi like '%$id%' or nomor_jemaah like '%$id%' or nama_jemaah like '%$id%' or  b.nomor_agent like '%$id%' ";

        if($_SESSION['role'] == "Agent"){
            $cek = "select *,b.nomor_agent as nom,b.nama as nm,nomor_transaksi,a.nomor_resi as rs,a.isactive from jemaah a
            join m_agent b on a.m_agent_id = b.m_agent_id  
            join transaksi_agent c on c.transaksi_agent_id = a.transaksi_agent_id
            where a.m_agent_id = '$usr' $filter 
            order by convert(urutan,UNSIGNED INTEGER),a.nomor_resi asc";
        }else{
            $cek = "select *,b.nomor_agent as nom,b.nama as nm,nomor_transaksi,a.nomor_resi as rs,a.isactive from jemaah a
            join m_agent b on a.m_agent_id = b.m_agent_id  
            join transaksi_agent c on c.transaksi_agent_id = a.transaksi_agent_id
            where 1=1 $filter 
            order by convert(urutan,UNSIGNED INTEGER),a.nomor_resi asc";
        }

        $out = "";
        $result = mysqli_query($con,$cek);
        $i = 1;
        while($res = mysqli_fetch_array($result)){
            $sts = "";
            if(strlen($res['nama_jemaah']) > 2 && $res['isactive'] == "0"){
                $sts = "Gagal Terjual";
            }if (strlen($res['nama_jemaah']) > 2 && $res['isactive'] == null){
                $sts = "Terpakai";
            }else if(strlen($res['nama_jemaah']) < 2){
               $sts = "Belum Terpakai";
            }
            // item td
            $obj = "";
            if($_SESSION['role'] == 'Agent'){

            }else{
                $obj = '<td>';
                    if(strlen($res['nama_jemaah']) > 2 && $res['isactive'] == null){
                        $obj .= "<a onclick='javascript:confirmationDelete($(this));return false;'  href='./pages/stok_pin/action_pin.php?id=".$res['jemaah_id']."'>
                        <Button class='btn btn-danger'>Reset PIN</Button>
                    </a>";
                    } $obj .="</td>";
            }
            $out = $out. "<tr>
                <td>". $i."</td>
                <td>".$res['nom']."</td>
                <td>". $res['nm']."</td>
                <td>". $res['urutan']." </td>
                <td>". $res['rs']."</td>
                <td>". $res['nomor_jemaah']."</td>
                <td>". $res['uniq_key']."</td>
                <td>".$sts."</td>
                ".$obj."
            </tr>";
            $i +=1;
        }

        // echo $cek;
        echo $out;
    }else{
        $id = $_GET['id'];
        // $upd = "update jemaah set nama_jemaah = null,nomor_ktp = null, nomor_paspor='', tempat_lahir = null
        //                 , tgl_lahir = null,npwp = null,embarkasi = null
        //                 ,alamat = null,provinsi = null, kecamatan = null
        //                 ,status = null,pekerjaan = null,created = null,creted_by = null,email = null,kode_pos = null
        //                 ,status_jemaah = 'Draft ',bukti = null,nama_ahli_waris = null,hubungan = null,hp_ahli_waris = null
        //                 ,nomor_hp_jemaah =null, kelurahan = null,rt=null, rw =null, jenis_kelamin = null
        //                 where jemaah_id = '$id' ";
        // echo $upd;

        $upd0 = "update jemaah set isactive = 0 where jemaah_id = '$id'";

        $selpin = "select * from transaksi_point where jamaah_id = '$id'";
        $result1 = mysqli_query($con,$selpin);
        $ds = mysqli_fetch_array($result1);


        $sts = $ds['is_claim'];
        $agent = $ds['m_agent_id'];
        $status = $ds['status'];
        if($sts == "N"){
            $upd = "update transaksi_point set is_claim = 'X' where jamaah_id = '$id'";
            $result2 = mysqli_query($con,$upd);
            $cek = "select * from transaksi_point where is_claim = 'Y' and status <> 'Payment' and m_agent_id = '$agent'";
            $result3 = mysqli_query($con,$cek);
            if(mysqli_num_rows($result3) > 0){
                $dscek = mysqli_fetch_array($result3);
                $trans_id = $dscek['transaksi_point_id'];
                $updx = "update transaksi_point set is_claim = 'N' where transaksi_point_id = '$trans_id'";
                echo $updx;
                $res = mysqli_query($con,$updx);
            }
        }else if($sts == "Y" && $status != "Payment"){
            $upd = "update transaksi_point set is_claim = 'X' where jamaah_id = '$id'";
            $result2 = mysqli_query($con,$upd);
        }
        // echo $upd;
        $result5 = mysqli_query($con,$upd);
        $result = mysqli_query($con,$upd0);
        if($result){
            echo "<script>alert('Berhasil')</script>";
            echo "<script>window.location='../../?go=stok_pin'</script>";
        }
    }
    
?>