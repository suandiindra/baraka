<div class="card mb-4">
    <div class="table-responsive p-3">
        <table class="table align-items-center table-flush table-hover" id="tblAgent">
            <thead class="thead-light">
                <tr>
                <th>No</th>
                <th>Kode Agent</th>
                <th>Nama Agent</th>
                <th>Tipe Agent</th>
                <th>Agent Penginput</th>
                <th>ID Agent Penginput</th>
                <th>Tipe Agent Penginput</th>
                <th>Tgl Input</th>
                <th>Jenis Pembelian</th>
                <th>Petugas</th>
                <th>Status</th>
                <th style="width:200px">Action</th>
                </tr>
            </thead>
            <tbody id="dtlAgent">
            <tbody>
        </table>
    </div>
</div>

<script>
    // window.onload = function(){
    //     console.log('papa');

    //     lihat()
    // }
    window.addEventListener('load', lihat, false);

    function lihat(){
        console.log('papa');
        $.ajax({
            type:'POST',
            url:'pages/inputagent/inputagent_be.php',
            data:{
                find:true,
            },
            success: function(data){
                console.log(data);
                // return
                var obj = $.parseJSON(data);
                obj = obj.data;
                var table = $('#tblAgent').DataTable();
                table.clear().destroy(); 
                let dom = ``
                for(let i = 0; i < obj.length ; i++){
                    
                    if(obj[i].status == "Konfirmasi" && (obj[i].roles == "admin" || obj[i].roles == "Agent")){
                        obj[i].status = "Approved"
                    }
                    dom = dom + `<tr>
                        <td>${i+1}</td>
                        <td>${obj[i].nomor_agent ? obj[i].nomor_agent : "Belum Mendapat Nomor"}</td>
                        <td>${obj[i].nama_user}</td> 
                        <td>${obj[i].tipe_agent_penginput}</td>
                        <td>${obj[i].nama}</td>
                        <td>${obj[i].agent_penginput    }</td>
                        <td>${obj[i].tipe_agent == "" ? "Portal" : obj[i].tipe_agent}</td>
                        <td>${obj[i].dibuat}</td>
                        <td>${obj[i].tipe_bayar == "" ? "FREE" : obj[i].tipe_bayar}</td>
                        <td>${obj[i].action_user ? obj[i].action_user : ""}</td>
                        <td><div class="badge bg-${obj[i].status == "Approved" ? "success" : "danger"} text-white">${obj[i].status}</div></td>
                        <td>
                            <a href="./?go=agentdetail&id=${obj[i].transaksi_daftar_id}"><button class="btn btn-success">Detail</button></a>
                            ${obj[i].kode_status == "WT2" && obj[i].roles == "1" ? `<Button class='btn btn-warning' onclick="konfirmasi('${obj[i].transaksi_daftar_id}')">Konfirmasi</button>` : ""}
                        </td>
                    </tr>`
                }
                $('#dtlAgent').html(dom)
                $('#tblAgent').DataTable(({ 
                    "destroy": true, //use for reinitialize datatable
                }));
            }
        })
    }

    function konfirmasi(v){
        console.log(v);
        $.ajax({
            type:'POST',
            url:'pages/inputagent/inputagent_be.php',
            data:{
                konfirmasi:true,
                id:v
            },
            success: function(data){
                // console.log(data);
                if(data == "200"){
                    alert(`Berhasil...`)
                    lihat()
                }
            }
        })
    }
</script>