<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
<div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-body">
                <h4><b>Menu Input Data New Agent</b></h4>
                <hr>
                <a class="float-right" href="./?go=listnewagent"><u>List Pendaftaran Agent</u></a>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama Login</label>
                    <input type="text" maxlength="36" required  id="nama" class="form-control" value= "">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama KTP</label>
                    <input type="text" maxlength="36" required  id="nama_alias" class="form-control" value= "">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">NIK KTP</label>
                    <input type="text" required  id="nik" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tempat Lahir</label>
                    <input type="text"  id="tempat_lahir" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">TGL LAHIR</label>
                    <input type="date"  id="tgl_lahir" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Alamat KTP</label>
                    <textarea id="alamat_ktp" class="form-control" cols="10" rows="3"></textarea>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Alamat Domisili</label>
                    <textarea id="alamat_domisili" class="form-control" cols="10" rows="3"></textarea>
                </div>
                <br>
                <br><hr>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">No Rekening</label>
                    <input type="text" required  id="norek" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama Bank</label>
                    <input type="text" required  id="bank" class="form-control" value= "" >
                </div>
                <br>
                <br><hr>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">NPWP</label>
                    <input type="text" required  id="npwp" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Email</label>
                    <input type="text" required  id="email" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">HP</label>
                    <input type="number" required  id="hp" class="form-control" value= "" >
                </div>
            </div>
            <div class="card-body">
                <div class="bg-primary text-white" style="padding:5px; border-radius:5px">
                    <b><h4>Data Ahli Waris</h4></b>
                </div>
                <br>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Ahli Waris</label>
                    <input type="text" required  id="ahli_waris" class="form-control" value= "" >
                </div>
                <!-- <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Hubungan</label>
                    <input type="text" required  id="hubungan_ahli_waris" class="form-control" value= "" >
                </div> -->
                <div class="form-group">
                    <label>Hubungan Ahli Waris</label>
                   <select name="hub"  id="hubungan_ahli_waris" class="form-control" >
                        <option value=""></option>
                        <option value="Suami">Suami</option>
                        <option value="Istri">Istri</option>
                        <option value="Anak">Anak</option>
                        <option value="Saudara Kandung">Saudara Kandung</option>
                        <option value="Orang Tua">Orang Tua</option>
                        <option value="Sepupu">Sepupu</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tlp Ahli Waris</label>
                    <input type="text" required  id="tlp_ahli_waris" class="form-control" value= "" >
                </div>
                <!-- identitas penginput -->
                <br>
                <br>
                <div class="bg-primary text-white" style="padding:5px; border-radius:5px">
                    <b><h4>Identitas Penginput</h4></b>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama Penginput</label>
                    <input type="text" readonly  id="penginput" class="form-control" value= "<?php echo $_SESSION['nama'] ?>" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">ID Agnet</label>
                    <input type="text" readonly  id="id_agent_penginput" class="form-control" value= "<?php echo $_SESSION['nomor_agent'] ?>" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tipe Agent</label>
                    <input type="text" required  id="tipeagent" class="form-control" value="" readonly >
                </div>
                <br>
                <br>
                <!-- data pilihan agent  -->
                <div class="bg-primary text-white" style="padding:5px; border-radius:5px">
                    <b><h4>Data Pilihan Agent</h4></b>
                </div>
                <div class="mb-3" style="display:none">
                    <label for="inputPassword5" class="form-label">No ID Agent</label>
                    <input type="text" required  id="id_agent_user" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tipe Agent User</label>
                    <select name="" id="tipe_agent_user" class="form-control" onchange="cektipe(this)">
                        <option value=""></option>
                        <option value="MNN">Agent Baraka (MNN)</option>
                        <option value="MGB 1">Kepala Posko Agent MGB (MGB 1)</option>
                        <option value="MGB 2">Kepala Posko Agent MGB (MGB 2)</option>
                        <option value="MGB 3">Kepala Posko Agent MGB (MGB 3)</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" id="lbl_kode_agent" class="form-label">Kode Agent</label>
                    <input type="text" readonly  id="kode_agent_user" class="form-control" value= "" >
                </div>
                <div class="mb-3" id="groupbayar">
                    <label for="inputPassword5" class="form-label">Pilihan Lisensi Agent</label>
                    <select name="" id="lisensi_agent_user" class="form-control">
                        <option value=""></option>
                        <option value="Lisensi Daftar MGB">Lisensi Daftar MGB</option>
                        <option value="Pembelian PIN">Pembelian PIN</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label" id="labelfree">Input Bukti Transfer</label>
                    <input type="file" required  id="buktitransfer" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Insert KTP</label>
                    <input type="file" required  id="buktiktp" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Insert Dok Pernyataan</label>
                    <input type="file" required  id="buktifile" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Input Formulir MGB</label>
                    <input type="file" required  id="buktiformulir" class="form-control" value= "" >
                </div>
                <div id="notice" style="display:block">
                    <p class="text-primary">Kepala posko agent MGB Baraka, Wajib mengajukan permohonan menjadi kepala posko Agent MGB Baraka. (Permohonan menjadi kepala Posko MGB Baraka)</p>
                    <p class="text-success" style="margin-top:-10px">Konfirmasi pengiriman file via email atau ke nomor 
                        WA <b>0813-1913-8151</b></p>
                </div>



                <div>
                    <button class="btn btn-success" id="btnsimpan" onclick="konfirmasi(this)">Simpan Data</button>
                </div>
                
            </div>
        </div>
    </div>
</div>
</div>
<script>
    // window.onload = function(){
    //     console.log(<?php $_SESSION['nomor_agent']; ?>);
    // }
    window.addEventListener('load', load, false);
    function load(){
        $.ajax({
            type:'POST',
            url:'pages/inputagent/inputagent_be.php',
            data:{
                load:true
            },
            success: function(data){
                console.log(data);
                $(`#tipeagent`).val(data)
            }
        })
    }

    function cektipe(v){
        // console.log(v.value);
        if(v.value == "MNN"){
            $(`#kode_agent_user`).val('FLA1')
            // notice
            document.getElementById(`notice`).style.display = "none"
        }else if(v.value == "MGB 1"){
            document.getElementById(`notice`).style.display = "block"
            $(`#kode_agent_user`).val('FLA5')
        }else if(v.value == "MGB 2"){
            document.getElementById(`notice`).style.display = "block"
            $(`#kode_agent_user`).val('FLA8')
        }else if(v.value == "MGB 3"){
            document.getElementById(`notice`).style.display = "block"
            $(`#kode_agent_user`).val('FLA9')
        }else{
            document.getElementById(`notice`).style.display = "none"
            $(`#kode_agent_user`).val('')
        }
        // $(`#kode_agent_user`).css('display','none')
        document.getElementById('kode_agent_user').style.display = "none"
        document.getElementById('lbl_kode_agent').style.display = "none"

        if(v.value == "MNN"){
            document.getElementById("groupbayar").style.display = "none"
            document.getElementById("buktitransfer").style.display = "none"
            $(`#labelfree`).html(`FREE`)
        }else{
            $(`#labelfree`).html(`Input Bukti Transfer`)
            document.getElementById("buktitransfer").style.display = "block"
            document.getElementById("groupbayar").style.display = "block"
        }
    }
    function konfirmasi(t){
        // console.log(t.id);
        // cek disini
        let isvalid = []
        $(`#nama`).val() == "" ? isvalid.push("Nama Login") : null
        $(`#nama_alias`).val() == "" ? isvalid.push("Nama KTP") : null
        $(`#nik`).val() == "" ? isvalid.push("NIK") : null
        $(`#tgl_lahir`).val() == "" ? isvalid.push("Tgl Lahir") : null
        $(`#alamat_domisili`).val() == "" ? isvalid.push("Domisili") : null
        $(`#alamat_ktp`).val() == "" ? isvalid.push("alamat ktp") : null
        $(`#tempat_lahir`).val() == "" ? isvalid.push("Tempat Lahir") : null
        $(`#tgl_lahir`).val() == "" ? isvalid.push("Tgl Lahir") : null
        $(`#hp`).val() == "" ? isvalid.push("HP") : null
        $(`#bank`).val() == "" ? isvalid.push("BANK") : null
        $(`#norek`).val() == "" ? isvalid.push("Nomor Rekening") : null
        $(`#tlp_ahli_waris`).val() == "" ? isvalid.push("Tlp Ahli Waris") : null
        $(`#hubungan_ahli_waris`).val() == "" ? isvalid.push("Hubungan Ahli Waris") : null
        $(`#ahli_waris`).val() == "" ? isvalid.push("Ahli Waris") : null
        $(`#tipe_agent_user`).val() == "" ? isvalid.push("Tipe Agent") : null
        $(`#lisensi_agent_user`).val() == "" && $(`#tipe_agent_user`).val() !== "MNN" ? isvalid.push("Tipe Lisensi Baraka") : null
        
        console.log('sasd',$(`#lisensi_agent_user`).val());

        var buktiktp = $('#buktiktp').prop('files')[0]; 
        var buktifile = $('#buktifile').prop('files')[0]; 
        var buktiformulir = $('#buktiformulir').prop('files')[0]; 
        !buktiktp ? isvalid.push("Upload KTP") : null
        !buktifile ? isvalid.push("Upload Dok Pernyataan") : null
        !buktiformulir ? isvalid.push("Upload Formulir") : null

        if($(`#tipe_agent_user`).val() !== "MNN"){
            var buktitransfer = $('#buktitransfer').prop('files')[0]; 
            !buktitransfer ? isvalid.push("Bukti Transfer") : null
        }

        if(isvalid.toString().length > 0){
            alert(`Anda Belum melengkapi \n${isvalid.toString().replace(/,/g,' , ')}`)
            return
        }

        // return
        
        $(`#${t.id}`).html('Mohon Tunggu ...')
        $(`#${t.id}`).attr('disabled','disabled');
        // return
        var file_data_formulir = ``;
        var file_data_file = ``;
        var file_data_ktp = ``;
        var file_data_bukti = ``;

        file_data_formulir = $('#buktiformulir').prop('files')[0];   
        file_data_file = $('#buktifile').prop('files')[0];   
        file_data_ktp = $('#buktiktp').prop('files')[0]; 
        

        if($("#buktitransfer").val().length > 0){
            file_data_bukti = $('#buktitransfer').prop('files')[0];  
        }
        var form_data = new FormData();      
        form_data.append('simpan', true);
        form_data.append('nama', $(`#nama`).val());
        form_data.append('nik', $(`#nik`).val());
        form_data.append('alamat_ktp', $(`#alamat_ktp`).val());
        form_data.append('alamat_domisili', $(`#alamat_domisili`).val());
        form_data.append('norek', $(`#norek`).val());
        form_data.append('bank', $(`#bank`).val());
        form_data.append('npwp', $(`#npwp`).val());
        form_data.append('tempat_lahir', $(`#tempat_lahir`).val());
        form_data.append('email', $(`#email`).val());
        form_data.append('hp', $(`#hp`).val());
        form_data.append('penginput', $(`#penginput`).val());
        form_data.append('tipeagent', $(`#tipeagent`).val());
        form_data.append('id_agent_user', $(`#id_agent_user`).val());
        form_data.append('tipe_agent_user', $(`#tipe_agent_user`).val());
        form_data.append('kode_agent_user', $(`#kode_agent_user`).val());
        form_data.append('lisensi_agent_user', $(`#lisensi_agent_user`).val());
        form_data.append('ahli_waris', $(`#ahli_waris`).val());
        form_data.append('hubungan_ahli_waris', $(`#hubungan_ahli_waris`).val());
        form_data.append('tlp_ahli_waris', $(`#tlp_ahli_waris`).val());
        form_data.append('tgl_lahir', $(`#tgl_lahir`).val());
        form_data.append('nama_alias', $(`#nama_alias`).val());
        
        // form_data.append('file', file_data);
        if($("#buktitransfer").val().length > 0){
            var buktitransfer = $('#buktitransfer').prop('files')[0]; 
            form_data.append('buktitransfer', buktitransfer);
        }
        var buktiktp = $('#buktiktp').prop('files')[0]; 
        var buktifile = $('#buktifile').prop('files')[0]; 
        var buktiformulir = $('#buktiformulir').prop('files')[0]; 
        form_data.append('buktifile', buktifile);
        form_data.append('buktiktp', buktiktp);
        form_data.append('buktiformulir', buktiformulir);

        console.log(form_data);                            
        $.ajax({
            url: 'pages/inputagent/inputagent_be.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(res){
                $(`#${t.id}`).html('Simpan Data')
                console.log(res);
                if(res == "200"){
                    alert("Proses Berhasil..")
                    window.location = "./?go=listnewagent"
                }else{
                    if(res == "usia"){
                        alert("Batas usia tidak diizinkan")
                    }else{
                        console.log(`salah..`);
                        alert(res)
                    }
                }
                $(`#${t.id}`).removeAttr('disabled');
                $(`#${t.id}`).html('Simpan Data')
            }
        });
    }
</script>