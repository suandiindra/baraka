<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
<div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-body">
                <a class="float-right" href="./?go=listnewagent"><u>List Pendaftaran Agent</u></a>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tanggal Input</label>
                    <input type="text" maxlength="36" readonly  id="inputdate" class="form-control" value= "">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama Login</label>
                    <input type="text" maxlength="36" readonly  id="nama" class="form-control" value= "">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama KTP</label>
                    <input type="text" maxlength="36" readonly  id="nama_alias" class="form-control" value= "">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">NIK KTP</label>
                    <input type="text" readonly  id="nik" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tempat Lahir</label>
                    <input type="text" readonly id="tempat_lahir" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">TGL LAHIR</label>
                    <input type="date" readonly id="tgl_lahir" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Alamat KTP</label>
                    <textarea id="alamat_ktp" readonly class="form-control" cols="10" rows="3"></textarea>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Alamat Domisili</label>
                    <textarea id="alamat_domisili" readonly class="form-control" cols="10" rows="3"></textarea>
                </div>
                <!-- <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Alamat Domisili</label>
                    <textarea id="alamat_domisili" class="form-control" cols="10" rows="3"></textarea>
                </div> -->
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">No Rekening</label>
                    <input type="text" readonly  id="norek" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama Bank</label>
                    <input type="text" readonly  id="bank" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Npwp</label>
                    <input type="text" readonly  id="npwp" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Email</label>
                    <input type="text" readonly  id="email" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">HP</label>
                    <input type="number" readonly  id="hp" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Ahli Waris</label>
                    <input type="text" readonly  id="ahli_waris" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Hubungan</label>
                    <input type="text" readonly  id="hubungan_ahli_waris" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tlp Ahli Waris</label>
                    <input type="text" readonly  id="tlp_ahli_waris" class="form-control" value= "" >
                </div>
                <div class="mb-3" id="formpassword">
                    <label for="inputPassword5" class="form-label">Password</label>
                    <input type="text"  id="pwd" class="form-control" value= "" >
                </div>
                <!-- identitas penginput -->
                <div class="bg-primary text-white" style="padding:5px; border-radius:5px">
                    <b><h4>Identitas Penginput</h4></b>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama Penginput</label>
                    <input type="text" readonly  id="penginput" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">ID Agnet</label>
                    <input type="text" readonly  id="id_agent_penginput" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tipe Agent</label>
                    <input type="text" readonly  id="tipeagent" class="form-control"  >
                </div>
                <!-- data pilihan agent  -->
                <div class="bg-primary text-white" style="padding:5px; border-radius:5px">
                    <b><h4>Data Pilihan Agent</h4></b>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label" style="display:none">No ID Agent</label>
                    <input type="text" readonly  id="id_agent_user" class="form-control" value= "" style="display:none">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tipe Agent User</label>
                    <input type="text" readonly  id="tipe_agent_user" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Kode Agent</label>
                    <input type="text" readonly  id="kode_agent_user" class="form-control" value= "" >
                </div>
                <div class="mb-3" id="groupbayar">
                    <label for="inputPassword5" class="form-label">Pilihan Lisensi Agent</label>
                    <select name="" id="lisensi_agent_user" disabled class="form-control">
                        <option value=""></option>
                        <option value="Lisensi Daftar MGB">Lisensi Daftar MGB</option>
                        <option value="Pembelian PIN">Pembelian PIN</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label" id="labelfree">Download Bukti Transfer</label>
                    <div id="buktitransfer"></div>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Download KTP</label>
                    <div id="buktiktp"></div>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Download Dok Pernyataan</label>
                    <div id="buktifile"></div>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Download Forrmulir</label>
                    <div id="buktiformulir"></div>
                </div>
                <hr>
                <div id="konfirmdiv">
                    <button class="btn btn-success" id="btnsimpan" onclick="approve(this)">Konfirmasi</button>
                    <button class="btn btn-danger float-right" id="btnreject" onclick="reject(this)">Reject</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    let m_agent_id_ = ""    
    window.addEventListener('load', findOne, false);

    function reject(o){
        if(!confirm(`Yakin melanjutkan transaksi ini..?`)){
            return
        }
        $(`#${o.id}`).val('Please Wait...')
        var url_string = window.location.href
        var url = new URL(url_string);
        var id = url.searchParams.get("id");
        $.ajax({
            type:'GET',
            url:'pages/inputagent/inputagent_be.php',
            data:{
                reject:true,
                id : id
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    alert('Berhasil')
                    window.location = "./?go=listnewagent"
                }else{
                    alert(data)
                }
            }
        })
        $(`#${o.id}`).val('Konfirmasi')
    }
    function approve(o){
        if($(`#pwd`).val().length == 0){
            alert(`Sett default passwordnya dulu`)
            return
        }
        if(!confirm(`Yakin melanjutkan transaksi ini..?`)){
            return
        }
        $(`#${o.id}`).val('Please Wait...')
        var url_string = window.location.href
        var url = new URL(url_string);
        var id = url.searchParams.get("id");
        $.ajax({
            type:'GET',
            url:'pages/inputagent/inputagent_be.php',
            data:{
                approve:true,
                id : id,
                pwd : $(`#pwd`).val()
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    alert('Berhasil')
                    window.location = "./?go=listnewagent"
                }else if(data == "Pembelian PIN"){
                    console.log('PIN');
                    $.ajax({
                        type:'GET',
                        url:'pages/pembelian_pin/action_pembelian.php',
                        data:{
                            act: "1",
                            id : id,
                            rjm : "Y",
                            go: true
                        },
                        success: function(data){
                            console.log(data);
                            alert('Berhasil')
                            window.location = "./?go=listnewagent"
                        }
                    })
                }else{
                    alert(data)
                }
            }
        })
        $(`#${o.id}`).val('Konfirmasi')
    }
    function findOne(){
        var url_string = window.location.href
        var url = new URL(url_string);
        var id = url.searchParams.get("id");
        $.ajax({
            type:'GET',
            url:'pages/inputagent/inputagent_be.php',
            data:{
                findOne:true,
                id : id
            },
            success: function(data){
                let obj = JSON.parse(data)
                obj = obj.data[0]
                // console.log(obj);
                m_agent_id_ = obj.m_agent_id;
                $(`#nama`).val(obj.nama)
                $(`#nik`).val(obj.ktp)
                $(`#tgl_lahir`).val(obj.tgl_lahir)
                $(`#alamat_domisili`).val(obj.alamat)
                $(`#alamat_ktp`).val(obj.alamat_ktp)
                $(`#norek`).val(obj.nomor_rekening)
                $(`#bank`).val(obj.bank)
                $(`#npwp`).val(obj.npwp)
                $(`#email`).val(obj.email)
                $(`#hp`).val(obj.hp)
                $(`#ahli_waris`).val(obj.nama_ahli_waris)
                $(`#hubungan_ahli_waris`).val(obj.hubungan)
                $(`#tlp_ahli_waris`).val(obj.nomor_hp)
                $(`#id_agent_user`).val(obj.nomor_agent)
                $(`#tipe_agent_user`).val(obj.tipe_agent)
                $(`#kode_agent_user`).val(obj.kode_agent)
                $(`#lisensi_agent_user`).val(obj.tipe_bayar)
                $(`#tipe_agent`).val("xxxxx")
                $(`#inputdate`).val(obj.tgl)
                $(`#nama_alias`).val(obj.nama_alias)
                $(`#tempat_lahir`).val(obj.tempat_lahir)
                $(`#tipeagent`).val(obj.tipe_agent_penginput ? obj.tipe_agent_penginput : "Portal")

                console.log(obj.tipe_agent_penginput,'xxxxxxxxxxxxx');
                $(`#penginput`).val(obj.penginput)
                $(`#id_agent_penginput`).val(obj.id_agent_penginput ? obj.id_agent_penginput : "")
                
                if(obj.tipe_agent == "MNN"){
                    document.getElementById('groupbayar').style.display = "none"
                }
                // groupbayar

                // a.dokumen,a.bukti_ktp,a.bukti_file,a.bukti_formulir
                if(obj.dokumen.length > 0){
                    let btn1 = `<a href="${obj.dokumen}"><button class="btn btn-primary">Download</button></a>`
                    $(`#buktitransfer`).html(btn1)
                }
                let btn2 = `<a href="${obj.bukti_ktp}"><button class="btn btn-primary">Download</button></a>`
                $(`#buktiktp`).html(btn2)
                let btn3 = `<a href="${obj.bukti_file}"><button class="btn btn-primary">Download</button></a>`
                $(`#buktifile`).html(btn3)
                let btn4 = `<a href="${obj.bukti_formulir}"><button class="btn btn-primary">Download</button></a>`
                $(`#buktiformulir`).html(btn4)

                console.log(obj.role,'xxxxxxxxxxx');
                if(obj.role == "1" || obj.role == "admin"){
                    obj.kode_status == "WT1" ? $(`#konfirmdiv`).show() : $(`#konfirmdiv`).hide()
                    obj.kode_status == "WT1" ? $(`#formpassword`).show() : $(`#formpassword`).hide()
                }
                if(obj.role == "Agent"){
                    $(`#konfirmdiv`).hide()
                    $(`#formpassword`).hide()
                }

            }
        })
    }
</script>