<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Admin Baraka</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Admin Baraka</li>
        </ol>
    </div>
    <?php
        $nama_lengkap = "";
        $ktp = "";
        $alamat = "";
        $email = "";
        $hp = "";
        $bank = "";
        $rekening = "";
        $ahliwaris = "";
        $hp_ahliwaris = "";
        $hubungan = "";   
        $tipe = "add";
        $label = "Tambahkan";
        $key = "";
        $password = "";
        $id_agent = "";
        $roles = "";
        if(isset($_GET["id"])){
            $id = $_GET["id"];
            $key = $id;
            $sel = "select * from m_user where m_user_id = '$id'";
            $result = mysqli_query($con,$sel);
            if($result){
                $tipe = "edit";
                $label = "Edit";
                $data = mysqli_fetch_array($result);
                $id_agent = $data['m_user_id'];
                $nama_lengkap = $data['nama'];
                $email = $data['email'];
                $roles = $data['role'];
                $hp = $data['hp'];
            }
        }
    ?>
    <div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Form data diri agent diisi dan di kelola oleh Admin
                    <hr>
            </div>

            <div class="card-body">
            <Form method="POST" action="pages/admin/actionadmin.php">
                <input type="hidden"  name="tp" value="<?php echo $tipe?>" class="form-control">
                <input type="hidden"  name="key" value="<?php echo $key?>" class="form-control">
                <div class="overflow-auto" style="height:700px">
               
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">* Nama</label>
                    <input type="text" required  name="nama" class="form-control" value= "<?php echo $nama_lengkap ?>" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">* No Handphone</label >
                    <input type="text" required  name="hp" class="form-control" value= "<?php echo $hp ?>">
                </div>
                <!-- <div class="mb-3">
                    <label for="inputPassword5" class="form-label">* Roles</label >
                    <select name="roles" class="form-control" id="roles">
                        <option value="<?php echo $roles ?>"><?php echo $roles ?></option>
                        <option value="1">Direktur Utama</option>
                        <option value="administrator">Administrator</option>
                        <option value="admin">Admin</option>
                    </select>
                </div> -->
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Email</label>
                    <input type="text"  name="email" class="form-control" value= "<?php echo $email ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">* Password</label>
                    <input type="text"  name="pwd" class="form-control" value= "<?php echo $bank ?>">
                </div>
                
                <?php
                    if($_SESSION['role'] == "Agent"){
                        
                    }else{
                ?>
                    <Button class="btn btn-success">
                            <?php echo $label; ?>
                    </Button>
                <?php
                    }
                ?>
            </form>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>