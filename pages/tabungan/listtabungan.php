<div class="card mb-4">
    <div class="row" style="margin-left: 10px;margin-top:10px">
        <div class="col col-md-12">
            <div class="row">
                <div class="col col-md-3">
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Periode</label>
                        <input type="date"  id="tgl1" class="form-control" value= "" >
                    </div>
                </div>
                <div class="col col-md-3">
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Periode</label>
                        <input type="date"  id="tgl2" class="form-control" value= "" >
                    </div>
                </div>
                <div class="col col-md-2">
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Tipe Transaksi</label>
                        <select name="" id="tipe" class="form-control">
                            <option value="1">Cicilan Tabungan</option>
                            <option value="2">Pembayaran</option>
                        </select>
                    </div>
                </div>
                <div class="col col-md-2">
                    <button class="btn btn-success" style="margin-top:35px" onclick="loads()">Lihat</button>
                </div>
                <div class="col col-md-2">
                    <p style="margin-top:35px; margin-right:20px" class="float-right text-danger"><b id="nominal"></b></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col col-md-12">
            <div class="table-responsive p-3">
                <table class="table align-items-center table-flush table-hover" id="tblTabungan">
                    <thead class="thead-light">
                        <tr>
                            <th><i class="fa fa-cogs"></i></th>
                            <th>Tgl Nabung</th>
                            <th>Id Jamaah</th>
                            <th>Nomor VA</th>
                            <th>Nomor Resi</th>
                            <th>Nama Jamaah</th>
                            <th>Petugas</th>
                            <th>Nominal</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="dtlTabungan">
                    <tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    var amount = 0;
    window.addEventListener('load', loads, false);
    function loads(){
        $.ajax({
            type:'POST',
            url:'pages/tabungan/listtabungan_be.php',
            data:{
                find:true,
                tgl1 : $(`#tgl1`).val(),
                tgl2 : $(`#tgl2`).val(),
                tipe : $(`#tipe`).val(),
            },
            success: function(data){
                
                let obj = JSON.parse(data)
                obj = obj.data
                
                let dom = ``
                let amt = 0
                var table = $('#tblTabungan').DataTable();
                table.clear().destroy(); 
                for(let i = 0; i < obj.length; i++){
                    let cls = ``
                    if(obj[i].ket == "Reject"){
                        cls = ` class = "bg-secondary text-white" `
                    }
                    amt = amt + parseFloat(obj[i].nominal_nabung)
                    dom = dom + `<tr ${cls}>
                        <td><i class='fa fa-eye text-success' onclick="detail('${obj[i].jemaah_id}')"></i></td>
                        <td>${obj[i].nabung}</td>
                        <td>${obj[i].nomor_jemaah}</td>
                        <td>${obj[i].no_va}</td>
                        <td>${obj[i].nomor_resi}</td>
                        <td>${obj[i].nama_jemaah}</td>
                        <td>${obj[i].petugas}</td>
                        <td>${duit(obj[i].nominal_nabung)}</td>
                        <td>
                            ${obj[i].roles == "1" ? `${obj[i].ket == "Terverifikasi" && obj[i].roles == "1" ? `<button class="btn btn-warning" onclick="kofirmasi('${obj[i].trans_tabungan_id}')">Konfirmasi</button>
                            <button class="btn btn-danger" onclick="reject('${obj[i].trans_tabungan_id}')">Reject</button>` : `<div class='badge bg-success text-white'>${obj[i].ket }</div>`}`:`${obj[i].ket == "Konfirmasi" ? "<p class='badge bg-success text-white'>Konfirmasi</p>" : ""}`}
                        </td>
                    </tr>`
                }
                $('#dtlTabungan').html(dom)
                // console.log(obj);
                // return
                $(`#nominal`).html(duit(amt))
                $('#tblTabungan').DataTable(({ 
                    "destroy": true, //use for reinitialize datatable
                }));
            }
        })
    }
    function kofirmasi(v){
        $.ajax({
            type:'POST',
            url:'pages/tabungan/listtabungan_be.php',
            data:{
                konfirm:true,
                id : v,
                tipe : $(`#tipe`).val()
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    alert("Berhasil")
                    loads()
                }
            }
        })
    }
    function reject(v){
        if(!confirm(`Yakin melakukan proses.. ?`)){
            return
        }
        $.ajax({
            type:'POST',
            url:'pages/tabungan/listtabungan_be.php',
            data:{
                reject:true,
                id : v,
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    alert(`Berhasil`)
                    loads()
                }   
            }
        })
    }
    function duit(v){
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join('.').split('').reverse().join('');

        return ribuan
    }
    function detail(v){
        console.log(v);
    }
</script>