<?php
    session_start();
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    $m_agent_id_ = $_SESSION['m_agent_id'];
    $nomor_agent_ = $_SESSION['nomor_agent'];
    $role_ = $_SESSION['role'];
    $nama_ = $_SESSION['nama'];
    if(isset($_POST['konfirm'])){
        $id = $_POST['id'];
        $tipe = $_POST['tipe'];
        if($tipe == 1){
            $upd = "update trans_tabungan set keterangan = 'Konfirmasi' where trans_tabungan_id = '$id'";
        }else{
            $upd = "update pembayaran_jemaah set status = 'Konfirmasi' where pembayaran_jemaah_id = '$id'";
        }
        $res = mysqli_query($con,$upd);
        if($res){
            echo "200";
        }else{
            echo $upd;
        }
    }
    if(isset($_POST['reject'])){
        $id = $_POST['id'];
        $upd = "update trans_tabungan set kode_status = null,status_nabung = null 
                ,keterangan = 'Reject', nominal_nabung = 0  where trans_tabungan_id = '$id'";
        $res = mysqli_query($con,$upd);
        if($res){
            echo "200";
        }else{
            echo $upd;
        }

    }
    if(isset($_POST['find'])){
        $tgl1 = $_POST['tgl1'];
        $tgl2 = $_POST['tgl2'];
        $tipe = $_POST['tipe'];
        $filter = "";
        if(strlen($tgl1) > 2){
            $filter = $filter." and date_format(tgl_nabung ,'%Y-%m-%d') between '$tgl1' and '$tgl2' ";
        }
        if($tipe == "1"){
            $sel = "select tt.jemaah_id,tgl_nabung
                ,date_format(tgl_nabung ,'%d %M, %y') as nabung,b.no_va,b.nomor_resi,b.nama_jemaah 
                ,nominal,nominal_nabung,biaya_admin 
                ,tt.petugas ,petugas2 ,b.nomor_jemaah,trans_tabungan_id,coalesce(tt.keterangan,'') as ket
                ,'$role_' as roles
                from trans_tabungan tt 
                inner join jemaah b on tt.jemaah_id = b.jemaah_id 
                where 1=1 and tgl_nabung is not null $filter order by tgl_nabung desc";
        }else{
            $sel = "select j.*,b.pembayaran_jemaah_id as trans_tabungan_id,nominal as nominal_nabung,kode_status,doc,jenis 
                ,date_format(b.createdate ,'%d %M, %y') as nabung,'$role_' as roles
                ,b.status as ket
                from jemaah j 
                inner join pembayaran_jemaah b on j.jemaah_id = b.jemaah_id 
                order by createdate desc";
        }

        // echo $sel;
        echo queryJson($con,$sel);
        
    }
?>