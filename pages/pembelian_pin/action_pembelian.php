<?php
    session_start();
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    $tipe = "";
    $id_uniq = "select uuid() as id";
    $result = mysqli_query($con,$id_uniq);
    $id_uniq = mysqli_fetch_array($result);
    $id_uniq = $id_uniq["id"];
    $_user = $_SESSION["m_agent_id"];
    $_nomor_agent = $_SESSION["nomor_agent"];
    $_nama = $_SESSION["nama"];

    if(isset($_POST['konfirm'])){
        $id = $_POST['id'];
        $upd = "update transaksi_agent set kode_status = 'WT3' where transaksi_agent_id = '$id'";
        $rex = mysqli_query($con,$upd);
        if($rex){
            echo "200";
        }else{
            echo $upd;
        }
    }
    if(isset($_POST['pin'])){
        // echo $_SESSION["m_agent_id"]."--".$_SESSION["nama"]."--".$_SESSION["nomor_agent"];
        $jml = $_POST['pin'];
        $nomor_transaksi = rand(100000,10000);
        $nominal_topup = $jml * 2000000;

        $cek1 = "select * from transaksi_agent where m_agent_id = '$_user' and kode_status not in ('WT2','0','WT3')";
        // echo $cek1;
        $result1 = mysqli_query($con,$cek1);
        if(mysqli_num_rows($result1) > 0){
            echo "<script>alert('ada transaksi yang belum selesai')</script>";
            // echo "<script>window.location='../../?go=pembelian'</script>";
        }else{

            // $cek2 = "select * from jemaah where m_agent_id = '$_user' and status_jemaah = 'Draft'";

            // echo $cek2;
            // $result2 = mysqli_query($con,$cek2);
            // if(mysqli_num_rows($result2) > 0){
            //     echo "<script>alert('ada pin yang belum terpakai')</script>";
            //     echo "<script>window.location='../../?go=pembelian'</script>";
            // }else{
                $urutan = transApprove($con);
                $insert = "insert into transaksi_agent (transaksi_agent_id,m_agent_id,nomor_agent,nama_agent,nominal_topup,tgl_topup
                ,status_transaksi,kode_status,nomor_transaksi,jml_pin,urutan)
                values ('$id_uniq','$_user','$_nomor_agent','$_nama','$nominal_topup',now(),'Menunggu Bukti Transfer'
                ,'WT0','$nomor_transaksi',$jml,'$urutan')";
                $result = mysqli_query($con,$insert);
                if($result){
                    echo "<script>alert('Berhasil')</script>";
                    echo "<script>window.location='../../?go=pembelian'</script>";
                }
            // }
        }
    }else if(isset($_POST['tipe'])){
        if($_POST['tipe'] == "batal"){
            $id = $_POST['id'];
            $del = "delete from transaksi_agent where transaksi_agent_id = '$id'";
            // echo $del;
            $result = mysqli_query($con,$del);
            if($result){
                echo "<script>alert('Berhasil')</script>";
                echo "<script>window.location='../../?go=pembelian'</script>";
            }
        }
    }else if(isset($_POST['id_bukti'])) {
        $id = $_POST['id_bukti'];
        $nama_file1 = $_FILES['bukti']['name'];
        $ukuran_file1 = $_FILES['bukti']['size'];
        $tipe_file1 = $_FILES['bukti']['type'];
        $tmp_file1 = $_FILES['bukti']['tmp_name'];
        $path1 = "../../asset/bukti_transfer/".$id."/".$nama_file1;
        if( is_dir($path1) === false )
        {
            mkdir("../../asset/bukti_transfer/".$id."/");
        }
        $pathData = "asset/bukti_transfer/".$id."/".$nama_file1;
        // echo $pathData;
        if($ukuran_file1 <= 1000000){		
            if(move_uploaded_file($tmp_file1, $path1)){
                $upd = "update transaksi_agent set bukti_topup = '$pathData' 
                ,status_transaksi = 'Menunggu Approval', kode_status = 'WT1'
                ,confirm_date = now() where transaksi_agent_id = '$id'";
                $result = mysqli_query($con,$upd);
                if($result){
                    echo "<script>alert('Berhasil')</script>";
                    echo "<script>window.location='../../?go=pembelian'</script>";
                }
            }
        }
    }else if(isset($_GET['act'])){
        if($_GET['act'] == "1"){

     

            $id = $_GET['id'];
            $rjm = isset($_GET['rjm']);

            $sel = "select * from transaksi_daftar where transaksi_daftar_id = '$id'";
            
            $ro = mysqli_query($con,$sel);
            if(mysqli_num_rows($ro) > 0){
                if(isset($_GET['direct'])){
                    echo "<script>alert('Approval harus dilakukan di menu verifikasi Agent')</script>";
                    echo "<script>window.location='../../?go=pembelian'</script>";
                    return;
                }
            }

            $sel = "select * from transaksi_upgrade where transaksi_upgrade_id = '$id'";
            
            $ro = mysqli_query($con,$sel);
            if(mysqli_num_rows($ro) > 0){
                if(isset($_GET['direct'])){
                    echo "<script>alert('Approval harus dilakukan di menu verifikasi Upgrade Agnet')</script>";
                    echo "<script>window.location='../../?go=pembelian'</script>";
                    return;
                }
            }

            // exit;

            $cek = "select * from transaksi_agent where transaksi_agent_id = '$id'";
            $resultcek = mysqli_query($con,$cek);
            $res = mysqli_fetch_array($resultcek);
            if($res['kode_status'] != "WT1"){
                echo "<script>alert('Proses ditolak..')</script>";
                echo "<script>window.location='../../?go=pembelian'</script>";
    
                return;
            }
            
            $nomor_agent = $res['nomor_agent'];

            // echo $nomor_agent;
            // exit;

            $urutan = transApprove($con);
            $upd = "update transaksi_agent set is_confirm_topup = 'Y' ,kode_status = 'WT2'
            , status_transaksi = 'Approved',admin = '$_nama' where transaksi_agent_id = '$id'";

            // echo $upd;
            $result = mysqli_query($con,$upd);
            if($result){
                $cek = "select * from transaksi_agent where transaksi_agent_id = '$id'";
                $dt = mysqli_fetch_array(mysqli_query($con,$cek));
                $pin = $dt['jml_pin'];
                $m_agent_id = $dt['m_agent_id'];
                $no_agent = $dt['nomor_agent'];
                for($i = 1; $i <= $pin; $i++){
                    $sel = "select uuid() as id, SUBSTRING(replace(uuid(),'-',''),1,16) as key_";
                    $result = mysqli_query($con,$sel);
                    $uniq = mysqli_fetch_array($result);
                    $uniq_ = $uniq["id"];
                    $key_ = $uniq["key_"];
                    if($rjm == "Y"){
                        $resi = cekUrutanResi2($con);
                    }else{
                        if(strlen($nomor_agent) == 6){
                            $resi = cekUrutanResi2($con);
                        }else{
                            $resi = cekUrutanResi($con);
                        }
                    }

                    

                    $id_wisatawan = idwisatawan($con,$m_agent_id,$no_agent);
                    // echo $resi." - ".$id_wisatawan." - ".$no_agent." \n ";
                    $id_va = str_replace("JU","",$id_wisatawan);
                    
                    $masuk = "insert into jemaah (jemaah_id,status_jemaah,transaksi_agent_id,m_agent_id,nomor_resi,uniq_key
                    ,nomor_agent,nomor_jemaah,no_va)
                    values ('$uniq_','Draft','$id','$m_agent_id','$resi','$key_','$_nomor_agent','$id_wisatawan','$id_va')";
                    
                    $result = mysqli_query($con,$masuk);
                }
                if(isset($_GET['go'])){
                    echo "200";
                }else{
                    echo "<script>alert('Berhasil')</script>";
                    echo "<script>window.location='../../?go=pembelian'</script>";
                }
            }
        }else{
            $id = $_GET['id'];
            $cek = "select * from transaksi_agent where transaksi_agent_id = '$id'";

            $sel = "select * from transaksi_daftar where transaksi_daftar_id = '$id'";
            
            $ro = mysqli_query($con,$sel);
            if(mysqli_num_rows($ro) > 0){
                if(isset($_GET['direct'])){
                    echo "<script>alert('Reject harus dilakukan di menu verifikasi Agent')</script>";
                    echo "<script>window.location='../../?go=pembelian'</script>";
                    return;
                }
            }

            $sel = "select * from transaksi_upgrade where transaksi_upgrade_id = '$id'";
            
            $ro = mysqli_query($con,$sel);
            if(mysqli_num_rows($ro) > 0){
                if(isset($_GET['direct'])){
                    echo "<script>alert('Approval harus dilakukan di menu verifikasi Upgrade Agnet')</script>";
                    echo "<script>window.location='../../?go=pembelian'</script>";
                    return;
                }
            }

            // echo $cek;
            $resultcek = mysqli_query($con,$cek);
            $res = mysqli_fetch_array($resultcek);
            if($res['kode_status'] != "WT1"){
                echo "<script>alert('Proses ditolak...')</script>";
                echo "<script>window.location='../../?go=pembelian'</script>";
    
                return;
            }
            
            $upd = "update transaksi_agent set is_confirm_topup = 'N' ,kode_status = '0'
            , status_transaksi = 'Ditolak',admin = '$_nama' where transaksi_agent_id = '$id'";

            // echo $upd;
            $result = mysqli_query($con,$upd);
            if($result){
                echo "<script>alert('Pembelian PIN Ditolak')</script>";
                echo "<script>window.location='../../?go=pembelian'</script>";
            }
        }
    }

?>