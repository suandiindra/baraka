<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">List Pembelian PIN</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">List Pembelian</li>
    </ol>
</div>
<?php
        $filterTabahan = "";
        $filter_id = "";
        if($_SESSION['role'] == "Agent"){
            $id = $_SESSION['m_agent_id'];
            $filterTabahan = $filterTabahan." and a.m_agent_id = '$id' "; 
            $filter_id = " and a.m_agent_id = '$id' "; 
        }
        
        if(isset($_POST['lihat'])){
            if($_POST['randcheck']==$_SESSION['rand']){
                $date1         = $_POST['date1'];
                $date2         = $_POST['date2'];
                $m_agent_id    = $_POST['agent'];
                if(strlen($m_agent_id) > 2){
                    $filterTabahan = $filterTabahan." and a.m_agent_id = '$m_agent_id' "; 
                }
                if(strlen($date1) > 2 && strlen($date2) > 2){
                    $filterTabahan = $filterTabahan." and DATE_FORMAT(tgl_topup, '%Y-%m-%d') between '$date1' and '$date2' ";
                }
                
                
            }
        }
        
    ?>
<div class="card" style="margin-bottom:20px">
            <div class="card-body">
            <form action="" method="POST">
            <?php
                        $rand=rand();
                        $_SESSION['rand']=$rand;
                ?> 
                <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" /> 
                <div class="container col-12" style="margin-top:0px">
                <div class="row" style="margin-bottom:20px">
                    <div class="col-sm">
                        <label>Periode Beli</label>
                        <input type="date" class="form-control" name="date1"/>
                    </div>
                    <div class="col-sm">
                        <label>Periode Beli</label>
                        <input type="date" class="form-control" name="date2"/>
                    </div>
                    <div class="col-sm">
                        <label>Agent</label>
                        <select class="form-control" name="agent">
                            <option value=""></option>
                        <?php
                            $val = "select * from m_agent a where 1=1 $filter_id 
                            order by nama asc";
                            $result = mysqli_query($con,$val);
                            while($res=mysqli_fetch_array($result)){
                        ?>
                            <option value="<?php echo $res['m_agent_id'] ?>"><?php echo $res['nama'] ?></option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>
                    <div class="col-sm" style="margin-top:32px">
                        <Button class="btn btn-success" name="lihat" >Lihat</Button>
                        <Button class="btn btn-danger" name="export">Export</Button>
                    </div>
                </div>
                </div>
            </form>
            
            </div>
    </div>
<div class="row">
            <div class="col-lg-12">
              <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                </div>
                <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHovers">
                    <thead class="thead-light">
                      <tr>
                        <th>Nomor Transaksi</th>
                        <th>ID Agent</th>
                        <th>Nama Agent</th>
                        <th>Bank</th>
                        <th>No Rekening</th>
                        <th>Nominal</th>
                        <th>Status</th>
                        <th>PIN</th>
                        <th>Tgl Pengajuan</th>
                        <th>Tgl Upload</th>
                        <th>Bukti</th>
                        <th>Petugas</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                   
                        <?php
                            $sel = "select a.*,b.bank,b.nomor_rekening,DATE_FORMAT(tgl_topup,'%d-%M-%Y') as tgl_topup1 
                            ,DATE_FORMAT(confirm_date,'%d-%M-%Y') as confirm_date1,admin
                            from transaksi_agent a
                            inner join m_agent b on a.m_agent_id = b.m_agent_id
                            where kode_status <> 'WT0' $filterTabahan  order by tgl_topup desc";

                            // echo $sel;
                            $result = mysqli_query($con,$sel);
                            while($res = mysqli_fetch_array($result)){
                        ?>
                            <tr>
                                <td><?php echo $res['urutan']; ?></td>
                                <td><?php echo $res['nomor_agent']; ?></td>
                                <td><?php echo $res['nama_agent']; ?></td>
                                <td><?php echo $res['bank']; ?></td>
                                <td><?php echo $res['nomor_rekening']; ?></td>
                                <td><?php echo number_format($res['nominal_topup']); ?></td>
                                <td><?php echo $res['status_transaksi']; ?></td>
                                <td><?php echo $res['jml_pin']; ?></td>
                                <td><?php echo $res['tgl_topup1'] ?></td>
                                <td><?php echo $res['confirm_date1']; ?></td>
                                <td><a href="<?php echo $res['bukti_topup']; ?>">Lihat Bukti</a></td>
                                <td><?php echo $res['admin'] ?></td>
                                <td style="text-align:center">
                                <?php
                                    if(($res['kode_status'] == "WT2" || $res['kode_status'] == "0") && $_SESSION['role'] == "1"){
                                ?>
                                    <button class="btn btn-primary" onclick="kofirmasi(`<?php echo $res['transaksi_agent_id'] ?>`)">Konfirmasi</button>
                                   <?php
                                    }else if($res['kode_status'] == "WT3"){
                                        echo "<p class='badge bg-warning text-white'>Konfirm</p>";
                                    }else if($res['kode_status'] == "0" || $res['kode_status'] == "WT2"){
                                        
                                    }else{
                                ?>
                                        <a onclick='javascript:confirmationDelete($(this));return false;' href="./pages/pembelian_pin/action_pembelian.php?act=1&direct=1&id=<?php echo $res['transaksi_agent_id']; ?>"><button class="btn btn-success">Approve</button></a>
                                        <a onclick='javascript:confirmationDelete($(this));return false;' href="./pages/pembelian_pin/action_pembelian.php?act=0&direct=1&id=<?php echo $res['transaksi_agent_id']; ?>"><button class="btn btn-danger">Reject</button></a>
                                <?php
                                    }
                                ?>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
    </div>
    <script>
        function confirmationDelete(anchor){
            var conf = confirm('Apakah yakin melakukan proses ini ???');
            if(conf)
                window.location=anchor.attr("href");
        }
        function kofirmasi(v){
          let id = v
          $.ajax({
            type:'POST',
            url:'pages/pembelian_pin/action_pembelian.php',
            data:{
                konfirm:true,
                id : v
            },
            success: function(data){
                console.log(data);
                alert('Berhasil Konfirmasi')
                window.location = "./?go=pembelian"
            }
        })

        }
    </script>