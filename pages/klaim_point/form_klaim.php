<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Point Klaim</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Point Klaim</li>
        </ol>
    </div>
    <?php
        
    ?>
    <div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Form Klaim Point Agent
                    <hr>
            </div>

            <div class="card-body">
            <Form method="POST" action="pages/klaim_point/actionklaim.php">
                <input type="hidden"  name="tp" value="add" class="form-control">
                <!-- <input type="hidden"  name="key" value="<?php echo $key?>" class="form-control"> -->
                <div class="overflow-auto" style="height:700px">
               
                <div class="mb-3">
                        <label>Nama Agent</label>
                        <select class="form-control" id="agent" name="agent" onChange="showpoint()">
                            <option value=""></option>
                        <?php
                            $val = "select * from m_agent a where 1=1 $filter_id order by nama asc";
                            $result = mysqli_query($con,$val);
                            while($res=mysqli_fetch_array($result)){
                        ?>
                            <option value="<?php echo $res['m_agent_id'] ?>"><?php echo $res['nama'] ?></option>
                        <?php
                            }
                        ?>
                        </select>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">* Point Sekarang</label >
                    <input type="text"  name="points" id="point" disabled class="form-control" value= "">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">* Point Klaim</label >
                    <input type="number" required  name="pointklaim" id = "pointklaim" class="form-control" value= "" onchange="periksa()">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">* Tgl Klaim</label >
                    <input type="date" required  name="tgl" class="form-control" value= "">
                </div>
                
                <Button class="btn btn-success" id="btn" disabled>
                     Proses
                </Button>
            </form>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>
<script>
    function showpoint(){
            var x = document.getElementById("agent").value;
            $.ajax({
                url: './pages/klaim_point/actionklaim.php',
                type: 'POST',
                data: {option: x},
                success: function(e) {
                    console.log(e);
                    document.getElementById("point").value = e;
                    document.getElementById("pointklaim").value = "0";
                    document.getElementById("btn").disabled = true;
                    document.getElementById("btn").className = "btn btn-success";
                }
            });
            // console.log(x)
    }

    function periksa(){
        var x = document.getElementById("pointklaim").value;
        var z = document.getElementById("point").value;
        var val = z - x
        console.log(val)
        if(val >= 0 && z > 0){
            console.log(z,x)
            document.getElementById("btn").disabled = false;
            document.getElementById("btn").className = "btn btn-danger";
        }else if(val < 0){
            console.log("xx")
            console.log(z<x)
            document.getElementById("btn").className = "btn btn-success";
            document.getElementById("btn").disabled = true;
        }
    }
</script>