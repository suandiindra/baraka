<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Klaim Point</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">List Klaim Point</li>
    </ol>
</div>
<div class="row">
            <!-- DataTable with Hover -->
            <div class="col-lg-12">
              <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <?php
                    $filter = "";
                    if($_SESSION['role'] == "Agent"){
                        $id = $_SESSION['m_agent_id'];
                        $filter = " and a.m_agent_id = '$id' ";
                    }else{
                ?>
                        <h6 class="m-0 font-weight-bold text-primary"><a href="./?go=formklaim">
                            <Button class="btn btn-facebook">+ Klaim Point</Button>
                        </a></h6>
                <?php
                    }
                ?>
                
                </div>
                <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>No</th>
                        <th>Nomor Agent</th>
                        <th>Nama</th>
                        <th>Klaim Point</th>
                        <th>Tanggal Klaim</th>
                        <th>Petugas</th>
                        <th style="width:200px">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sel = "select *,DATE_FORMAT(proses_date,'%d-%M-%Y') as proses_date1  from klaim_point a
                            inner join m_agent b on a.m_agent_id = b.m_agent_id where 1 =1  $filter
                            order by a.proses_date desc";
                            $result = mysqli_query($con,$sel);
                            $i = 1;
                            while($res = mysqli_fetch_array($result)){
                        ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $res['nomor_agent']; ?></td>
                                <td><?php echo $res['nama']; ?></td>
                                <td><?php echo $res['jumlah_klaim']; ?></td>
                                <td><?php echo $res['proses_date1']; ?></td>
                                <td><?php echo $res['admin']; ?></td>
                                <td style="text-align:center">
                                  <table>
                                    <tr>
                                    <?php
                                        if($_SESSION['role'] == "Agent"){

                                        }else{
                                    ?>
                                    <td  style="padding:0"><a onclick='javascript:confirmationDelete($(this));return false;' href="./pages/klaim_point/actionklaim.php?del=del&id=<?php echo $res['klaim_point_id']; ?>"><button class="btn btn-danger">Batal Klaim</button></a></td>
                                    <?php
                                        }
                                    ?>
                                    </tr>
                                  </table>
                                </td>
                            </tr>
                        <?php
                            $i += 1;
                            }
                        ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <script>
            function confirmationDelete(anchor)
            {
            var conf = confirm('Apakah yakin melakukan proses ini ???');
            if(conf)
                window.location=anchor.attr("href");
            }
        </script>