<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Pembelian PIN</li>
    </ol>
    </div>
    <?php
        $filter = "  ";
        if($_SESSION['role'] == "agent"){
            $filter = " and jenis = 'Agent' ";
        }else if($_SESSION['role'] == "jamaah"){
            $filter = " and jenis = 'Wisatawan' ";
        }else{
            // $filter = " and jenis = 'Agent' ";

        }
        $sel = "select *,DATE_FORMAT(created,'%d-%M-%Y %h:%i') as crt  from informasi
         where isactiv = 1 $filter order by created desc";
        $result = mysqli_query($con,$sel);
        while($res = mysqli_fetch_array($result)){
        $id = $res['informasi_id'];
    ?>
    <div class="col-lg-12">
        <div class="col-xl-12 col-lg-7 mb-4">
            <div class="card">
                <div class="card-header" >
                    <b><h4><?php echo $res['judul'] ?></h4></b>
                    <p class="badge bg-success text-white"><?php echo $res['jenis'] ?></p>
                    <?php
                        if(strlen($res['lampiran'])>2){
                    ?>
                        <div style="color:green">
                            <a href="<?php echo $res['lampiran'] ?>"> <i class="fa fa-paperclip" aria-hidden="true"> Lampiran</i></a><br>
                            
                        </div>
                    <?php
                        }
                    ?>
                    <div style="float:right; color:red; margin-top:-30px">
                    
                    <?php 
                        echo $res['crt'];
                    ?>
                    </div>
                </div>
                <hr>
                <div class="card-body">
                <div class="row">
                    <div class="col">
                        <p>
                            <?php 
                                echo $res['isi'];
                            ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php
                    if( $_SESSION['role'] == "1"){
                ?>
                <div class="card-footer">
                 <a href="./pages/info/action_info.php?act=del&id=<?php echo $id; ?>"><Button class="btn btn-danger">Hapus</Button></a>
                </div>
                <?php
                    }
            ?>
        </div>
    </div>
    </div>
    <?php
    }
    ?>
</div>
