<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"></h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Informasi</li>
    </ol>
    </div>
    <?php
        $txt = "";
        $sel = "select * from info_jalan";
        $result = mysqli_query($con,$sel);
        while($res = mysqli_fetch_array($result)){
        $txt = $res['nama'];
        }
    ?>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col col-md-3">
                            <div class="mb-3">
                                <label for="inputPassword5" class="form-label">Tlp Admin MBT</label>
                                <input type="text" required  id="tlpmbt" class="form-control" value= "" >
                            </div>
                        </div>
                        <div class="col col-md-3">
                            <div class="mb-3">
                                <label for="inputPassword5" class="form-label">TLP Join Us</label>
                                <input type="text" required  id="tlpju" class="form-control" value= "" >
                            </div>
                        </div>
                        <div class="col col-md-3">
                            <div class="mb-3">
                                <label for="inputPassword5" class="form-label">Tlp Prog Tab Nikah</label>
                                <input type="text" required  id="tlpkemitraan" class="form-control" value= "" >
                            </div>
                        </div>
                        <div class="col col-md-3">
                            <div class="mb-3">
                                <label for="inputPassword5" class="form-label">Tlp Admin Terpadu</label>
                                <input type="text" required  id="admin_terpadu" class="form-control" value= "" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col col-md-3">
                            <div class="mb-3">
                                <label for="inputPassword5" class="form-label">Tlp Cancel Tabungan</label>
                                <input type="text" required  id="cancel_tabungan" class="form-control" value= "" >
                            </div>
                        </div>
                        <div class="col col-md-3">
                            <div class="mb-3">
                                <label for="inputPassword5" class="form-label">TLP Manasik Terpadu</label>
                                <input type="text" required  id="manasik_terpadu" class="form-control" value= "" >
                            </div>
                        </div>
                        <div class="col col-md-3">
                            <div class="mb-3">
                                <label for="inputPassword5" class="form-label">Tlp Perlengkapan</label>
                                <input type="text" required  id="perlengkapan" class="form-control" value= "" >
                            </div>
                        </div>
                        <div class="col col-md-3">
                            <div class="mb-3">
                                <label for="inputPassword5" class="form-label">Tlp Purna Kepulangan</label>
                                <input type="text" required  id="purna_kepulangan" class="form-control" value= "" >
                            </div>
                        </div>
                        <div class="col col-md-3">
                            <div class="mb-3">
                                <label for="inputPassword5" class="form-label">Tlp Admin Pasport</label>
                                <input type="text" required  id="pasport" class="form-control" value= "" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button class="btn btn-danger" onclick="simpan()">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top:25px">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Informasi Berjalan
                    <div style="float:right">
                    </div>
                    <hr>
                <div class="mb-3">
                <Row>
                    <col>
                    <Form method="POST" action="./pages/info/action_info2.php">
                        <input type="hidden"  name="txt" class="form-control" value= "<?php  echo $txt; ?>">
                        <input type="text" required  name="infojalan" class="form-control" value= "<?php  echo $txt; ?>">
                        <Button class="btn btn-warning" style="margin-top:10px">
                            Tetapkan
                        </Button>
                    </Form>
                    </col>
                </Row>
                </div>
            </div>
        </div>
    </div>
    </div>



    <div class="row" style="margin-top:25px">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Form Update Informasi
                    <div style="float:right">
                     <b><a href="./?go=listinfo">List Informasi</a></b>
                    </div>
                    <hr>
               
            </div>

            <div class="card-body">
            <Form method="POST" action="pages/info/action_info.php" enctype="multipart/form-data">
                <input type="hidden"  name="tp" value="<?php echo $tipe?>" class="form-control">
                <input type="hidden"  name="key" value="<?php echo $key?>" class="form-control">
                <div class="overflow-auto" style="height:700px">
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Judul Informasi</label>
                    <input type="text" required  name="judul" class="form-control" value= "" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tujuan Informasi</label>
                    <select name="tujuan" id="" class="form-control">
                        <option value="Agent">Agent</option>
                        <option value="Wisatawan">Wisatawan</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Lampiran</label>
                    <input type="file"  name="informasi" class="form-control" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Informasi</label>
                    <textarea required class="form-control" id="exampleFormControlTextarea1" rows="5" name = "info"></textarea>
                </div>
               
                <Button class="btn btn-success">
                    Tambahkan
                </Button>
              
            </form>
            </div>
            </div>
        </div>
    </div>
    </div>
</div>
<script>
    window.addEventListener('load', loads, false);

    function loads(){
        $.ajax({
            type:'GET',
            url:'pages/info/action_info.php',
            data:{
                tlp:true
            },
            success: function(data){
                let obj = JSON.parse(data)
                obj = obj.data[0]
                $(`#tlpmbt`).val(obj.tlp_mbt)
                $(`#tlpju`).val(obj.tlp_ju)
                $(`#tlpkemitraan`).val(obj.tlp_kemitraan)

                $(`#admin_terpadu`).val(obj.admin_terpadu)
                $(`#cancel_tabungan`).val(obj.cancel_tabungan)
                $(`#manasik_terpadu`).val(obj.manasik_terpadu)
                $(`#perlengkapan`).val(obj.perlengkapan)
                $(`#purna_kepulangan`).val(obj.purna_kepulangan)
                $(`#pasport`).val(obj.pasport)

            }
        })
    }
    function simpan(){
        let tlpmbt = $(`#tlpmbt`).val()
        let tlpju = $(`#tlpju`).val()
        let tlpkemitraan = $(`#tlpkemitraan`).val()

        let admin_terpadu = $(`#admin_terpadu`).val()
        let cancel_tabungan = $(`#cancel_tabungan`).val()
        let manasik_terpadu = $(`#manasik_terpadu`).val()
        let perlengkapan = $(`#perlengkapan`).val()
        let purna_kepulangan = $(`#purna_kepulangan`).val()
        let pasport = $(`#pasport`).val()

        $.ajax({
            type:'POST',
            url:'pages/info/action_info.php',
            data:{
                set:true,
                tlpmbt : tlpmbt,
                tlpju : tlpju,
                tlpkemitraan : tlpkemitraan,
                admin_terpadu: admin_terpadu,
                cancel_tabungan : cancel_tabungan,
                manasik_terpadu : manasik_terpadu,
                perlengkapan : perlengkapan,
                purna_kepulangan : purna_kepulangan,
                pasport : pasport
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    alert('Berhasil')
                }
            }
        })
    }
</script>