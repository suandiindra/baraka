<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Detail Rincian Point</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detail Rincian Point</li>
        </ol>
    </div>

<?php
    $filterTabahan = "";
    $filter_id = "";
    if($_SESSION['role'] == "Agent"){
        $id = $_SESSION['m_agent_id'];
        $filterTabahan = $filterTabahan." and a.m_agent_id = '$id' "; 
        $filter_id = " and a.m_agent_id = '$id' "; 
    }
    
    if(isset($_POST['lihat'])){
        if($_POST['randcheck']==$_SESSION['rand']){
            $date1         = $_POST['date1'];
            $status        = $_POST['status'];
        //    echo "dddd";
            if(strlen($date1) > 2){
                $filterTabahan = $filterTabahan." and DATE_FORMAT(cut_off, '%Y-%m-%d') = '$date1' ";
            }

            if(strlen($status) > 2){
                $filterTabahan = $filterTabahan." and kode_status = '$status' ";
            }
        }
    }else{
        // echo "adsada";
    }
?>
<div class="card" style="margin-bottom:20px;margin-top:-25px">
    <div class="card-body">
        
        <div class="table-responsive p-3">
            <table class="table align-items-center table-flush table-hover" id="dataTableHover">
            <thead class="thead-light">
                <tr>
                <th>No.</th>
                <th>ID Jamaah</th>
                <th>Nomor Resi</th>
                <th>Unik Key</th>
                <th>Nama Jamaah</th>
                <th>ID Agent</th>
                <th>Nama Agent</th>
                <th>Nomor HP</th>
                <th>Tgl Tiba</th>
                </tr>
            </thead>
            <?php
                $cutoff = $_GET['cut_off'];
                $id = $_GET['id'];
                $kode = $_GET['kode'];
                $sel = "select b.nomor_jemaah,b.nomor_resi,no_va,uniq_key,nama_jemaah,c.nomor_agent,nama as nama_agent 
                ,nomor_hp_jemaah,tgl_tiba
                from transaksi_point a
                inner join jemaah b on a.jamaah_id = b.jemaah_id
                inner join m_agent c on c.m_agent_id = a.m_agent_id
                where a.m_agent_id = '$id'
                and DATE_FORMAT(cut_off,'%d-%M-%Y') = '$cutoff'
                and a.kode_status = '$kode'
                and b.isactive is null and is_claim = 'Y'";

                // echo $sel;
                $result = mysqli_query($con,$sel);
                $i = 1;
                while($res = mysqli_fetch_array($result)){
                $style = "";
            ?>
                <tr <?php echo $style; ?>>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $res['nomor_jemaah']; ?></td>
                    <td><?php echo $res['nomor_resi']; ?></td>
                    <td><?php echo $res['uniq_key']; ?></td>
                    <td><?php echo $res['nama_jemaah']; ?></td>  
                    <td><?php echo $res['nomor_agent']; ?></td>
                    <td><?php echo $res['nama_agent']; ?></td>
                    <td><?php echo $res['nomor_hp_jemaah']; ?></td>
                    <td><?php echo $res['tgl_tiba']; ?></td>
                </tr>
            <?php
                $i += 1;
                }
            ?>
            </table>
        </div>
    </div>
</div>
</div>
<?php
        if(isset($_POST['export'])){
            // if($_POST['randcheck']==$_SESSION['rand']){
                $date1         = $_POST['date1'];
                $status        = $_POST['status'];

                if($_SESSION['role'] == "Agent"){
                    $id = $_SESSION['m_agent_id'];
                }

                if(strlen($date1) > 2){
                    $filterTabahan = $filterTabahan." and DATE_FORMAT(cut_off, '%Y-%m-%d') = '$date1' ";
                }
    
                if(strlen($status) > 2){
                    $filterTabahan = $filterTabahan." and kode_status = '$status' ";
                }

                $id = str_replace("'","@",$filterTabahan);
                echo "<script>window.location = './pages/point/export_point.php?q=$id'</script>";
            // }
        }
    ?>