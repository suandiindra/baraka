<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Pencairan</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Point</li>
        </ol>
    </div>
    <?php
        $id_agent = "";
        $nama_agent = "";
        $bank = "";
        $rekening = "";
        $pencairan = "";
        $tgl_pencairan = "";
        $tgl_cut_off = "";
        $transaksi_id = $_GET['id'];

        $sel = "select a.petugas,a.m_agent_id,b.nomor_agent,b.nama,bank,nomor_rekening,sum(point_count) as point,cut_off, sum(point_amount) as amount,sum(point_pajak) as ppn
        ,'' as cutoff,'',case when a.status = 'Point' then 'On Going' else status end as stat,tgl_pencairan,kode_status from transaksi_point a
        inner join m_agent b on a.m_agent_id = b.m_agent_id where a.m_agent_id = '$transaksi_id'
        group by a.m_agent_id,b.nomor_agent,b.nama,bank,nomor_rekening,cut_off,tgl_pencairan,kode_status,a.petugas";

        // echo $sel;
        $result = mysqli_query($con,$sel);
        $res = mysqli_fetch_array($result);
        if($res){
            $id_agent   = $res['nomor_agent'];
            $nama_agent = $res['nama'];
            $bank       = $res['bank'];
            $rekening   = $res['nomor_rekening'];
            $pencairan  = number_format($res['amount']);
            $tgl_pencairan = $res['tgl_pencairan'];
            $tgl_cut_off = $res['cut_off'];
            $transaksi_id = $_GET['id'];
            $kode_status = $res['kode_status'];
            $petugas = $res['petugas'];
        }
    ?>
    <div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Data Pencairan
                    <div style="float:right">
                       <a href="./?go=listwisatawan">List Wisatawan</a>
                    </div>
            <hr>
            </div>
            <div class="card-body" id="formInput" style = "display:block">
            <Form method="POST" action="pages/point/action_point.php" enctype="multipart/form-data">
                    <input type="hidden"  name="edt" value="edit" class="form-control">
                    <input type="hidden" id="agent_id"  name="agent_id" value = "<?php echo $transaksi_id; ?>" class="form-control">
                    <div class="overflow-auto" style="height:700px">
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nomor agent</label>
                        <input type="text" readonly  name="id_agent" class="form-control" value= "<?php echo $id_agent ?>" >
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nama Agent</label>
                        <input type="text" readonly  name="nama_agent" class="form-control" value= "<?php echo $nama_agent ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Bank</label>
                        <input type="text" readonly  name="hp_wisatawan" class="form-control" value= "<?php echo $bank ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Rekening</label>
                        <input type="text" readonly  name="tempat_lahir" class="form-control" value= "<?php echo $rekening ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Tgl CutOff</label>
                        <input type="date" readonly  name="tgl_cut_off" class="form-control" value= "<?php echo $tgl_cut_off ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Tgl Pencairan</label>
                        <input type="date" required  name="tgl_pencairan" class="form-control" value= "<?php echo $tgl_pencairan ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Petugas</label>
                        <input type="text" readonly  name="petugas" class="form-control" value= "<?php echo $petugas ?>">
                    </div>
                <?php
                    if($_SESSION['role'] == "Agent"){

                    }else{
                        if($kode_status != "WT3"){
                ?>
                        <Button class="btn btn-warning">
                            Set Pencairan
                        </Button>
                <?php
                        }
                    }
                ?>
                <!-- <Button class="btn btn-warning">
                            Set Pencairan
                        </Button> -->
            </form>   
            </div>
            </div>
            </div>
        </div>
    </div>
</div>