<script type="text/javascript">
    window.onload = function () {
        $.ajax({
                url: './pages/stok_pin/action_pin.php',
                type: 'POST',
                data: {option: 'all'},
                success: function(e) {
                    console.log(e);
                    $('#partial').html(e);
                }
            });
    };
</script>
<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Stock PIN</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Stock PIN</li>
    </ol>
    </div>
</div>
<?php
    
    if($_SESSION['role'] == "Agent"){
        $cek = "select sum(case when status_jemaah = 'Draft' and isactive is null then 1 else 0 end) as stok,
        sum(case when status_jemaah <> 'Draft' and isactive is null then 1 else 0 end) as terpakai,
        sum(case when isactive = 0 then 1 else 0 end) as gagal,
        sum(case when jemaah_id is not null then 1 else 0 end) as semua
        from jemaah where m_agent_id = '$_user'";
    }else{
        $cek = "select sum(case when status_jemaah = 'Draft' and isactive is null then 1 else 0 end) as stok,
        sum(case when status_jemaah <> 'Draft' and isactive is null then 1 else 0 end) as terpakai,
        sum(case when isactive = 0 then 1 else 0 end) as gagal,
        sum(case when jemaah_id is not null then 1 else 0 end) as semua
        from jemaah ";
    }
        
    $result = mysqli_query($con,$cek);
    $res1 = mysqli_fetch_array($result)
?>
<div class="row">
        <div class="col-xl-2 col-md-4 mb-4" style="margin-left:10px; margin-top:-25px">
            <div class="card h-100" style="background-color:#FFD000">
                <div class="card-body">
                    <div class="row align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:black">Stock PIN ( Belum terpakai )</div>
                        <div class="h5 mb-0 font-weight-bold" style="color:black"><?php echo $res1['stok'] ?></div>
                        <div class="mt-2 mb-0 text-muted text-xs">
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-bookmark fa-2x text-primary"></i>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-2 col-md-4 mb-2" style="margin-left:0px; margin-top:-25px">
            <div class="card h-110" style="background-color:#32C28A">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:white">PIN Terpakai</div>
                    <div class="h5 mb-0 font-weight-bold" style="color:white"><?php echo $res1['terpakai'] ?></div>
                    <div class="mt-2 mb-0 text-muted text-xs">
                    </div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-suitcase fa-2x text-warning"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-xl-2 col-md-4 mb-2" style="margin-left:0px; margin-top:-25px">
            <div class="card h-110" style="background-color:#D8684E">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:white">Gagal Terjual</div>
                    <div class="h5 mb-0 font-weight-bold" style="color:white"><?php echo $res1['gagal'] ?></div>
                    <div class="mt-2 mb-0 text-muted text-xs">
                    </div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-window-close fa-2x" style="background-color:red"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-4 mb-2" style="margin-left:0px; margin-top:-25px">
            <div class="card h-110" style="background-color:#009EDD">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:white">Keseluruhan PIN</div>
                    <div class="h5 mb-0 font-weight-bold" style="color:white"><?php echo $res1['semua'] ?></div>
                    <div class="mt-2 mb-0 text-muted text-xs">
                    </div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-flag fa-2x text-danger"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
        

    

    <?php
    if($_SESSION['role'] == "Agent"){
        $cek = "select *,b.nomor_agent as nom,b.nama as nm,nomor_transaksi,a.nomor_resi as rs from jemaah a
        join m_agent b on a.m_agent_id = b.m_agent_id  
        join transaksi_agent c on c.transaksi_agent_id = a.transaksi_agent_id
        where a.m_agent_id = '$_user' and status_jemaah = 'Draft' 
        order by cast(substring(a.nomor_resi,3,10) as int) asc";
    }else{
        $cek = "select *,b.nomor_agent as nom,b.nama as nm,nomor_transaksi,a.nomor_resi as rs from jemaah a
        join m_agent b on a.m_agent_id = b.m_agent_id  
        join transaksi_agent c on c.transaksi_agent_id = a.transaksi_agent_id
        order by cast(substring(a.nomor_resi,3,10) as int) asc";
    }
    ?>
            <!-- darisini -->
        <div class="col-lg-12">
            <div class="row">
                <div class="col-xl-12 col-md-4 mb-2">
                <div class="card h-100" style="background-color:#ffff">
                    <div class="card-body">
                    <table style="background-color:white; width:100%">
                        <tr>
                            <td style="background-color:white; width:20%">
                            <select class="form-control" id="tipe" onchange="cekLagi()">
                                <option value="">Pilih Status PIN</option>
                                <option value="all">Semua</option>
                                <option value="Terpakai">Sudah Terpakai</option>
                                <option value="Belum terpakai">Belum Terpakai</option>
                                <option value="Gagal">Gagal Terpakai</option>
                            </select>
                            </td>
                            <td style="padding-left:20px">
                                <!-- <input type="text" onChange="cekLagi()" placeholder="Pencarian disini" 
                                class="form-control col-md-4" id="src" name="src"/> -->
                            </td>
                        </tr>
                    </table>
                       
                    </div>
                </div>
            </div>
        </div>

        <div class="card mb-4">
            <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="tblpin">
                    <thead class="thead-light">
                      <tr>
                        <th>No</th>
                        <th>ID Agent</th>
                        <th>Nama Agent</th>
                        <th>ID Transaksi</th>
                        <th>Nomor Resi</th>
                        <th>Nomor Jamaah</th>
                        <th>Uniq Key</th>
                        <th>Ket</th>
                        <th>Status</th>
                    <?php
                        if($_SESSION['role'] == "Agent"){

                        }else{
                    ?>
                        <th style="width:200px">Action</th>
                    <?php
                        }
                    ?>
                      </tr>
                    </thead>
                    <tbody id="dtlpin">
                    <tbody>
                  </table>
                </div>
                </div>
                </div>
            <!-- sampai sini -->
</div>

<script>
        function confirmationDelete(anchor){
            var conf = confirm('Apakah yakin melakukan proses ini ???');
            if(conf)
                window.location=anchor.attr("href");
        }

        window.addEventListener('load', show, false);


        function show(){
            var x = document.getElementById("tipe").value;
            var table = $('#tblpin').DataTable();
            table.clear().destroy(); 
            $.ajax({
                url: './pages/stok_pin/action_pin.php',
                type: 'POST',
                data: {option: x},
                success: function(e) {
                    // console.log(e);
                    $('#dtlpin').html(e)
                    $('#tblpin').DataTable(({ 
                        "destroy": true, //use for reinitialize datatable
                        // order: [[5, 'asc']],
                    }));
                }
            });
        }


        function cekLagi(){
            // var x = document.getElementById("src").value;
            // console.log(x)
            var x = document.getElementById("tipe").value;
            var table = $('#tblpin').DataTable();
            table.clear().destroy(); 
            $.ajax({
                url: './pages/stok_pin/action_pin.php',
                type: 'POST',
                data: {search: x},
                success: function(e) {
                    console.log(e);
                    $('#dtlpin').html(e)
                    $('#tblpin').DataTable(({ 
                        "destroy": true, //use for reinitialize datatable
                        // order: [[5, 'asc']],
                    }));
                }
            });
        }
        
</script>