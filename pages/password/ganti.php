<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">History Tabungan Calon Wisatawan</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ganti Password</li>
        </ol>
    </div>
</div>
<div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-body">
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Masukan Password</label>
                    <input type="text" maxlength="36" placeholder = "masukan password"  id="pwd1" class="form-control" value= "">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Konfirmasi Password</label>
                    <input type="text" maxlength="36" placeholder = "ulangi password" id="pwd2" class="form-control" value= "">
                </div>
                <div class="mb-3">
                    <button class="btn btn-danger" id="btnganti" onclick="gantiPwd(this)">Ganti Password</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    function gantiPwd(v){
        if($(`#pwd1`).val() !== $(`#pwd2`).val()){
            alert(`Password Belum sama`)
            return
        }

       
        $.ajax({
            type:'POST',
            url:'pages/password/ganti_be.php',
            data:{
                pwd:$(`#pwd2`).val()
            },
            success: function(data){
                console.log(data);
                // if(data == "200"){
                    alert(`Password Berhasil di ubah`)
                    $(`#pwd1`).val("") 
                    $(`#pwd2`).val("") 
                    window.location = "./"
                // }else{
                //     console.log(data);
                // }
            }
        })
    }
</script>