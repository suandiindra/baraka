<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Agent</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="./">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Agent Profile</li>
    </ol>
    </div>

    <div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-body">
            <div class="overflow-auto" style="height:700px">
            <div class="mb-3">
                <label for="inputPassword5" class="form-label">ID Agent</label>
                <input type="text" disabled id="inputPassword5" class="form-control" value="Indra Suandi">
            </div>
            <div class="mb-3">
                <label for="inputPassword5" class="form-label">Nama Lengkap</label>
                <input type="text" disabled id="inputPassword5" class="form-control" value="Indra Suandi">
            </div>
            <div class="mb-3">
                <label for="inputPassword5" class="form-label">NIK KTP</label>
                <input type="text" disabled id="inputPassword5" class="form-control" value="1234566">
            </div>
            <div class="mb-3">
                <label for="inputPassword5" class="form-label">Alamat</label>
                <input type="text" disabled id="inputPassword5" class="form-control" value="BEKASI">
            </div>
            <div class="mb-3">
                <label for="inputPassword5" class="form-label">No Handphone</label>
                <input type="text" disabled id="inputPassword5" class="form-control" value="089998856597">
            </div>
            <div class="mb-3">
                <label for="inputPassword5" class="form-label">Bank</label>
                <input type="text" disabled id="inputPassword5" class="form-control" value="">
            </div>
            <div class="mb-3">
                <label for="inputPassword5" class="form-label">No Rekening</label>
                <input type="text" disabled id="inputPassword5" class="form-control" value="">
            </div>
            <hr>
            <div class="mb-3">
                <label for="inputPassword5" class="form-label">Nama Ahli Waris</label>
                <input type="text" disabled id="inputPassword5" class="form-control" value="Hasti Ningrum">
            </div>
            <div class="mb-3">
                <label for="inputPassword5" class="form-label">No. Handphone</label>
                <input type="text" disabled id="inputPassword5" class="form-control" value="xxxxxxxxxxxxxx">
            </div>
            <div class="mb-3">
                <label for="inputPassword5" class="form-label">Hubungan</label>
                <input type="text" disabled id="inputPassword5" class="form-control" value="Istri">
            </div>
            <!-- <div class="table-responsive p-3">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>No.</th>
                        <th>No. DO</th>
                        <th>Tgl DO</th>
                        <th>Tipe DO</th>
                        <th>Customer</th>
                        <th>QTY</th>
                        <th>Dibuat Oleh</th>
                        <th>Catatan</th>
                        <th style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    
                  </table>
            </div> -->
            </div>
            </div>
        </div>
    </div>
    </div>
</div>