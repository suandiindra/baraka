<div class="card mb-4">
    <div class="table-responsive p-3">
        <table class="table align-items-center table-flush table-hover" id="tblUpgrade">
            <thead class="thead-light">
                <!-- <th>No</th> -->
                <th>Nama</th>
                <th>ID Agent</th>
                <th>Tipe Upgrade</th>
                <th>Sebelumnya</th>
                <th style="display:none">Periode</th>
                <th>Periode</th>
                <th>Pembayaran</th>
                <th>Dok Transfer</th>
                <th>Dok Formulir</th>
                <th>Status</th>
                <th>Action Date</th>
                <th>Petugas</th>
                <th>Action</th>
            </thead>
            <tbody id="dtlUpgrade">
            <tbody>
        </table>
    </div>
</div>
<script>
    window.addEventListener('load', loads, false);
    function loads(){
        var table = $('#tblUpgrade').DataTable();
        table.clear().destroy();
        $.ajax({
            type:'GET',
            url:'pages/agent/upgrade_be.php',
            data:{
                findOne:true
            },
            success: function(data){
                // console.log(data);
                let obj = JSON.parse(data)
                obj = obj.data
                let role = "<?php echo $_SESSION['role'] ?>";
                let dom = ``
                let urutan = 0
                for(let i = 0; i < obj.length;i++){
                    urutan = obj.length - i;
                    dom = dom + `<tr>
                        <td>${obj[i].nama_agent}</td>
                        <td>${obj[i].nomor_agent}</td>
                        <td>${obj[i].tipe_agent_after}</td>
                        <td>${obj[i].tipe_agent_before}</td>
                        <td style="display:none">${obj[i].createdate}</td>
                        <td>${obj[i].tgl}</td>
                        <td>${obj[i].tipe_bayar}</td>
                        <td><a href='${obj[i].bukti_transaksi}'>Bukti</a></td>
                        <td><a href='${obj[i].bukti_form}'>Bukti</a></td>
                        <td><div class='badge bg-${obj[i].kode_status == "WT0" ? "danger" : "success"} text-white'>${obj[i].status_desc}</div></td>
                        <td>${obj[i].tglCOnfirm ? obj[i].tglCOnfirm : ""}</td>
                        <td>${obj[i].action_user ? obj[i].action_user : ""}</td>
                        ${role !== "1" ? `<td>
                            ${obj[i].kode_status == "WT0" || obj[i].kode_status == "WT2"? "" :
                            `<button class='btn-success' onclick="approve('${obj[i].transaksi_upgrade_id}')">Approve</button>
                            <button class='btn-danger' onclick="reject('${obj[i].transaksi_upgrade_id}')">Reject</button>`
                            }
                        </td>` : `<td>${obj[i].kode_status == "WT2" && !obj[i].date_know? `<button class='btn btn-warning' onclick="conf('${obj[i].transaksi_upgrade_id}')">Confirm</button>`:`<div class='badge bg-warning text-white'>${obj[i].date_know ? `Confirm` : ``}</div>`}</td>`}
                        
                    </tr>`
                }
                $('#dtlUpgrade').html(dom)
                $('#tblUpgrade').DataTable(({ 
                    "destroy": true, //use for reinitialize datatable
                    order: [[4, 'desc']],
                }));
            }
        })
    }
    function approve(v){
        if(!confirm(`Yakin melakukan transaksi...??`)){
            return
        }
        $.ajax({
            type:'POST',
            url:'pages/agent/upgrade_be.php',
            data:{
                approve : true,
                id : v
            },
            success: function(data){
                // console.log(data);
                if(data == "200"){
                    alert(`Berhasil`)
                    loads()
                }else if(data == "500"){
                    alert(`Gagal`)
                }else if(data == "Pembelian PIN"){
                    console.log('PIN');
                    $.ajax({
                        type:'GET',
                        url:'pages/pembelian_pin/action_pembelian.php',
                        data:{
                            act: "1",
                            id : v,
                            rjm : "Y",
                            go: true
                        },
                        success: function(data){
                            // console.log(data);
                            alert('Berhasil')
                            window.location = "./?go=listupgrade"
                        }
                    })
                }
            }
        })
    }
    function reject(v){
        if(!confirm(`Yakin melakukan transaksi...??`)){
            return
        }
        $.ajax({
            type:'POST',
            url:'pages/agent/upgrade_be.php',
            data:{
                reject : true,
                id : v
            },
            success: function(data){
                console.log(data,' -po');
                if(data == "200"){
                    alert(`Berhasil`)
                    loads()
                }else if(data == "500"){
                    alert(`Gagal`)
                }else if(data == "Pembelian PIN"){
                    console.log('PIN');
                    $.ajax({
                        type:'GET',
                        url:'pages/pembelian_pin/action_pembelian.php',
                        data:{
                            act: "1",
                            id : v,
                            rjm : "0",
                            go: true
                        },
                        success: function(data){
                            console.log(data);
                            // alert('Berhasil')
                            // window.location = "./?go=listnewagent"
                        }
                    })
                }
            }
        })
    }

    function conf(v){
        if(!confirm(`Yakin melakukan transaksi...??`)){
            return
        }
        $.ajax({
            type:'POST',
            url:'pages/agent/upgrade_be.php',
            data:{
                conf : true,
                id : v
            },
            success: function(data){
                // console.log(data);
                if(data == "200"){
                    alert(`Berhasil`)
                    loads()
                }else if(data == "500"){
                    alert(`Gagal`)
                }
            }
        })
    }
</script>