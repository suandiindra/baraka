<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Agent</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Agent Profile</li>
        </ol>
    </div>
    <?php
        $nama_lengkap = "";
        $ktp = "";
        $alamat = "";
        $email = "";
        $hp = "";
        $bank = "";
        $rekening = "";
        $ahliwaris = "";
        $hp_ahliwaris = "";
        $hubungan = "";   
        $tipe = "add";
        $label = "Tambahkan";
        $key = "";
        $password = "";
        $id_agent = "";
        $ket = "";
        $npwp = "";
        $Identitas = "";
        $nama_alias = "";
        $tipe_agent = "";

        $read = "Readonly";
        if(isset($_GET["id"])){
            $id = $_GET["id"];
            $key = $id;
            $sel = "select * from m_agent where m_agent_id = '$id'";

            $result = mysqli_query($con,$sel);
            if($result){
                $tipe = "edit";
                $label = "Edit";
                $data = mysqli_fetch_array($result);
                $id_agent = $data['nomor_agent'];
                $nama_lengkap = $data['nama'];
                $ktp = $data['ktp'];
                $alamat = $data['alamat'];
                $alamat_ktp = $data['alamat_ktp'];
                $email = $data['email'];
                $tipe_agent = $data['tipe_agent'];
                $nama_alias = $data['nama_alias'];
                $hp = $data['hp'];
                $bank = $data['bank'];
                $rekening = $data['nomor_rekening'];    
                $ahliwaris = $data['nama_ahli_waris'];
                $hp_ahliwaris = $data['nomor_hp'];;
                $hubungan = $data['hubungan'];  
                $ket = $data['keterangan'];  
                $npwp = $data['npwp'];  
                $Identitas = $data['identitas'];  

            }
        }
        // echo $_SESSION['role'];
        if($_SESSION['role'] == "1" || $_SESSION['role'] == "administrator"){
            $read = "";
        }
    ?>
    <div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                <div class="float-right">
                    <?php
                        if($tipe_agent == "MGB 1" || $tipe_agent == "MNN"){
                    ?>
                        <button id="btnUpgradeForm" class="btn btn-success" onclick="upgrade('<?php echo $tipe_agent ?>')"><i class="fa fa-arrow-up text-white"></i> Upgrade</button>
                    <?php
                        }
                    ?>
                    <button id="btnhistory" class="btn btn-warning" onclick="cekHistory()"><i class="fa fa-clock text-white"></i> History Upgrade</button>
                </div>
            </div>

            <div class="card-body">
            <?php
                if($_SESSION['role'] == "Agent"){
            ?>
                    <Form method="POST" action="pages/agent/action_agent.php" style="margin-bottom:30px;">
                        <input type="hidden"  name="tp" value="ganti-pwd" class="form-control">
                        <div class="mb-3">
                        <label for="inputPassword5" class="form-label badge-danger">&nbsp&nbsp Password Lama &nbsp&nbsp </label>
                            <input type="text" required  name="old_pwd" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword5" class="form-label badge-danger">&nbsp&nbsp Password Baru &nbsp&nbsp </label>
                            <input type="text" required  name="new_pwd" class="form-control" >
                        </div>

                        <button class="btn btn-success">Konfirmasi Password</button>
                    </form>
            <?php
                }
            ?>
            <Form method="POST" action="pages/agent/action_agent.php">
                <input type="hidden"  name="tp" value="<?php echo $tipe?>" class="form-control">
                <input type="hidden"  name="key" value="<?php echo $key?>" class="form-control">
                <div class="overflow-auto" style="height:700px">
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">ID Agent</label>
                    <input type="text" maxlength="6" required  name="nomor_agent" class="form-control" value= "<?php echo $id_agent ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Tipe Agent</label>
                    <input type="text" maxlength="6" readonly  name="tipe_agent" class="form-control" value= "<?php echo $tipe_agent ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama Login</label>
                    <input type="text" required  name="nama" class="form-control" value= "<?php echo $nama_lengkap ?>" >
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama KTP</label>
                    <input type="text" required  name="nama_alias" class="form-control" value= "<?php echo $nama_alias ?>" >
                </div>
                <?php
                    if($_SESSION['role'] == "Agent"){
                        
                    }else{
                ?>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Password</label>
                    <input type="text"  name="password" class="form-control" value= "<?php echo $password ?>" >
                </div>
                <?php
                    }
                ?>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">NIK KTP</label>
                    <input type="number" required  name="ktp" class="form-control" value= "<?php echo $ktp ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Alamat KTP</label>
                    <input type="text" required  name="alamat" class="form-control" value= "<?php echo $alamat_ktp ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Alamat Domisili</label>
                    <input type="text" required  name="alamat" class="form-control" value= "<?php echo $alamat ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">No Handphone</label >
                    <input type="text" required  name="hp" class="form-control" value= "<?php echo $hp ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Email</label>
                    <input type="text"  name="email" class="form-control" value= "<?php echo $email ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">NPWP</label>
                    <input type="number"  name="npwp" class="form-control" value= "<?php echo $npwp ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Bank</label>
                    <input type="text" required  name="bank" class="form-control" value= "<?php echo $bank ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">No Rekening</label>
                    <input type="text" required  name="rekening" class="form-control" value= "<?php echo $rekening ?>">
                </div>
                <hr>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nama Ahli Waris</label>
                    <input type="text" required  name="ahliwaris" class="form-control" value= "<?php echo $ahliwaris ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">No. Handphone</label>
                    <input type="text" required  name="hp_ahliwaris" class="form-control" value= "<?php echo $hp_ahliwaris ?>">
                </div>
                <!-- <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Hubungan</label>
                    <input type="text"  name="hub" class="form-control">
                </div> -->
                <div class="form-group">
                    <label>Hubungan</label>
                   <select name="hub" required id="m_merk_id" class="form-control" >
                        <option value="<?php echo $hubungan ?>"><?php echo $hubungan ?></option>
                        <option value="Suami">Suami</option>
                        <option value="Istri">Istri</option>
                        <option value="Anak">Anak</option>
                        <option value="Saudara Kandung">Saudara Kandung</option>
                        <option value="Orang Tua">Orang Tua</option>
                        <option value="Sepupu">Sepupu</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Identitas</label>
                    <input type="text" required  name="identitas" class="form-control" value= "<?php echo $Identitas ?>">
                </div>
                <?php
                    if($_SESSION['role'] == "Agent"){
                        
                    }else{
                ?>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Keterangan Agent</label>
                    <textarea name="ket" <?php echo $read; ?> class="form-control" style="height: 350px; padding-left:0px"><?php echo $ket ?></textarea>
                </div>
                <?php
                    }


                    if($_SESSION['role'] == "Agent"){
                        
                    }else{
                ?>
                    <Button class="btn btn-success">
                            <?php echo $label; ?>
                    </Button>
                <?php
                    }
                ?>
            </form>
            </div>
            </div>
        </div>
    </div>
    </div>
    <!-- modal history -->
    <div class="modal fade bd-example-modal-lg" id="modalHistory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">History Upgrade</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <Table class="table table-striped">
                    <thead>
                        <th>Tipe Upgrade</th>
                        <th>Sebelumnya</th>
                        <th>Periode</th>
                        <th>Pembayaran</th>
                        <th>Dok Transfer</th>
                        <th>Dok Formulir</th>
                        <th>Status</th>
                    </thead>
                    <tbody id="isiModal">

                    </tbody>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
    </div>
    <!-- modal disini -->
    <div class="modal fade" id="modalUpgrade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Upgrade Tipe Agent</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="inputPassword5" class="form-label">Pilihan Tipe Agent</label>
            <select name="tipe" id="tipe" class="form-control">
            </select>
            <div class="mb-3" id="groupbayar">
                <label for="inputPassword5" class="form-label">Pilihan Lisensi Agent</label>
                <select name="" id="lisensi" class="form-control">
                    <option value=""></option>
                    <option value="Lisensi Upgrade">Lisensi Upgrade</option>
                    <option value="Pembelian PIN">Pembelian PIN</option>
                </select>
            </div>
            <div class="mb-3">
                <label for="inputPassword5" class="form-label" id="labelfree">Input Bukti Transfer</label>
                <input type="file" required  id="buktitransfer" class="form-control" value= "" >
            </div>
            <div class="mb-3">
                <label for="inputPassword5" class="form-label">Formulir Upgrade</label>
                <input type="file" required  id="buktiformulir" class="form-control" value= "" >
            </div>
            <div>
                <p class="text-primary">Kepala posko agent MGB Baraka, wajib mengajukan permohonan menjadi kepala posko Agent MGB Baraka (Permohonan menjadi kepala Posko MGB Baraka)</p>
                <p class="text-success">Konfirmasi pengiriman file via email atau ke nomor 
                     WA <b>0813-1913-8151</b></p>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" onclick="konfirmUpgrade(this)" id="btnUpgrade" class="btn btn-primary">Konfirmasi</button>
        </div>
        </div>
    </div>
    </div>
</div>

<script>
    window.addEventListener('load', loads, false);
    var dataHistory = [];
    function loads(){
        var url_string = window.location; 
        var url = new URL(url_string);
        var c = url.searchParams.get("id");
        $.ajax({
            type:'GET',
            url:'pages/agent/upgrade_be.php',
            data:{
                findOne:true,
                m_agent_id : c
            },
            success: function(data){
                // console.log(data);
                // return
                let obj = JSON.parse(data)
                obj = obj.data
                if(obj.length > 0){
                    dataHistory = obj
                }else{
                    $(`#btnhistory`).css('display','none')
                }
                let ro = obj.filter(item => item.kode_status !== "WT0")
                // hanya sekali upgrade saja
                if(ro.length > 0){
                    $(`#btnUpgradeForm`).css('display','none')
                }
            }
        })
    }
    function cekHistory(){
        console.log(dataHistory);
        let obj = dataHistory
        let dom = ``
        for(let i = 0; i<obj.length; i++){
            dom = dom + `<tr>
                <td>${obj[i].tipe_agent_after}</td>
                <td>${obj[i].tipe_agent_before}</td>
                <td>${obj[i].tgl}</td>
                <td>${obj[i].tipe_bayar}</td>
                <td><a href='${obj[i].bukti_transaksi}'>Bukti</a></td>
                <td><a href='${obj[i].bukti_form}'>Bukti</a></td>
                <td><div class='badge bg-${obj[i].kode_status == "WT2" ? "success" : "danger"} text-white'>${obj[i].status_desc}</div></td>
            </tr>`
        }
        $(`#isiModal`).html(dom)
        $(`#modalHistory`).modal('show')
    }
    function konfirmUpgrade(v){
        let isvalid = []
        var lisensi = $(`#lisensi`).val()
        var buktitransfer = $('#buktitransfer').prop('files')[0]; 
        var buktiformulir = $('#buktiformulir').prop('files')[0]; 
        lisensi.length == 0 ? isvalid.push("Pilih Tipe lisensi") : null
        !buktitransfer ? isvalid.push("Upload Bukti Transfer") : null
        !buktiformulir ? isvalid.push("Upload Bukti formulir") : null

        var url_string = window.location; 
        var url = new URL(url_string);
        var c = url.searchParams.get("id");
    

        if(isvalid.length > 0){
            alert(isvalid.toString())
            return
        }
        if(!confirm(`Yakin melanjutkan transaksi...`)){
            return
        }
        var form_data = new FormData(); 
        $(`#${v.id}`).html('Please Wait...')
        form_data.append('upgrade',true);
        form_data.append('tipe', $(`#tipe`).val());
        form_data.append('lisensi', $(`#lisensi`).val());
        form_data.append('m_agent_id',c );
        form_data.append('buktitransfer', buktitransfer);
        form_data.append('buktiformulir', buktiformulir);

        console.log(form_data);                            
        $.ajax({
            url: 'pages/agent/upgrade_be.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(res){
                $(`#${v.id}`).html('Konfirmasi')
                console.log(res);
                if(res == "200"){
                    $(`#modalUpgrade`).modal('toggle')
                    alert("Proses Berhasil..")
                    loads()
                }else if(res == "300"){
                    alert(`Masih ada dokument yang belum selesai..`)
                    $(`#modalUpgrade`).modal('toggle')
                }
            }
        });


    }
    function upgrade(v){
        // console.log(v);
        let options = ``
        if(v == "MGB 1"){
            options = `<option value="MGB 2">Kepala posko Agent MGB (2)</option><option value="MGB 3">Kepala posko Agent MGB (3)</option>`
        }else if(v == "MNN"){
            options = `<option value="MGB 1">Kepala posko Agent MGB (1)</option><option value="MGB 2">Kepala posko Agent MGB (2)</option><option value="MGB 3">Kepala posko Agent MGB (3)</option>`
        }
        $(`#tipe`).html(options)
        $(`#modalUpgrade`).modal('show')

    }
</script>