<?php
session_start();
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    $_nama = $_SESSION['nama'];
    if(isset($_GET['findOne'])){
        $filter = ``;
        if(isset($_GET['m_agent_id'])){
            $m_agent_id = $_GET['m_agent_id'];
            $filter = " and a.m_agent_id = '$m_agent_id'";
        }
        $sel = "select a.*,date_format(createdate ,'%d %M %y') as tgl 
                ,date_format(action_date ,'%d %M %y') as tglCOnfirm, b.nomor_agent 
                from transaksi_upgrade a 
                inner join m_agent b on a.m_agent_id = b.m_agent_id
                where 1=1 $filter order by createdate desc ";
        echo queryJson($con,$sel);
        // echo $sel;
    }
    if(isset($_POST['upgrade'])){
        $m_agent_id = $_POST['m_agent_id'];
        $tipe = $_POST['tipe'];
        $lisensi = $_POST['lisensi'];

        $selCek = "select *,date_format(createdate ,'%d %M %y') as tgl 
        from transaksi_upgrade where m_agent_id = '$m_agent_id' and kode_status = 'WT1'";
        $res = mysqli_query($con,$selCek);
        if(mysqli_num_rows($res) > 0){
            echo "300";
            exit;
        }

        // echo "as";
        // exit;
        $sel = "select *,uuid() as idx from m_agent where m_agent_id = '$m_agent_id' ";
        $res = mysqli_query($con,$sel);
        $do = mysqli_fetch_array($res);
        $unik = $do['idx'];
        $nama_agent = $do['nama'];
        $tipe_agent = $do['tipe_agent'];
        $nomor_agent = $do['nomor_agent'];


        if(isset($_FILES['buktitransfer'])){
            $nama_file1 = $_FILES['buktitransfer']['name'];
            $data_file1 = $_FILES['buktitransfer']['tmp_name'];
            $path1 = "../../asset/bukti_upgrade_transaksi/$unik/".$nama_file1;
            $pathData = "asset/bukti_upgrade_transaksi/$unik/".$nama_file1; 
            if( is_dir($path1) === false ){
                mkdir("../../asset/bukti_upgrade_transaksi/$unik/");
            }
            if(move_uploaded_file($data_file1, $path1)){
                $dokumen1 = $pathData;
            }
        }

        if(isset($_FILES['buktiformulir'])){
            $nama_file1 = $_FILES['buktiformulir']['name'];
            $data_file1 = $_FILES['buktiformulir']['tmp_name'];
            $path1 = "../../asset/bukti_upgrade_formulir/$unik/".$nama_file1;
            $pathData = "asset/bukti_upgrade_formulir/$unik/".$nama_file1; 
            if( is_dir($path1) === false ){
                mkdir("../../asset/bukti_upgrade_formulir/$unik/");
            }
            if(move_uploaded_file($data_file1, $path1)){
                $dokumen2 = $pathData;
            }
        }
        if($lisensi == "Pembelian PIN"){
            // echo "yoi";
            $urutan = transApprove($con);
            $jml =  0; // $_POST['pin'];
            $nominal_topup = 0;
            if($tipe == "MGB 1"){
                $nominal_topup = 4000000;
                $jml =  2;
            }else if($tipe == "MGB 2"){
                $nominal_topup = 8000000;
                $jml =  4;
            }else if($tipe == "MGB 3"){
                $nominal_topup = 16000000;
                $jml =  8;
            }
            $nomor_transaksi = rand(100000,10000);

            $insertTrans = "insert into transaksi_agent (transaksi_agent_id,m_agent_id,nomor_agent,nama_agent,nominal_topup,tgl_topup
            ,status_transaksi,kode_status,nomor_transaksi,jml_pin,urutan,bukti_topup,confirm_date,keterangan,tipe_agent_)
            values ('$unik','$m_agent_id','$nomor_agent','$nama_agent','$nominal_topup',now(),'Menunggu Approval'
            ,'WT1','$nomor_transaksi',$jml,'$urutan','$dokumen1',now(),'Upgrade Agent','$tipe_agent -> $tipe')";

            // echo $insert;
            $res = mysqli_query($con,$insertTrans);
        }

        // exit;
        $insert = "insert into transaksi_upgrade
                (transaksi_upgrade_id,m_agent_id,nama_agent,tipe_agent_before,tipe_agent_after 
                ,tipe_bayar,kode_status ,status_desc ,isactive ,createdate ,bukti_transaksi ,bukti_form)
                values ('$unik','$m_agent_id','$nama_agent','$tipe_agent','$tipe'
                ,'$lisensi','WT1','Menunggu',1,now(),'$dokumen1','$dokumen2')";

        $res = mysqli_query($con,$insert);
        if($res){
            echo "200";
        }else{
            echo $insert;
        }
        
    }
    if(isset($_POST['reject'])){
        $id  = $_POST['id'];

        $sel = "select * from transaksi_upgrade tu where transaksi_upgrade_id = '$id'";
        $res = mysqli_query($con,$sel);

        $do = mysqli_fetch_array($res);
        $tipe_bayar = $do['tipe_bayar'] ;
        if($tipe_bayar == "Lisensi Upgrade"){
            $upd = "update transaksi_upgrade set kode_status = 'WT0', status_desc = 'Reject', action_date = now()
            ,action_user = '$_nama' where transaksi_upgrade_id = '$id' ";

            $res = mysqli_query($con,$upd);
            if($res){
                echo "200";
            }else{
                echo "500";
            }
        }else{
            $upd = "update transaksi_upgrade set kode_status = 'WT0', status_desc = 'Reject', action_date = now()
            ,action_user = '$_nama' where transaksi_upgrade_id = '$id' ";

            $res = mysqli_query($con,$upd);
            if($res){
                $cek2 = "select * from transaksi_agent where transaksi_agent_id = '$id' ";
                $result = mysqli_query($con,$cek2);
                if(mysqli_num_rows($result) > 0 ){
                    $updReset = "update transaksi_agent set is_confirm_topup = 'N' ,kode_status = '0'
                    , status_transaksi = 'Ditolak',admin = '$_nama' 
                    where transaksi_agent_id = '$id'";

                    // echo $updReset;
                    $result = mysqli_query($con,$updReset);
                    if($result){
                        echo "200";
                        exit;
                    }
                }
                echo "200";
            }else{
                echo "500";
            }
        }

    }
    if(isset($_POST['approve'])){
        $id  = $_POST['id'];

        $sel = "select * from transaksi_upgrade tu where transaksi_upgrade_id = '$id'";
        $res = mysqli_query($con,$sel);

        $do = mysqli_fetch_array($res);
        $tipe_bayar = $do['tipe_bayar'] ;
        $tipe = $do['tipe_agent_after'];
        $m_agent_id = $do['m_agent_id'];

        if($tipe_bayar == "Lisensi Upgrade"){
            $upd = "update transaksi_upgrade set kode_status = 'WT2', status_desc = 'Approved', action_date = now()
            ,action_user = '$_nama' where transaksi_upgrade_id = '$id' ";

            $res = mysqli_query($con,$upd);
            if($res){
                // echo "200";
                    $upd2 = "update m_agent set old_tipe = tipe_agent, tipe_agent = '$tipe' 
                    where m_agent_id = '$m_agent_id' ";

                    // echo $upd2;
                    $res = mysqli_query($con,$upd2);
                    if($res){
                        $upd2 = "update m_agent set old_tipe = tipe_agent, tipe_agent = '$tipe' 
                            where m_agent_id = '$m_agent_id' ";

                        $res = mysqli_query($con,$upd2);
                        if($res){
                            echo "Pembelian PIN";
                        }
                    }
            }else{
                echo "500";
            }
        }else{
            $upd = "update transaksi_upgrade set kode_status = 'WT2', status_desc = 'Approved', action_date = now()
            ,action_user = '$_nama' where transaksi_upgrade_id = '$id' ";

            // echo $upd;
            $res = mysqli_query($con,$upd);
            if($res){
                $upd2 = "update m_agent set old_tipe = tipe_agent, tipe_agent = '$tipe' 
                    where m_agent_id = '$m_agent_id' ";

                    // echo $upd2;
                $res = mysqli_query($con,$upd2);
                if($res){
                    echo "Pembelian PIN";
                }
            }
        }
    }
    if(isset($_POST['conf'])){
        $id  = $_POST['id'];

        $upd = "update transaksi_upgrade set date_know = now() where transaksi_upgrade_id = '$id' ";

        // echo $upd;
        // exit;
        $res = mysqli_query($con,$upd);
        if($res){
            echo "200";
        }else{
            echo "500";
        }

    }
?>