<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    session_start();
    $_user = $_SESSION["m_agent_id"];
    $_nama = $_SESSION['nama'];
    $_role = $_SESSION['role'];
    if(isset($_POST['tp'])){
        $id = $_POST['id'];
        $tgl_bayar = $_POST['tgl_bayar'];
        $tenor = $_POST['tenor'];

        $cektenor = "select count(*) as jml from trans_tabungan where jemaah_id = '$id' and nominal_nabung is null";

        // echo $cektenor;
        $restenor = mysqli_query($con,$cektenor);
        $dtr = mysqli_fetch_array($restenor);

        // echo $dtr['jml']."xx".$tenor ;
        if($dtr['jml'] >= $tenor){
            // echo "cc";
            $sel = "select * from trans_tabungan where jemaah_id = '$id' and coalesce(nominal_nabung,0) = 0 
            order by seq asc limit $tenor ";
            $res = mysqli_query($con,$sel);
            // $tgl = $_POST['tgl_bayar'];
            while($dt = mysqli_fetch_array($res)){
                $id_tab = $dt['trans_tabungan_id'];
                $upd = "update trans_tabungan set nominal_nabung = 650000, biaya_admin = 10000,kode_status = 'WT1'
                , status_nabung = 'Approved',tgl_nabung = '$tgl_bayar', petugas = '$_nama' where trans_tabungan_id = '$id_tab'";

                // echo $upd;
                $rd = mysqli_query($con,$upd);
                if($rd){
                    echo "<script>alert('Berhasil')</script>";
                    echo "<script>window.location='../../?go=nabung&id=".$id."'</script>";
                }
            }
        }else{
            // echo "dcc";
            // echo $dtr['jml']."xx".$tenor ;
            echo "<script>alert('Pembayaran melebihi sisa tenor')</script>";
            echo "<script>window.location='../../?go=nabung&id=".$id."'</script>";
        }
        
    }else if(isset($_GET['id'])){
       $act = $_GET['act'];
       if($act == "btl"){
            if($_role == "admin" || $_role == "administrator"  || $_role == "1"){
                $id = $_GET['id'];
                $wst = $_GET['wst'];
                $selid = "select * from trans_tabungan where trans_tabungan_id = '$id' and jemaah_id = '$wst'";

                // echo $selid;
                $rd = mysqli_query($con,$selid);
                if(mysqli_num_rows($rd) > 0){
                    $dt = mysqli_fetch_array($rd);
                    // if($dt['kode_status'] == "WT1"){
                        $updt = "update trans_tabungan set nominal_nabung =  null, biaya_admin = 10000,kode_status = null
                        , status_nabung = null,tgl_nabung = null, petugas = null,keterangan = null  where trans_tabungan_id = '$id' and jemaah_id = '$wst'";
                        // echo $updt;
                        $hsl = mysqli_query($con,$updt);
                        if($hsl){
                            echo "<script>alert('Berhasil')</script>";
                            echo "<script>window.location='../../?go=nabung&id=".$wst."'</script>";
                        }
                    // }else{
                    //     echo "<script>alert('Tidak diizinkan')</script>";
                    //     echo "<script>window.location='../../?go=nabung&id=".$wst."'</script>";
                    // }
                }else{
                    echo "<script>alert('illegal Akses...')</script>";
                    echo "<script>window.location='../../?go=nabung&id=".$wst."'</script>";
                }
            }else{
                    echo "<script>alert('illegal Role $_role...')</script>";
                    echo "<script>window.location='../../?go=nabung&id=".$wst."'</script>";
            }
       }else if($act == "ver"){
            if($_role == "admin" || $_role == "administrator" || $_role == "1"){
                $id = $_GET['id'];
                $wst = $_GET['wst'];
                $selid = "select * from trans_tabungan where trans_tabungan_id = '$id' and jemaah_id = '$wst'";
                echo $selid;
                $rd = mysqli_query($con,$selid);
                if(mysqli_num_rows($rd) > 0){
                    $dt = mysqli_fetch_array($rd);
                    if($dt['kode_status'] == "WT1"){
                        $updt = "update trans_tabungan set kode_status = 'WT2'
                        , keterangan = 'Terverifikasi', petugas2 = '$_nama' 
                        where trans_tabungan_id = '$id' and jemaah_id = '$wst'";
                        // echo $updt;
                        $hsl = mysqli_query($con,$updt);
                        if($hsl){
                            echo "<script>alert('Berhasil')</script>";
                            echo "<script>window.location='../../?go=nabung&id=".$wst."'</script>";
                        }
                    }else{
                        echo "<script>alert('Tidak diizinkan')</script>";
                        echo "<script>window.location='../../?go=nabung&id=".$wst."'</script>";
                    }
                }else{
                    echo "<script>alert('illegal Role $_role...')</script>";
                    echo "<script>window.location='../../?go=nabung&id=".$wst."'</script>";
                }
            }
       }
    }else if(isset($_GET['biaya'])){
        $id = $_GET['idx'];
        $amount = $_GET['amount'];

        $upd = "update jemaah set biaya_total = '$amount',last_update = now(),update_bayar = '$_nama' 
        where jemaah_id = '$id'";

        $res = mysqli_query($con,$upd);
        if($res){
            echo "200";
        }else{
            echo $upd;
        }
    }else if(isset($_GET['load'])){
        $id = $_GET['idx'];

        $selp = "select sum(coalesce(nominal_nabung,0)) + 2000000 as nabung  from trans_tabungan where 
        jemaah_id = '$id'";
        $re = mysqli_query($con,$selp);
        $dp = mysqli_fetch_array($re);

        $nabung = $dp['nabung'];

        $sel = "select j.*,b.pembayaran_jemaah_id,nominal,kode_status,b.status,doc
            ,jenis,$nabung as nabung,'$_role' as rolez,b.catatan as notes 
            ,date_format(createdate,'%d %M %Y') as input,created_by
            ,coalesce(date_format(j.last_update,'%d %M %Y'),'') as last_update
            ,coalesce(update_bayar,'') as update_bayar
            from jemaah j 
            left join pembayaran_jemaah b on j.jemaah_id = b.jemaah_id 
            where j.jemaah_id = '$id'";

        echo queryJson($con,$sel);
        // echo $sel;
    }else if(isset($_POST['bayarke'])){
        $ke = $_POST['bayarke'];
        $id = $_POST['id'];
        $nominal = $_POST['nominal'];
        $catatan = $_POST['catatan'];

        $nama_file1 = $_FILES['bukti']['name'];
        $data_file1 = $_FILES['bukti']['tmp_name'];

        $path1 = "../../asset/tabungan/$id/".$nama_file1;
        $pathData = "asset/tabungan/$id/".$nama_file1; 

        if( is_dir("../../asset/tabungan/$id") === false ){
            mkdir("../../asset/tabungan/$id/");
        }
        if(move_uploaded_file($data_file1, $path1)){
            $dokumen1 = $pathData;
        }

        $cekDuls = "select * from pembayaran_jemaah where jemaah_id = '$id' and jenis = '$ke'";
        $resx = mysqli_query($con,$cekDuls);
        if(mysqli_num_rows($resx) > 0){
            $op = mysqli_fetch_array($resx);
            $keys = $op['pembayaran_jemaah_id'];
            $upd = "update pembayaran_jemaah set nominal = '$nominal', doc = '$pathData'
                    ,jenis = '$ke',created_by = '$_nama',createdate = now()
                    ,catatan = '$catatan'
                    where  pembayaran_jemaah_id = '$keys'";

            $res = mysqli_query($con,$upd);
            if($res){
                echo "200";
            }else{
                echo $upd;
            }
        }else{
    
            $ins = "insert into pembayaran_jemaah (pembayaran_jemaah_id, jemaah_id,nominal,kode_status,doc,createdate,created_by,jenis,catatan)
            values (uuid(), '$id','$nominal','WT1','$pathData',now(),'$_nama','$ke','$catatan')";
            $res = mysqli_query($con,$ins);
            if($res){
                echo "200";
            }else{
                echo $ins;
            }
        }

        
    }else if(isset($_GET['ver'])){
        $id = $_GET['idx'];
        $jenis = $_GET['jenis'];

        $sel = "select * from pembayaran_jemaah where jemaah_id = '$id' and jenis = '$jenis'";
        $res = mysqli_query($con,$sel);
        $do = mysqli_fetch_array($res);
        $idx = $do['pembayaran_jemaah_id'];

        $upd = "update pembayaran_jemaah set status = 'Terverifikasi' where pembayaran_jemaah_id = '$idx'";
        $hsl = mysqli_query($con,$upd);
        if($hsl){
            echo "200";
            // echo $upd;

        }else{
            echo $upd;
        }
    }
    // echo "As";

?>