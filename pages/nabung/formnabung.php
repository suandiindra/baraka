<?php
    $id = $_GET['id'];
    $cek = "select * from jemaah a where a.jemaah_id = '$id'";
    // echo $cek;
    $nama_jemaah = "";
    $nomor_jemaah = "";
    $nomor_resi = "";
    $nomor_va = "";
    $nama_pasport = "";
    $islunas = "";
    $rd = "";
    $lunas = "";
    $res = mysqli_query($con, $cek);
    if(mysqli_num_rows($res) > 0){
        $cek2 = "select * from trans_tabungan a inner join jemaah b on a.jemaah_id =  b.jemaah_id 
        where a.jemaah_id = '$id'";
        $res2 = mysqli_query($con, $cek2);
        $crtd = mysqli_fetch_array($res);
        $crt = $crtd['created'];
        $nama_jemaah = $crtd['nama_jemaah'];
        $nomor_jemaah = $crtd['nomor_jemaah'];
        $nama_pasport = $crtd['nama_pasport'];
        $nomor_resi = $crtd['nomor_resi'];
        $nomor_va = $crtd['no_va'];
        if(mysqli_num_rows($res2) == 0){
            for($i = 1; $i <= 40; $i++){
                $sc = "SELECT MONTHNAME(DATE_ADD('$crt' , INTERVAL +$i MONTH ) ) as bulan
                ,year(DATE_ADD( '$crt' , INTERVAL +$i MONTH ) ) as tahun";

                $mys = mysqli_query($con,$sc);
                $dr = mysqli_fetch_array($mys);
                $bln = $dr['bulan'];
                $thn = $dr['tahun'];

                $ins = "insert into trans_tabungan  (jemaah_id,seq,bulan,tahun,nominal,biaya_admin)
                values ('$id',$i,'$bln','$thn',650000,10000)";

                // echo $ins."</br>";
                $res = mysqli_query($con,$ins);

            }
        }else{
            $cektenor = "select count(*) as jml from trans_tabungan where jemaah_id = '$id' and nominal_nabung is null";

            // echo $cektenor;
            $restenor = mysqli_query($con,$cektenor);
            $dtr = mysqli_fetch_array($restenor);
            if($dtr['jml'] == "0"){
                $rd = "readonly";
                $islunas = "disabled";
                $lunas = "L U N A S";
            }
        }
    }else{
        
    }
?>
<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tabungan Calon Wisatawan</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./?go=listwisatawan">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Nabung</li>
        </ol>
    </div>
</div>
<div class="row mb-3" style="margin-top:-30px">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                <!-- Input Data tabungan Calon Wisatawan disini -->
            </div>
            <div class="card-body" id="formInput" style = "display:block; margin-top:-30px">
                <Form method="POST" onsubmit="return submitForm(this);" action="pages/nabung/actionnabung.php" enctype="multipart/form-data">
                    <input type="hidden"  name="id" class="form-control" value= "<?php echo $id ?>" >
                    <input type="hidden"  name="tp" class="form-control" value= "<?php echo "add" ?>" >

                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">ID Wisatawan</label>
                        <input type="text" readonly  name="nama" class="form-control" value= "<?php echo $nomor_jemaah ?>" >
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nomor Resi</label>
                        <input type="text" readonly  name="ktp" class="form-control" value= "<?php echo $nomor_resi ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nama Wisatawan</label>
                        <input type="text" readonly  name="hp_wisatawan" class="form-control" value= "<?php echo $nama_jemaah ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nama Pasport</label>
                        <input type="text" readonly  name="hp_wisatawan" class="form-control" value= "<?php echo $nama_pasport ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nomor VA</label>
                        <input type="text" readonly  name="va" class="form-control" value= "<?php echo $nomor_va ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nominal Tabungan + Biaya Admin</label>
                        <input type="text" readonly id = "nom" name="nominal" class="form-control" value= "<?php echo number_format(660000) ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Jumlah Tenor</label>
                        <input type="number" <?php echo $rd ?> required id="tenor"  name="tenor" onchange="hitung()" class="form-control" value="1">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Tanggal Bayar</label>
                        <input type="date" required <?php echo $rd ?> name="tgl_bayar" class="form-control">
                    </div>
                   
                    <div>
                        <Button <?php echo $islunas; ?> class="btn btn-success">Konfirmasi</Button>
                       <b> <h3 style="color:red; margin-top:10px">  <?php echo $lunas ?> </h3></b>
                    </div>
                </form>
                <hr/>
                <div class="row">
                    <div class="col col-md-8">
                        <label for="inputPassword5" class="form-label">Total Pembayaran</label>
                        <input type="text" id="biaya" placeholder="Total Biaya Perjalanan" class="form-control">
                        <p class="text-danger" id="info"></p>
                    </div>
                    <div class="col col-md-4" >
                        <button style="margin-top:35px" class="btn btn-success" onclick="setBiaya()"> Save</button>
                    </div>
                </div> 
                <br/>

                <div class="row">
                    <div class="col col-md-8">
                        <label for="inputPassword5" class="form-label">(41.A) Pembayaran I</label>
                        <input type="text" id="bayar1" placeholder="Pembayaran I" class="form-control">
                        <p class="text-danger" id="info1"></p>
                    </div>
                    <div class="col col-md-2"  style="margin-top:35px">
                        <input type="file" class="form-control" id="upload_bayar1">
                    </div>
                    <div class="col col-md-2"  style="padding-top:35px">
                        <button class="btn btn-danger" id="btn1" onclick="setBayarke(1,this)">
                            Save
                        </button>
                        <button class="btn btn-warning" style="display:none" id="ver1" onclick="verify(1)">Verifikasi</button>
                        <a href="" id="file1" style="display:none">Download</a>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col col-md-8">
                    <label for="inputPassword5" class="form-label">(41.B) Pembayaran II</label>
                        <input type="text" id="bayar2" placeholder="Pembayaran II" class="form-control">
                        <p class="text-danger" id="info2"></p>
                    </div>
                    <div class="col col-md-2"  style="margin-top:35px">
                        <input type="file" class="form-control" id="upload_bayar2" >
                    </div>
                    <!-- <div class="col col-md-2"  style="margin-top:35px">
                        <button class="btn-ver2" style="display:block" onchange="verify(this)">Verifikasi</button>
                    </div> -->
                    <div class="col col-md-2" style="margin-top:35px">
                        <button class="btn btn-danger" id="btn2" onclick="setBayarke(2,this)">
                            Save
                        </button>
                        <button class="btn btn-warning" style="display:none"  id="ver2" onclick="verify(2)">Verifikasi</button>
                        <a href="" id="file2" style="display:none">Download</a>
                    </div>
                </div> 
                <br/>
                <div class="row">
                    <div class="col col-md-8">
                        <label for="inputPassword5" class="form-label">Biaya Tambahan</label>
                        <input type="text" id="bayar3" placeholder="Biaya Tambahan" class="form-control">
                        <p class="text-danger" id="foo"></p>
                    </div>
                    <div class="col col-md-2" style="margin-top:35px">
                        <input type="file" class="form-control" id="upload_bayar3">
                    </div>
                    <div class="col col-md-2" style="margin-top:35px">
                        <button class="btn btn-danger" id="btn3" onclick="setBayarke(3,this)">
                            Save
                        </button>
                        <button class="btn btn-warning" style="display:none"  id="ver3" onclick="verify(3)">Verifikasi</button>
                        <a href="" id="file3" style="display:none">Download</a>
                    </div>
                </div> 
                <div class="table-responsive p-3" style="margin-top:20px">
                  <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                    <thead class="thead-light">
                      <tr>
                        <th>No</th>
                        <th>Pembayaran Ke-</th>
                        <th>Nominal</th>
                        <th>Biaya Admin</th>
                        <th>Tgl Bayar</th>
                        <th>Petugas</th>
                        <th>Status</th>
                        <th>Keterangan</th>
                        <th>Petugas Verify</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sel = "select *,DATE_FORMAT(tgl_nabung,'%d-%M-%Y') as tgl_nabung1 from trans_tabungan where jemaah_id = '$id'";
                            $result = mysqli_query($con,$sel);
                            $i = 1;
                            while($res = mysqli_fetch_array($result)){
                            $sty = "";
                            if($res['kode_status'] == "WT2"){
                                $sty = "background-color:#55ACEE;color:white";
                            }
                            $idx = $res['trans_tabungan_id'];
                        ?>
                            <tr style="<?php echo $sty; ?>">
                                <td><?php echo $i; ?></td>
                                <td><?php echo $res['seq']." / 40"; ?></td>
                                <td><?php echo number_format($res['nominal_nabung']); ?></td>
                                <td><?php echo $res['nominal_nabung'] ? number_format($res['biaya_admin']) : 0; ?></td>
                                <td><?php echo $res['tgl_nabung1']; ?></td>
                                <td><?php echo $res['petugas']; ?></td>
                                <td><?php echo $res['status_nabung']; ?></td>
                                <td><?php echo $res['keterangan']; ?></td>
                                <td><?php echo $res['petugas2']; ?></td>
                                <td>
                                    <?php
                                        if($res['nominal_nabung'] && $res['kode_status'] != "WT2"){
                                    ?>
                                        <a onclick='javascript:confirmationDelete($(this));return false;' href="pages/nabung/actionnabung.php?id=<?php echo $idx; ?>&act=ver&wst=<?php echo $id ?>"><button class="btn-primary">Verifikasi</button></a>
                                    <?php
                                        }

                                        if($res['nominal_nabung'] && $_SESSION['role'] != "admin"){
                                    ?>
                                         <a onclick='javascript:confirmationDelete($(this));return false;' href="pages/nabung/actionnabung.php?id=<?php echo $idx; ?>&act=btl&wst=<?php echo $id ?>"><button class="btn-danger">Batalkan</button></a>
                                    <?php
                                        }
                                    ?>
                                </td>
                                
                            </tr>
                        <?php
                            $i += 1;
                            }
                        ?>
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.addEventListener('load', loads, false);

    function setBayarke(v,t){
        var form_data = new FormData();      
        form_data.append('bayarke', v);
        form_data.append('id', "<?php echo $_GET['id'] ?>");
        var bukti = "";
        var nominal = 0;
        if(v == 1){
            bukti = $('#upload_bayar1').prop('files')[0]; 
            nominal = $(`#bayar1`).val()
        }else if(v == 2){
            bukti = $('#upload_bayar2').prop('files')[0]; 
            nominal = $(`#bayar2`).val()
        }else if(v == 3){
            bukti = $('#upload_bayar3').prop('files')[0]; 
            nominal = $(`#bayar3`).val()
        }
        if(!bukti || nominal == 0){
            alert(`Lengkapi isian`)
            return
        }

        let catatan = ""
        if(v == 3){
            let foo = prompt('Masukan Alasan Revisi');
            if(!foo){
                return
            }
            catatan = foo
        }
        
        $(`#${t.id}`).html(`Please Wait...`)

        form_data.append('nominal', nominal.replace(/\./g,''));
        form_data.append('catatan', catatan);
        form_data.append('bukti', bukti);
        console.log(form_data);
        // return
        $.ajax({
            url: 'pages/nabung/actionnabung.php', // <-- point to server-side PHP script 
            dataType: 'text',  // <-- what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(res){
                console.log(res);
                $(`#${t.id}`).html(`Save`)
                alert(`Berhasil`)
                loads()
            }
        });
    }

    function setBiaya(){
        // let exc = '45.000.000'
        // console.log(exc.replace(/\./g,''));

        $.ajax({
            type:'GET',
            url:'pages/nabung/actionnabung.php',
            data:{
                biaya:true,
                idx : "<?php echo $_GET['id'] ?>",
                amount : $(`#biaya`).val().replace(/\./g,'')
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    alert('Berhasil')
                    loads()
                }
            }
        })
    }
    function duit(v){
        var bilangan = v;
        if(bilangan.toString().includes("-")){
            // bilangan = bilangan.substring(1)
        }
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join('.').split('').reverse().join('');

        if(bilangan.toString().includes("-")){
            return `-${ribuan}`
        }
        return ribuan
    }

    function verify(v){
        console.log(v);
        $.ajax({
            type:'GET',
            url:'pages/nabung/actionnabung.php',
            data:{
                ver:true,
                idx : "<?php echo $_GET['id'] ?>",
                jenis : v
            },
            success: function(data){
                console.log(data);
                if(data == "200"){
                    // alert('Berhasil')
                    // loads()
                    window.location = "./?go=nabung&id=<?php echo $_GET['id'] ?>"
                }
            }
        })
    }
    function loads(){
        $.ajax({
            type:'GET',
            url:'pages/nabung/actionnabung.php',
            data:{
                load:true,
                idx : "<?php echo $_GET['id'] ?>"
            },
            success: function(data){
                // console.log(data);
                let objek = JSON.parse(data)
                let obj = objek.data[0]
                
                // console.log(data);
                // return;

                if(obj.biaya_total){
                    $(`#biaya`).val(duit(obj.biaya_total))
                }
                objek = objek.data
                for(let i = 0; i < objek.length; i ++){
                    objek[i].update_bayar ? $(`#info`).html(objek[i].update_bayar+", "+objek[i].last_update) : ""

                    if(objek[i].jenis){
                        // console.log(objek[i].jenis,objek[i].doc);
                        if(objek[i].jenis == "1"){
                            $(`#bayar1`).val(duit(objek[i].nominal))
                           
                            $('#file1').attr('href',objek[i].doc)
                            $(`#file1`).css('display', 'block');
                            $(`#btn1`).css('display', 'none');
                            if(objek[i].kode_status == "WT1" && !objek[i].status){
                                $(`#ver1`).css('display', 'block');
                            }

                            if(objek[i].rolez == "1"){
                                $(`#upload_bayar1`).prop('disabled', false);
                                $(`#btn1`).css('display', 'block');
                            }else{
                                $(`#upload_bayar1`).prop('disabled', true);
                            }
                            $(`#info1`).html(objek[i].created_by+", "+objek[i].input)

                        }else if(objek[i].jenis == "2"){    
                            $(`#bayar2`).val(duit(objek[i].nominal))
                            if(objek[i].rolez !== "1"){
                                $(`#upload_bayar2`).prop('disabled', true);
                            }
                            $('#file2').attr('href',objek[i].doc)
                            $(`#file2`).css('display', 'block');
                            $(`#btn2`).css('display', 'none');
                            if(objek[i].kode_status == "WT1" && !objek[i].status){
                                $(`#ver2`).css('display', 'block');
                            }

                            if(objek[i].rolez == "1"){
                                $(`#upload_bayar2`).prop('disabled', false);
                                $(`#btn2`).css('display', 'block');
                            }else{
                                $(`#upload_bayar2`).prop('disabled', true);
                            }
                            $(`#info2`).html(objek[i].created_by+", "+objek[i].input)

                        }else if(objek[i].jenis == "3"){
                            $(`#bayar3`).val(duit(objek[i].nominal))
                            if(objek[i].rolez !== "1"){
                                $(`#upload_bayar3`).prop('disabled', true);
                            }
                            $('#file3').attr('href',objek[i].doc)
                            $(`#file3`).css('display', 'block');
                            $(`#btn3`).css('display', 'none');
                            if(objek[i].kode_status == "WT1" && !objek[i].status){
                                $(`#ver3`).css('display', 'block');
                            }

                            $(`#foo`).html(objek[i].created_by+", "+objek[i].input+" ("+objek[i].notes+")")


                            if(objek[i].rolez == "1"){
                                $(`#upload_bayar3`).prop('disabled', false);
                                $(`#btn3`).css('display', 'block');
                            }else{
                                $(`#upload_bayar3`).prop('disabled', true);
                            }
                        }
                    }
                }
            }
        })
    }
    function confirmationDelete(anchor)
    {
    var conf = confirm('Apakah yakin melakukan proses ini ???');
    if(conf)
        window.location=anchor.attr("href");
    }

    function submitForm() {
        return confirm('Yakin akan melanjutkan transaksi .. ?');
    }

    function hitung(){
        var tenor = document.getElementById("tenor").value;
        
        var nominal = 0;
        if(tenor > 0){
            nominal = (650000 * tenor) + 10000;
        }
        console.log(nominal);
        document.getElementById("nom").value = uang(nominal);
    }

    function uang(v){
        var	number_string = v.toString(),
        sisa 	= number_string.length % 3,
        rupiah 	= number_string.substr(0, sisa),
        ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        return rupiah;
    }
</script>