<?php
    $tot = 0;
?>
<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">History Tabungan Calon Wisatawan</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Nabung</li>
        </ol>
    </div>
</div>

<div class="row mb-3" style="margin-top:-30px">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-body" id="formInput" style = "display:block; margin-top:0px">
            <?php
                $sel = "select a.*,b.nama,b.nomor_agent as agn,DATE_FORMAT(created,'%d-%M-%Y %H:%i') as crt 
                ,DATE_FORMAT(a.tgl_lahir,'%d-%M-%Y') as born, DATE_FORMAT(created,'%d') as tgl, DATE_FORMAT(now(),'%M') as nw
                from jemaah a
                inner join m_agent b on a.m_agent_id = b.m_agent_id
                where jemaah_id = '$_user'";

                // echo $sel;
                $result = mysqli_query($con,$sel);
                $data = mysqli_fetch_array($result);

            ?>  
              <a href="./pages/nabung/cetakwisatawan.php">  <Button class="btn btn-outline-primary">Cetak Tabungan</Button> </a>
                <table class="table" style="margin-top:30px">
                    <tr>
                        <td><b><label for="inputPassword5" class="form-label">Nama Wisatawan</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label">:</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label"><?php echo $data['nama_jemaah'] ?></label></b></td>
                    </tr>
                    <tr>
                        <td><b><label for="inputPassword5" class="form-label">Nama Pasport</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label">:</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label"><?php echo $data['nama_pasport'] ?></label></b></td>
                    </tr>
                    <tr>
                        <td><b><label for="inputPassword5" class="form-label">ID Wisatawan</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label">:</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label"><?php echo $data['nomor_jemaah'] ?></label></b></td>
                    </tr>
                    <tr>
                        <td><b><label for="inputPassword5" class="form-label">Tanggal Daftar</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label">:</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label"><?php echo $data['crt'] ?></label></b></td>
                    </tr>
                    <tr>
                        <td><b><label for="inputPassword5" class="form-label">Jatuh Tempo</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label">:</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label"><?php echo $data['tgl'] ?></label></b></td>
                    </tr>
                    <tr>
                        <td><b><label for="inputPassword5" class="form-label">Durasi Cicilan</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label">:</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label"><?php echo "40 Bulan/Kali" ?></label></b></td>
                    </tr>
                    <tr>
                        <td><b><label for="inputPassword5" class="form-label">Nomor Va</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label">:</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label"><?php echo $data['no_va'] ?></label></b></td>
                    </tr>
                    <tr>
                        <td><b><label for="inputPassword5" class="form-label">Nomor Resi</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label">:</label></b></td>
                        <td><b><label for="inputPassword5" class="form-label"><?php echo $data['nomor_resi'] ?></label></b></td>
                    </tr>
                    <tr id="form1">
                        
                    </tr>
                    <tr id="form2">
                        
                    </tr>
                    <tr id="form3">
                        
                    </tr>
                </table>
                <?php
                    $perkiraan = "select MONTHNAME(DATE_ADD('".$data['createddate']."' , INTERVAL +40 MONTH ) ) bln, YEAR(DATE_ADD('".$data['createddate']."', INTERVAL +40 MONTH ) ) thn";
                    $rt = mysqli_query($con,$perkiraan);
                    $rt = mysqli_fetch_array($rt);
                ?>
                <div style="float:right; margin-right:30px">
                   <p> Perkiraan Pelunasan : <?php  echo $rt['bln']." ".$rt['thn']; ?></p>
                   <p>Catatan : Wisatawan dapat lebih awal melakukan pelunasan</p>

                </div>
                <div class="table-responsive">
                <table class="table" style="margin-top:30px">
                    <thead class="bg-primary" style="color:white">
                    <tr>
                        <th scope="col">pembayaran Ke-</th>
                        <th scope="col">Nominal</th>
                        <th scope="col">Admin</th>
                        <th scope="col">Tgl Bayar</th>
                        <th scope="col">Status</th>
                        <!-- <th scope="col">Keterangan</th> -->
                        <th scope="col">Petugas</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                            $seldetail = "select *,DATE_FORMAT(tgl_nabung,'%d-%M-%Y') as tgl_nabung1  from trans_tabungan where 
                            jemaah_id = '$_user' order by seq asc";
                            $tot = 0;
                            // echo $seldetail;
                            $res1 = mysqli_query($con,$seldetail);
                            while($da = mysqli_fetch_array($res1)){
                        ?>
                            <tr>
                                <td ><?php echo $da['seq']."/ 40"; ?></td>
                                <td ><?php echo number_format( $da['nominal_nabung']); ?></td>
                                <td ><?php echo $da['nominal_nabung'] ? number_format($da['biaya_admin']) : "0"; ?></td>
                                <td ><?php echo $da['tgl_nabung1']; ?></td>
                                <td ><?php echo $da['status_nabung']; ?></td>
                                <!-- <td ><?php echo $da['keterangan']; ?></td> -->
                                <td ><?php echo $da['petugas']; ?></td>
                            </tr>
                        <?php
                            $tot += $da['nominal_nabung'];
                            }
                        ?>
                    </tbody>
                </table>
                </div>
               <i><b> Baraka berhak setiap saat melakukan koreksi, apabila terjadi kesalahan laporan ini</b> </i>
               <br>
               <?php
                $cektenor = "select count(*) as jml from trans_tabungan where jemaah_id = '$_user' and nominal_nabung is not null";

                $lunas = "";
                        // echo $cektenor;
                $restenor = mysqli_query($con,$cektenor);
                $dtr = mysqli_fetch_array($restenor);
                if($dtr['jml'] == "40"){
                    $lunas = "L  U  N  A  S";
                }
                    echo "<center style='margin-top:20px; color:red'> <b><h2>".$lunas."</h2></b></center>";
               ?>
               
                <div class="row">
                    <div class="col col-md-7">
                        <table class="table" style="margin-top:30px">
                            <tr>
                                <td>Down Payment</td>
                                <td>:</td>
                                <td><?php echo number_format(2000000) ?></td>
                            </tr>
                            <tr>
                                <td>Total Tabungan</td>
                                <td>:</td>
                                <td><u><?php echo number_format($tot) ?></u></td>
                            </tr>
                            <tr>
                                <td><b>Total saldo (DP + Tot. Tab)</b></td>
                                <td>:</td>
                                <td><b><?php echo number_format($tot + 2000000) ?></b></td>
                            </tr>
                            <tr>
                                <td colspan="3" class="bg-secondary"></td>
                            </tr>
                            <tr id="bayar1">
                                
                            </tr>
                            <tr id="bayar2">
                                
                            </tr>
                            <tr id="div_total_saldo">
                                <!-- <td><b>Total saldo
                                    (DP + Tot. Tab. + Pelunasan/ Penyesuaian)*
                                    </b></td>
                                <td>:</td>
                                <td><b id="total_saldo">0</b></td> -->
                            </tr>
                            <tr class="bg-secondary">
                                <td colspan="3"  style="height: -20px;"></td>
                            </tr>
                            <tr>
                                <td>Total Biaya Perjalanan</td>
                                <td>:</td>
                                <td id="total">0</td>
                            </tr>
                            <tr id="bayar3">
                                <td>Biaya Tambahan</td>
                                <td>:</td>
                                <td id="bayar3_amt">0</td>
                            </tr>
                            <tr id="final_saldo_form">
                                
                            </tr>
                        </table>
                    </div>
                    <div class="col col-md-5">
                        <table class="table table-primary" style="margin-top:30px">
                            <tr>
                                <td colspan=2>
                                    <b style="color:red">Simulasi</b>
                                </td>
                            </tr>
                            <tr>
                                <td>Down Payment</td>
                                <td>:</td>
                                <td><?php echo number_format(2000000) ?></td>
                            </tr>
                            <tr>
                                <td>Total Tabungan (20x)</td>
                                <td>:</td>
                                <td><u><?php echo number_format(13500000) ?></u></td>
                            </tr>
                            <tr>
                                <td>Total Dana Masuk</td>
                                <td>:</td>
                                <td><?php echo number_format(13500000 + 2000000) ?></td>
                            </tr>
                            <tr>
                                <td>*Contoh Total Biaya</td>
                                <td>:</td>
                                <td><?php echo number_format(28850000) ?></td>
                            </tr>
                            <tr>
                                <td>Selisih</td>
                                <td>:</td>
                                <td><?php echo number_format(13850000 + 0) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                
                <div class="row" style="margin-top:35px">
                    <div class="col col-md-7">
                        <img src="img/t1.png" style="width:100%"/>
                        <img src="img/t2.png" style="width:100%"/>
                    </div>
                </div>
                <!-- <p style="color:red">Informasi & Edukasi : </p> -->
                <!-- <p id='notes'></p>
                <div class="row">
                    <div class="col col-md-7" style="margin: auto; width: 50%;text-align: justify;text-justify: inter-word;">
                        <div class="center" style="text-align: center">
                        <i><b>Menabung, suatu konsep agar lebih mudah terjadi keberangkatan profesional, dan memudahkan petugas memberikan pelayanan yang prima
Menabung langkah awal menuju kota impian
PT. Baraka Insan Mandiri
Persiapan Pra PPIU menuju PPIU </br> PT. Baraka Insan Mandiri</i></b>
                        </div>
                    </div>
                    <div class="col"></div>
                </div> -->
            </div>
        </div>
    </div>
</div>

<script>
    window.addEventListener('load', load, false);
    function load(){
        $(`#notes`).html(`1. Biaya Total perjalanan di sesuaikan oleh saat wisatawan/ jamaah akan berangkat </br>
        &nbsp&nbsp&nbsp&nbspBulan / Tahun saat pelunasan tahun berjalan, hal ini di sebabkan penyesuian atas inflasi di tahun tersebut dll.</br>
        2. Edukasi Faktor yang mempengaruhi Total Biaya Perjalanan, di antara nya :</br>
        &nbsp&nbsp&nbsp&nbspa. Saat Bulan keberangkatan (contoh : biaya akhir tahun/Desember tentu lebih tinggi dari pada bulan biasanya atau bulan Ramadhan / masa liburan).</br>
        &nbsp&nbsp&nbsp&nbspb. Jumlah Wisatawan/ jamaah dalam per Group Keberangkatan (share cosh bus di negara tujuan semakin rendah, atas jumlah jamaah per Bus)</br>
        &nbsp&nbsp&nbsp&nbspc. Serta hotel/ Jarak hotel dan maskapai yang di pergunakan, serta Durasi keberangkatan dll.</br>
        &nbsp&nbsp&nbsp&nbspd. Serta masih banyak faktor yang mempengaruhi suatu biaya perjalanan, terlebih ini perjalanan jarak jauh/ lintas benua</br>
        &nbsp&nbsp&nbsp&nbspe.	Dimohon setiap wisatawan/ Jamaah memiliki dana saku lebih guna keperluan mendadakan dalam perjalanan nanti nya,</br>
        &nbsp&nbsp&nbsp&nbsp&nbsp&nbspserta mempersiapkan kesehatan dan lain-lain dengan baik.</br>
        Edukasi ini di sajikan BARAKA, guna membuka wawasan yang lebih luas serta dapat tercapai Pelayanan yang Prima bersama Travel Baraka`)
        $.ajax({
            type:'GET',
            url:'pages/nabung/actionnabung.php',
            data:{
                load:true,
                idx : "<?php echo $_SESSION['m_agent_id'] ?>"
            },
            success: function(data){
                // console.log(data);
                // return
                let obj = JSON.parse(data)
                obj = obj.data
                console.log(obj);
                let selh = 0
                let biaya_tambahan = 0

                for(let i = 0; i < obj.length; i++){
                    // console.log(obj[0].biaya_total);
                    let biaya_total = obj[0].biaya_total
                        if(obj[i].jenis == "1"){
                            selh = selh + parseFloat(obj[i].nominal ? obj[i].nominal : 0)
                            $(`#bayar1`).html(`<td>Pelunasan/Selisih I</td>
                                <td>:</td>
                                <td id="bayar1_amt">${duit(obj[i].nominal)}</td>`)

                            $(`#form1`).html(`<td><b><label for="inputPassword5" class="form-label">Pelunasan/Selisih I</label></b></td>
                            <td><b><label for="inputPassword5" class="form-label">:</label></b></td>
                                <td>
                                    <b><label for="inputPassword5" class="form-label">${duit(obj[i].nominal)}</label></b>
                                    <a href="${obj[i].doc}" style="display:none"><button class="btn bg-danger text-white">Dokumen</button></a>
                            </td>`)
                        }
                        if(obj[i].jenis == "2"){
                            selh = selh + parseFloat(obj[i].nominal ? obj[i].nominal : 0)
                            $(`#bayar2`).html(`<td>Pelunasan/Selisih II</td>
                                <td>:</td>
                                <td id="bayar2_amt">${duit(obj[i].nominal)}</td>`)

                            $(`#form2`).html(`<td><b><label for="inputPassword5" class="form-label">Pelunasan/Selisih II</label></b></td>
                                <td><b><label for="inputPassword5" class="form-label">:</label></b></td>
                                    <td>
                                        <b><label for="inputPassword5" class="form-label">${duit(obj[i].nominal)}</label></b>
                                        <a href="${obj[i].doc}" style="display:none"><button class="btn bg-danger text-white">Dokumen</button></a>
                                </td>`)
                        }
                    if(biaya_total > 0){
                        if(obj[i].jenis == "3"){
                            $(`#bayar3`).html(`<td>Biaya Tambahan</td>
                                <td>:</td>
                                <td id="bayar3_amt">${duit(obj[i].nominal)}</td>`)
                            biaya_tambahan = obj[i].nominal

                            $(`#form3`).html(`<td><b><label for="inputPassword5" class="form-label">Biaya Tambahan</label></b></td>
                                <td><b><label for="inputPassword5" class="form-label">:</label></b></td>
                                    <td>
                                        <b><label for="inputPassword5" class="form-label">${duit(obj[i].nominal)}</label></b>
                                        <a href="${obj[i].doc}" style="display:none"><button class="btn bg-danger text-white">Dokumen</button></a>
                                </td>`)
                        }
                    }
                    $(`#total`).html(duit(biaya_total))
                    
                    if(selh > 0 ){
                        let po = `<td><b>Total saldo
                                    (DP + Tot. Tab. + Pelunasan/ Penyesuaian)*
                                    </b></td>
                                <td>:</td>
                                <td><b id="total_saldo">0</b></td>`
                        $(`#div_total_saldo`).html(po)
                    }
                    $(`#total_saldo`).html(duit(parseFloat(obj[0].nabung) + parseFloat(selh)))
                    // $(`#total_saldo`).html(duit(parseFloat(obj[0].nabung)))
                    if(biaya_tambahan == 0){
                        // $(`#final_saldo_form`).css("display","none");
                    }else{
                        // $(`#final_saldo_form`).css("display","block");
                        let lp = `<td><b>Total saldo
                                (DP + Tot. Tab. + Pelunasan/ Penyesuaian + Biaya Tambahan). 
                                </b></td>
                                <td>:</td>
                                <td><b id="final_saldo">0</b></td>`
                        $(`#final_saldo_form`).html(lp)
                    }


                    $(`#final_saldo`).html(duit(parseFloat(biaya_total) + parseFloat(biaya_tambahan)))
                    // if(obj[i].jenis){

                    // }
                }
                if(biaya_tambahan == 0){
                    $(`#bayar3`).html(``)
                }
            }
        })
    }
    function duit(v){
        if(!v){
            return 0
        }
        var 	bilangan = v;
        var	reverse = bilangan.toString().split('').reverse().join(''),
            ribuan 	= reverse.match(/\d{1,3}/g);
            ribuan	= ribuan.join('.').split('').reverse().join('');

        // Cetak hasil	
        // console.log(ribuan);
        return ribuan
    }
</script>