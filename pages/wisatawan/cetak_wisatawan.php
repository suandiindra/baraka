<?php
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    include_once "../../utility/fpdf17/fpdf.php";
    session_start();
    if(isset($_GET['id'])){
        $id = $_GET['id'];
    }else{
        return;
    }

    $sel = "select a.*,b.nama,b.nomor_agent as agn,DATE_FORMAT(created,'%d-%M-%Y %H:%i') as crt 
    ,DATE_FORMAT(a.tgl_lahir,'%d-%M-%Y') as born,DATE_FORMAT(tgl_va,'%d-%M-%Y') as tgl_no_va 
    , DATE_FORMAT(created,'%d') as tgl, DATE_FORMAT(now(),'%M') as nw
    ,nama_pasport
    from jemaah a
    inner join m_agent b on a.m_agent_id = b.m_agent_id
    where jemaah_id = '$id'";
    $result = mysqli_query($con,$sel);
    $data = mysqli_fetch_array($result);

    $pdf = new FPDF('P','mm',array(210,297)); //L For Landscape / P For Portrait
    $pdf->AddPage();
    $pdf->SetX(5);
    $pdf->SetFont('Times','B',15);
    
    if($_SESSION['role'] == "Agent"){
        $pdf->Ln(15);
        $pdf->SetX(35);
        $pdf->Cell(10,8,'History Data Wisatawan Peradaban islam di Eropa');
    }else{
        $pdf->Ln(35);
        $pdf->Image("../../img/logo.jpg",10,15,185);
        $pdf->SetX(35);
        $pdf->Cell(10,8,'Data Peserta Program Tabungan Peradaban Islam di Eropa');
       
    }

    $path = "../../".$data['bukti'];
    // $pdf->Image("../../img/logo.jpg",10,25,-1150);
    // $pdf->SetX(30);
    // $pdf->SetFont('Times','',8);
    // $pdf->Cell(40,18,'PT. BARAKA INSAN MANDIRI');
    // $pdf->Ln(3);
    // $pdf->SetX(30);
    // $pdf->Cell(40,18,'Jl. Rawa Sumur III Blok DD No.13');
    // $pdf->Ln(3);
    // $pdf->SetX(30);
    // $pdf->Cell(40,18,'PULO GADUNG - JAKARTA');
    // $pdf->Ln(5);
    // $pdf->SetX(30);

    //bukti daftar 
    $pdf->Image($path,110,70,80,80);


    $pdf->Ln(0);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'____________________________________________________________________________________________________________________');
    $pdf->Ln(10);
    $pdf->SetFont('Times','B',10);
    $pdf->Cell(10,18,'*  Data Agent');
    $pdf->Ln(10);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nomor Agent');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['agn']);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nama Agent');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['nama']);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nomor Resi');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','B',12);
    $pdf->Cell(10,18,$data['nomor_resi']);

    $pdf->Ln(10);
    $pdf->SetFont('Times','B',10);
    $pdf->Cell(10,18,'*  Data Wisatawan');

    $pdf->Ln(10);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'ID Wisatawan');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['nomor_jemaah']);

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nama');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,strtoupper($data['nama_jemaah']));

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nama Pasport');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,strtoupper($data['nama_pasport']));

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Tgl Registrasi');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['crt']);

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'No. KTP');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['nomor_ktp']);

    if($_SESSION['role'] == "Agent"){
        
    }else{
        $pdf->Ln(15);
        $pdf->SetFont('Times','B',9);
        $pdf->Cell(10,18,'No. VA');
        $pdf->SetX(40);
        $pdf->Cell(10,18,':');
        $pdf->SetX(45);
        $pdf->SetFont('Times','',9);
        $pdf->Cell(10,18,$data['no_va']); 

        $pdf->Ln(5);
        $pdf->SetFont('Times','B',9);
        $pdf->Cell(10,18,'Pencetak VA');
        $pdf->SetX(40);
        $pdf->Cell(10,18,':');
        $pdf->SetX(45);
        $pdf->SetFont('Times','',9);
        $pdf->Cell(10,18,$data['petugas']." / ".$data['tgl_no_va']); 
    }
     

    $pdf->Ln(8);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Tempal Tgl Lahir');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['tempat_lahir'].",  ".$data['born']);

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Jenis Kelamin');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['jenis_kelamin']);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Alamat');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['alamat']);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'RT/RW');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['rt']."/".$data['rw']);
    
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Kelurahan/Desa');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['kelurahan']);

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Kecamatan');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['kecamatan']);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Kabupaten/Kota');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['kota']);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Provinsi');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['provinsi']);
    
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Kode Pos');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['kode_pos']);  
    $pdf->Ln(5);
    // $pdf->SetFont('Times','B',9);
    // $pdf->Cell(10,18,'Provinsi');
    // $pdf->SetX(40);
    // $pdf->Cell(10,18,':');
    // $pdf->SetX(45);
    // $pdf->SetFont('Times','',9);
    // $pdf->Cell(10,18,$data['kelurahan']);
    
    $pdf->Ln(9);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'NPWP');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['npwp']);  
    
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Status');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['status']);  

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Pekerjaan');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['pekerjaan']);  

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Email');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['email']);  

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Embarkasi');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['embarkasi']);  

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'____________________________________________________________________________________________________________________');

    $pdf->Ln(10);
    $pdf->SetFont('Times','B',10);
    $pdf->Cell(10,18,'*  Data Ahli Waris');

    $pdf->Ln(10);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nama Ahli Waris');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['nama_ahli_waris']);

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nomor HP');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['hp_ahli_waris']); 

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Hubungan');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['hubungan']);  

    // halaman selanjutnya
    $pdf->Ln(100);
    // $pdf->SetFont('Times','U',7);
    // $pdf->Cell(10,18,'Tgl Cetak VA');
    // $pdf->Image("../../img/logo.jpg",10,995,185);
    $pdf->SetFont('Times','B',12);
    $pdf->SetX(25);
    $pdf->Cell(10,8,'Lembar Kontrol Khusus Milik Wisatawan Paket Tabungan Peradaban Islam di Eropa');
    $pdf->Ln(0);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'____________________________________________________________________________________________________________________');
    $pdf->Ln(10);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nama Wisatawan');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,strtoupper($data['nama_jemaah']));

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'ID Wisatawan');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,strtoupper($data['nomor_jemaah']));

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Tgl Daftar');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,($data['crt']));

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Jatuh Tempo');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['tgl']);

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Durasi Cicilan');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,"40 Bulan/Kali");

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nomor VA');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['no_va']);

    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Nomor Resi');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,$data['nomor_resi']);

    $perkiraan = "select MONTHNAME(DATE_ADD('".$data['createddate']."' , INTERVAL +40 MONTH ) ) bln, YEAR(DATE_ADD('".$data['createddate']."', INTERVAL +40 MONTH ) ) thn";
    $rt = mysqli_query($con,$perkiraan);
    $rt = mysqli_fetch_array($rt);
    $pdf->SetX(120);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,"Perkiraan Pelunasan : ".$rt['bln'].", ".$rt['thn']."");
    $pdf->Ln(5);
    $pdf->SetX(120);
    $pdf->Cell(10,18,"Catatan : Wisatawan dapat lebih awal melakukan pelunasan");

    // table

    $pos = 75;
    $pdf->Ln(10);
    $Y_Fields_Name_position = $pos;
    $pdf->SetFillColor(255, 255, 255);
    $pdf->SetFont('Times','B',8);
    $pdf->SetY($Y_Fields_Name_position);
    $pdf->SetX(10);
    $pdf->Cell(25,10,'Pembayaran Ke-',1,0,'C',1);
    $pdf->SetX(35);
    $pdf->Cell(25,10,'Nominal',1,0,'C',1);
    $pdf->SetX(60);
    $pdf->Cell(20,10,'Admin',1,0,'C',1);
    $pdf->SetX(80);
    $pdf->Cell(30,10,'Tgl Bayar',1,0,'C',1);
    $pdf->SetX(110.0);
    $pdf->Cell(25,10,'Status',1,0,'C',1);    // $pdf->Cell(10,18,$perkiraan);
    $pdf->SetX(135.0);
    $pdf->Cell(25,10,'Keterangan',1,0,'C',1); 
    $pdf->SetX(160.0);
    $pdf->Cell(35,10,'Petugas',1,0,'C',1);

    $pdf->Ln(10);
    $qtyall = "0";
    $nm = 1;
    $x = 1;

    $seldetail = "select *,DATE_FORMAT(tgl_nabung,'%d-%M-%Y') as tgl_nabung1  from trans_tabungan where 
    jemaah_id = '$id' order by seq asc";
    $res1 = mysqli_query($con,$seldetail);

    // echo $seldetail;
    $tot = 0;
    while($da = mysqli_fetch_array($res1)){
        $pdf->SetFont('Times','',7);
        $pdf->Cell(25,8,$da['seq']." dari 40",1,0,'C',1);
        $pdf->SetX(35);
        $pdf->Cell(25,8,number_format( $da['nominal_nabung']),1,0,'C',1);
        $pdf->SetX(60);
        $pdf->Cell(20,8,$da['nominal_nabung'] ? number_format( $da['biaya_admin']) : 0 ,1,0,'C',1);
        $pdf->SetX(80);
        $pdf->Cell(30,8,$da['tgl_nabung1'],1,0,'C',1);
        $pdf->SetX(110.0);
        $pdf->Cell(25,8,$da['status_nabung'],1,0,'C',1);    // $pdf->Cell(10,18,$perkiraan);
        $pdf->SetX(135.0);
       //  $pdf->Cell(25,10,$da['keterangan'],1,0,'C',1);
       $pdf->Cell(25,8,$da['keterangan'],1,0,'C',1);
        $pdf->SetX(160.0);
        $pdf->Cell(35,8,$da['petugas'],1,0,'C',1);
        $pdf->Ln();
        $tot += $da['nominal_nabung'];
    }
    $pdf->Ln(-7);
    $pdf->SetFont('Times','I',9);
    $pdf->Cell(10,18,'Baraka berhak setiap saat melakukan koreksi, apabila terjadi kesalahan laporan ini.');
    $pdf->Ln(13);

    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Down Payment');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,number_format(2000000));

    $pdf->SetX(125);
    $pdf->SetFont('Times','B',10);
    $pdf->Cell(10,18,'Simulasi Perhitungan');
    


    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Total Tabungan');
    $pdf->SetX(40);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,number_format($tot));

    $pdf->SetX(125);
    $pdf->SetTextColor(0,0,255);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'DP');
    $pdf->SetX(155);
    $pdf->Cell(10,18,':');
    $pdf->SetX(165);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,number_format(2000000));
    $pdf->SetTextColor(0,0,0);

    $pdf->Ln(4);
    $pdf->Cell(10,18,'______________________________');

    $pdf->SetTextColor(0,0,255);
    $pdf->SetX(125);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Total Tab (20x)');
    $pdf->SetX(155);
    $pdf->Cell(10,18,':');
    $pdf->SetX(165);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,number_format(13000000));

    $pdf->SetTextColor(0,0,0);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Total Saldo');
    $pdf->Ln(3);
    $pdf->SetFont('Times','B',6);
    $pdf->Cell(10,18,'(DP + Tot. Tab)');
    $pdf->SetX(40);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,number_format(2000000 + $tot));
    
    $pdf->SetX(125);
    $pdf->Cell(10,18,'_____________________________________');
   

    $pdf->Ln(6);
    // $pdf->SetFont('Times','B',9);
    // $pdf->Cell(10,18,'* Total Biaya');
    // $pdf->SetX(40);
    // $pdf->Cell(10,18,':');
    // $pdf->SetX(45);
    // $pdf->SetFont('Times','',9);
    // $pdf->Cell(10,18,"");
    //number_format(28850000)

    $pdf->SetTextColor(0,0,255);
    $pdf->SetX(125);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Total');
    $pdf->SetX(155);
    $pdf->Cell(10,18,':');
    $pdf->SetX(165);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,number_format(15000000));

   

    $pdf->SetTextColor(0,0,0);
    $pdf->Ln(5);
    // $pdf->Cell(10,18,'______________________________');
    $pdf->SetTextColor(0,0,255);
   
    $pdf->SetX(125);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,'* Contoh Total Biaya');
    $pdf->SetX(155);
    $pdf->Cell(10,18,':');
    $pdf->SetX(165);
    // $pdf->SetFont('Times','U',9);
    // $pdf->SetTextColor(0,0,0);
    $pdf->Cell(10,18,number_format(28850000));

    $pdf->SetTextColor(0,0,0);
    $pdf->Ln(5);
    $pdf->SetFont('Times','B',9);
    // $pdf->Cell(10,18,'Selisih');
    $pdf->SetX(40);
    // $pdf->Cell(10,18,':');
    $pdf->SetX(45);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,"");
    //"(".number_format(28850000 - (2000000 + $tot)).")"
    $pdf->SetTextColor(0,0,255);

    $pdf->SetX(125);
    $pdf->SetFont('Times','B',9);
    $pdf->Cell(10,18,'Sisa Pelunasan');
    $pdf->SetX(155);
    $pdf->Cell(10,18,':');
    $pdf->SetX(165);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,number_format(13850000));

    // tambahan
    $tambahan = 0;
    $penyesuaian = 0;
    // if($data['biaya_total'] > 0){
        // $pdf->SetTextColor(0,0,0);
        // $pdf->Ln(8);
        // $pdf->SetFont('Times','B',9);
        // $pdf->Cell(10,18,'Biaya Perjalanan');
        // $pdf->SetX(40);
        // $pdf->Cell(10,18,':');
        // $pdf->SetX(45);
        // $pdf->SetFont('Times','',9);
        // $pdf->Cell(10,18,number_format($data['biaya_total']));

        $selx = "select * from pembayaran_jemaah pj where jemaah_id = '$id' and jenis = 1";
        $cek = mysqli_query($con,$selx);
        if(mysqli_num_rows($cek) > 0){
            $dok = mysqli_fetch_array($cek);
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln(-5);
            $pdf->SetFont('Times','B',9);
            $pdf->Cell(10,18,'Pelunasan /Selisih I');
            $pdf->SetX(40);
            $pdf->Cell(10,18,':');
            $pdf->SetX(45);
            $pdf->SetFont('Times','',9);
            $pdf->Cell(10,18,number_format($dok['nominal']));
            $tambahan = $tambahan + $dok['nominal'];
            $penyesuaian = $penyesuaian + $dok['nominal'];

        }

        $selx = "select * from pembayaran_jemaah pj where jemaah_id = '$id' and jenis = 2";
        $cek = mysqli_query($con,$selx);
        if(mysqli_num_rows($cek) > 0){
            $dok = mysqli_fetch_array($cek);
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln(5);
            $pdf->SetFont('Times','B',9);
            $pdf->Cell(10,18,'Pelunasan /Selisih II');
            $pdf->SetX(40);
            $pdf->Cell(10,18,':');
            $pdf->SetX(45);
            $pdf->SetFont('Times','',9);
            $pdf->Cell(10,18,number_format($dok['nominal']));
            $tambahan = $tambahan + $dok['nominal'];
            $penyesuaian = $penyesuaian + $dok['nominal'];

        }

        // $selx = "select * from pembayaran_jemaah pj where jemaah_id = '$id' and jenis = 3";
        // $cek = mysqli_query($con,$selx);
        // if(mysqli_num_rows($cek) > 0){
        //     $dok = mysqli_fetch_array($cek);
        //     $pdf->SetTextColor(0,0,0);
        //     $pdf->Ln(5);
        //     $pdf->SetFont('Times','B',9);
        //     $pdf->Cell(10,18,'Pelunasan /Selisih III');
        //     $pdf->SetX(40);
        //     $pdf->Cell(10,18,':');
        //     $pdf->SetX(45);
        //     $pdf->SetFont('Times','',9);
        //     $pdf->Cell(10,18,number_format($dok['nominal']));
        // }
    // }   
    
            $pdf->Ln(5);
            $pdf->SetX(10);
            $pdf->SetTextColor(0,0,0);
            if($penyesuaian > 0){
                $pdf->Cell(10,18,'______________________________');
            }

            $selx = "select * from pembayaran_jemaah pj where jemaah_id = '$id' and jenis = 2";
            $cek = mysqli_query($con,$selx);
            $dok = mysqli_fetch_array($cek);
            $pdf->SetTextColor(0,0,0);
            $pdf->Ln(5);
            if($penyesuaian > 0){
                $pdf->SetFont('Times','B',8);
                $pdf->Cell(10,18,'Total Saldo');
                $pdf->Ln(3);
                $pdf->SetFont('Times','B',5);
                $pdf->Cell(10,18,'(DP + Tabungan + Pelunasan/Penyesuaian)');
                $pdf->SetX(40);
                $pdf->Cell(10,18,':');
                $pdf->SetX(45);
                $pdf->SetFont('Times','',9);
                $pdf->Cell(10,18,number_format(2000000 + $tot + $tambahan));
            }
            $biaya_total = 0;
            if($data['biaya_total'] > 0){
                $biaya_total = $data['biaya_total'];

                $pdf->SetTextColor(0,0,0);
                $pdf->Ln(8);
                $pdf->SetFont('Times','B',9);
                $pdf->Cell(10,18,'Tot Biaya Perjalanan');
                $pdf->SetX(40);
                $pdf->Cell(10,18,':');
                $pdf->SetX(45);
                $pdf->SetFont('Times','B',9);
                $pdf->Cell(10,18,number_format($biaya_total));
            }
            $biaya_tambahan = 0;
            $cat = "";

            $selx = "select * from pembayaran_jemaah pj where jemaah_id = '$id' and jenis = 3";
            $cek = mysqli_query($con,$selx);
            if(mysqli_num_rows($cek) > 0){
                $rp = mysqli_fetch_array($cek);
                $biaya_tambahan = $rp['nominal'];
                $cat = $rp['catatan'];

            }
            if($biaya_tambahan > 0){
                $rp = mysqli_fetch_array($cek);
                $pdf->Ln(5);
                $pdf->SetFont('Times','B',9);
                $pdf->Cell(10,18,'Biaya Tambahan');
                $pdf->SetX(40);
                $pdf->Cell(10,18,':');
                $pdf->SetX(45);
                $pdf->SetFont('Times','',9);
                $pdf->Cell(10,18,number_format($biaya_tambahan));
                $pdf->Ln(3);
                $pdf->SetTextColor(255,0,0);
                $pdf->SetFont('Times','B',7);
                $pdf->Cell(10,18,"(".$cat.")");
            }

            if($biaya_tambahan > 0){
                $pdf->SetTextColor(0,0,0);
                $pdf->Ln(5);
                $pdf->Cell(10,18,'______________________________');
                $pdf->Ln(5);
                $pdf->SetFont('Times','B',9);
                $pdf->Cell(10,18,'Total Saldo');
                $pdf->SetX(40);
                $pdf->Cell(10,18,':');
                $pdf->SetX(45);
                $pdf->SetFont('Times','B',9);
                $pdf->Cell(10,18,number_format($biaya_total + $biaya_tambahan));
                $pdf->Ln(3);
                $pdf->SetFont('Times','B',6);
                $pdf->Cell(10,18,'Total saldo (DP + Tabungan + Pelunasan/ Penyesuaian + Biaya Tambahan)');
            }
    

    $pdf->Ln(20);
    $pdf->SetTextColor(0,0,255);
    $pdf->SetTextColor(0,0,0);

    $pdf->Ln(12);
    $pdf->SetTextColor(255,0,0);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,"Informasi & Edukasi : ");
    $pdf->SetTextColor(0,0,0);
    $pdf->Ln(7);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,"1. Biaya Total perjalanan di sesuaikan oleh saat wisatawan/ jamaah akan berangkat ");
    $pdf->Ln(4);
    $pdf->SetFont('Times','',9);
    $pdf->Cell(10,18,'    Bulan / Tahun saat pelunasan tahun berjalan, hal ini di sebabkan penyesuian atas inflasi di tahun tersebut dll.');
    $pdf->Ln(5);
    $pdf->Cell(10,18,'2. Edukasi Faktor yang mempengaruhi  Total Biaya Perjalanan, di antara nya :');
    $pdf->Ln(5);
    $pdf->Cell(10,18,'  a. Saat Bulan keberangkatan (contoh : biaya akhir tahun/Desember tentu lebih tinggi dari pada bulan biasanya atau bulan Ramadhan / masa liburan).');
    $pdf->Ln(5);
    $pdf->Cell(10,18,'  b. Jumlah Wisatawan/ jamaah dalam per Group Keberangkatan (share cosh bus di negara tujuan semakin rendah, atas jumlah jamaah per Bus)');
    $pdf->Ln(5);
    $pdf->Cell(10,18,'  c. Serta hotel/ Jarak hotel dan maskapai yang di pergunakan, serta Durasi keberangkatan dll.');
    $pdf->Ln(5);
    $pdf->Cell(10,18,'  d. Serta masih banyak faktor - faktor yang mempengaruhi suatu biaya perjalanan, terlebih ini perjalanan jarak jauh/ lintas benua');
    $pdf->Ln(5);
    $pdf->Cell(10,18,'  e.	Dimohon setiap wisatawan/ Jamaah memiliki dana saku lebih guna keperluan mendadakan dalam perjalanan nanti nya,');
    $pdf->Ln(5);
    $pdf->Cell(10,18,'       serta mempersiapkan kesehatan dan lain-lain dengan baik.');
    $pdf->Ln(7);
    $pdf->Cell(10,18,'Edukasi ini di sajikan BARAKA, guna membuka wawasan yang lebih luas serta dapat tercapai Pelayanan yang Prima bersama Travel Baraka');

    $pdf->Ln(5);
    $pdf->Cell(10,18,'_______________________________________________________________________________________________________________________________');
    $pdf->Ln(5);
    $pdf->SetFont('Times','',10);
    $pdf->Cell(10,18,"        Pelayanan Baraka bersifat Self service, guna memberikan pelayanan yang terbaik serta aman & nyaman bersama Travel Baraka");
    $pdf->Ln(5);
    $pdf->SetFont('Times','',10);
    $pdf->Cell(10,18,"                           serta Program tabungan di kembangkan Baraka guna membantu wisatawan/ jamaah nantinya ");
    $pdf->Ln(5);
    $pdf->SetFont('Times','',10);
    $pdf->Cell(10,18,"                                                   Guna terciptanya grouping yang terstruktur dimasa akan datang");


    $pdf->Ln(10);
    $cektenor = "select count(*) as jml from trans_tabungan where jemaah_id = '$id' and nominal_nabung is not null";

    $lunas = "";
            // echo $cektenor;
    $restenor = mysqli_query($con,$cektenor);
    $dtr = mysqli_fetch_array($restenor);
    if($dtr['jml'] == "40"){
        $lunas = "L  U  N  A  S";
    }

    

    $pdf->SetFont('Times','I',20);
    $pdf->SetX(80);
    $pdf->Cell(10,18,$lunas);

    $pdf->Ln(10);
    $pdf->SetFont('Times','U',9);
    $pdf->Cell(10,18,'Menabung, suatu konsep agar lebih mudah terjadi keberangkatan profesional, dan memudahkan petugas memberikan pelayanan yang prima');
    $pdf->Ln(5);
    $pdf->SetFont('Times','I',9);
    $pdf->SetX(73);
    $pdf->Cell(10,18,'Menabung langkah awal menuju kota impian');
    $pdf->Ln(5);
    $pdf->SetX(85);
    $pdf->Cell(10,18,'PT. Baraka Insan Mandiri');
    $pdf->Ln(3.5);
    $pdf->SetX(62);
    $pdf->SetTextColor(255,0,0);
    $pdf->Cell(10,18,'Persiapan Pra PPIU menuju PPIU PT. Baraka Insan Mandiri');
    // $cektenor = "select count(*) as jml from trans_tabungan where jemaah_id = '$id' and nominal_nabung is not null";

    // $lunas = "";
    //         // echo $cektenor;
    // $restenor = mysqli_query($con,$cektenor);
    // $dtr = mysqli_fetch_array($restenor);
    // if($dtr['jml'] == "40"){
    //     $lunas = "L  U  N  A  S";
    // }

    // $pdf->SetFont('Times','I',20);
    // $pdf->SetX(80);
    // $pdf->Cell(10,18,$lunas);

    // $pdf->Ln(10);
    // $pdf->SetFont('Times','U',9);
    // $pdf->Cell(10,18,'Menabung, suatu konsep agar lebih mudah terjadi keberangkatan profesional, dan memudahkan petugas memberikan pelayanan yang prima');
    // $pdf->Ln(7);
    // $pdf->SetFont('Times','I',9);
    // $pdf->SetX(73);
    // $pdf->Cell(10,18,'Menabung langkah awal menuju kota impian');
    // $pdf->Ln(5);
    // $pdf->SetX(85);
    // $pdf->Cell(10,18,'PT. Baraka Insan Mandiri');
    // $pdf->Ln(5);
    // $pdf->SetX(62);
    // $pdf->SetTextColor(255,0,0);
    // $pdf->Cell(10,18,'Persiapan Pra PPIU menuju PPIU PT. Baraka Insan Mandiri');


    $pdf->Output();
?>


