<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Calon Wisatawan</h1></br>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Wisatawan</li>
        </ol>
    </div>
    <?php
        $nama_lengkap = "";
        $ktp = "";
        $alamat = "";
        $email = "";
        $hp = "";
        $bank = "";
        $tempat_lahir = "";
        $tgl_lahir = "";
        $ahliwaris = "";
        $hp_ahliwaris = "";
        $hubungan = "";   
        $nama_pasport = "";
        $tipe = "add";
        $label = "Tambahkan";
        $key = "";
        $password = "";
        $npwp = "";
        $kota = "";
    ?>
    <div class="row mb-3">
    <div class="col-xl-12 col-lg-7 mb-4">
        <div class="card">
            <div class="card-header">
                    Form data diri calon wisatawan
                    <div style="float:right">
                       <a href="./?go=listwisatawan">List Wisatawan</a>
                    </div>
            <hr>
            </div>
            <div class="card-body">
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Nomor Resi</label >
                    <input type="text" required  id="resi" class="form-control" value= "<?php echo $hp ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">Unik Key</label>
                    <input type="text" required id="key" class="form-control" value= "<?php echo $email ?>">
                </div>
                <div class="mb-3">
                    <label for="inputPassword5" class="form-label">ID Wisatawan</label>
                    <input type="text" required  id="id_wisatawan" class="form-control" value= "<?php echo $bank ?>">
                </div>
                <Button onclick="show()" class="btn btn-success">Proses</Button>
                <!-- <p onclick="show()">Hide</p> -->
                <p id="notice" style="display:none; color: red">Data Yang anda input tidak dikenali...</p>
            </div>
            <div class="card-body" id="formInput" style = "display:none">
            <Form method="POST" action="pages/wisatawan/action_wisatawan.php" enctype="multipart/form-data">
                    <input type="hidden"  name="tp" value="add" class="form-control">
                    <input type="hidden" id="jamaah_id"  name="jamaah_id" class="form-control">
                    <div class="overflow-auto" style="height:700px">
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nama Lengkap (Wajib Sesuai KTP)</label>
                        <input type="text" required  name="nama" class="form-control" value= "<?php echo $nama_lengkap ?>" >
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nama Jamaah (3 Suku Nama)</label>
                        <input type="text" required  name="nama_pasport" class="form-control" value= "<?php echo $nama_pasport ?>" >
                        <p class="text-danger">* Tidak termasuk gelar, nama disingkat. 3 suku nama diambil dari nama ayah dan kakek (Wajib sesuai/sama dengan nama di Passport)</p>
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">NIK KTP</label>
                        <input type="number" required  name="ktp" class="form-control" value= "<?php echo $tempat_lahir ?>">
                    </div>
                    <!-- <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Nomor Hp</label>
                        <input type="text" required  name="hp_wisatawan" class="form-control" value= "<?php echo $hp ?>">
                    </div> -->
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Tempat Lahir</label>
                        <input type="text" required  name="tempat_lahir" class="form-control" value= "<?php echo $ktp ?>">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Jenis Kelamin</label>
                        <select required class="form-control" name="jk">
                            <option value=""></option>
                            <option value="Pria">Pria</option>
                            <option value="Wanita">Wanita</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Tgl Lahir</label>
                        <input type="date" required  name="tgl_lahir" class="form-control" value= "<?php echo $tgl_lahir ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Alamat</label>
                        <input type="text" required  name="alamat" class="form-control" value= "<?php echo $alamat ?>">
                    </div>
                    <div class="mb-3">
                        <Row class="col-md-5">
                            <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="inputPassword5" class="form-label">Rt</label>
                                    <input type="text" required  name="rt" class="form-control" value= "<?php echo $alamat ?>">
                                </div>
                                <div class="col-sm-6">
                                    <label for="inputPassword5" class="form-label">Rw.  </label>
                                    <input type="text" required  name="rw" class="form-control" value= "<?php echo $alamat ?>">
                                </div>
                            </div>
                            </div>
                        </Row>
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Kelurahan/Desa</label >
                        <input type="text" required  name="kabupaten" class="form-control" value= "<?php echo $hp ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Kecamatan</label >
                        <input type="text" required  name="kecamatan" class="form-control" value= "<?php echo $hp ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Kota</label >
                        <input type="text" required  name="kota" class="form-control" value= "<?php echo $kota ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Provinsi</label >
                        <input type="text" required  name="provinsi" class="form-control" value= "<?php echo $hp ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Kode Pos</label >
                        <input type="text" required  name="kodepos" class="form-control" value= "<?php echo $hp ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">No Hp Wisatawan</label >
                        <input type="text" required  name="hp" class="form-control" value= "<?php echo $hp ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Email</label>
                        <input type="text"  name="email" class="form-control" value= "<?php echo $email ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">NPWP</label>
                        <input type="text" name="npwp" class="form-control" value= "<?php echo $npwp ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Embarkasi</label>
                        <input type="text" readonly name="embarkasi" class="form-control" value= "CGK Bandara Soekarno Hatta">
                    </div>
                    <!-- <div class="mb-3">
                        <label for="inputPassword5" class="form-label">No ID VA</label>
                        <input type="text" placeholder = "Diisi Admin PT.Baraka" disabled  name="va" class="form-control">
                    </div> -->
                    <hr>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Pekerjaan</label>
                        <select class="form-control" name="pekerjaan">
                            <option value=""></option>
                            <option value="Wiraswasta">Wiraswasta</option>
                            <option value="PNS">PNS</option>
                            <option value="Polri">Polri</option>
                            <option value="Karyawan Swasta">Karyawan Swasta</option>
                            <option value="Ibu Rumah Tangga">Ibu Rumah Tangga</option>
                            <option value="Pelajar">Pelajar</option>
                            <option value="Mahasiswa">Mahasiswa</option>
                            <option value="lain-lain">Lain-lain</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Status</label>
                        <select class="form-control" name="status">
                            <option value=""></option>
                            <option value="Menikah">Menikah</option>
                            <option value="Lajang">Lajang</option>
                            <option value="Duda">Duda</option>
                            <option value="Janda">Janda</option>
                            <option value="Pelajar">Pelajar</option>
                        </select>
                    </div>
                    <hr>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Ahli Waris</label>
                        <input type="text"  name="ahliwaris" class="form-control" value= "<?php echo $ahliwaris ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">No Hp Ahli Waris</label>
                        <input type="text"  name="hp_ahliwaris" class="form-control" value= "<?php echo $hp_ahliwaris ?>">
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label">Hubungan</label>
                        <select required class="form-control" name="hubungan">
                            <option value=""></option>
                            <option value="Suami">Suami</option>
                            <option value="Istri">Istri</option>
                            <option value="Anak">Anak</option>
                            <option value="Orang Tua">Orang Tua</option>
                            <option value="Saudara Kandung">Saudara Kandung</option>
                            <option value="Sepupu">Sepupu</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label" style="color:blue">Masukkan foto New Wisatawan bersama formulir</label>
                        <input type="file" name="foto" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="inputPassword5" class="form-label" style="color:blue">Dengan ini, saya menyatakan bahwa data yang di isi adalah benar dan sesuai dengan data new Wisatawan,<br>
                         dan saya bertanggung jawab jika ada kesalahan yang terdapat pada isi form ini</label>
                    </div>
                    <Button class="btn btn-success">
                            Proses
                    </Button>
            </form>   
            </div>
            </div>
            </div>
        </div>
    </div>
</div>
<script>
    function show(){
        var key = document.getElementById("key").value;
        var resi = document.getElementById("resi").value;
        var id = document.getElementById("id_wisatawan").value;
        // console.log(key);
        $.ajax({
            type:'POST',
            url:'pages/wisatawan/action_wisatawan.php',
            data:'key='+key+"&resi="+resi+"&id_jemaah="+id,
            success:function(data){
                console.log(data);
                // return
                // $('#batch').html(html);
                // $('#rak').html('<option value="">Select state first</option>'); 
                if(data == "0"){
                    // var res = "asd#123".split("#");
                    // res = res[1];
                    document.getElementById("jamaah_id").value = "";
                    document.getElementById('formInput').style.display = 'none';
                    document.getElementById('notice').style.display = 'block';
                }else if(data == "X"){
                    document.getElementById('notice').style.display = 'block';
                    document.getElementById('notice').innerHTML = 'Masukan resi secara berurutan....';
                
                }else{
                    var res = data.split("#");
                    res = res[1];
                    console.log('oioioio...',data)
                    document.getElementById('jamaah_id').value = res;
                    document.getElementById('formInput').style.display = 'block';
                    document.getElementById('notice').style.display = 'none';
                }
            }
        }); 
        // alert("xx")
    }
</script>