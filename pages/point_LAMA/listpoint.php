<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">List Point</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">List Point</li>
        </ol>
    </div>

<?php
    $filterTabahan = "";
    $filter_id = "";
    if($_SESSION['role'] == "Agent"){
        $id = $_SESSION['m_agent_id'];
        $filterTabahan = $filterTabahan." and a.m_agent_id = '$id' "; 
        $filter_id = " and a.m_agent_id = '$id' "; 
    }
    
    if(isset($_POST['lihat'])){
        if($_POST['randcheck']==$_SESSION['rand']){
            $date1         = $_POST['date1'];
            $status        = $_POST['status'];
        //    echo "dddd";
            if(strlen($date1) > 2){
                $filterTabahan = $filterTabahan." and DATE_FORMAT(cut_off, '%Y-%m-%d') = '$date1' ";
            }

            if(strlen($status) > 2){
                $filterTabahan = $filterTabahan." and kode_status = '$status' ";
            }

            // echo "xxx";
        }
    }else{
        // echo "adsada";
    }
?>
<div class="card" style="margin-bottom:20px;margin-top:-25px">
    <div class="card-body">
        <form action="" method="POST">
        <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
            ?> 
            <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" /> 
            <div class="container col-12" style="margin-top:0px">
            <div class="row" style="margin-bottom:20px">
                <div class="col-sm">
                    <label>Periode CutOff</label>
                    <input type="date" class="form-control" name="date1"/>
                </div>
                <div class="col-sm">
                    <label>Status</label>
                    <select class="form-control" name="status">
                        <option value=""></option>
                        <option value="WT1">On Going</option>
                        <option value="WT2">On Proses</option>
                        <option value="WT3">Payment</option>
                    </select>
                </div>
                <div class="col-sm" style="margin-top:32px">
                    <Button class="btn btn-success" name="lihat" >Lihat</Button>
                    <Button class="btn btn-danger" name="export">Export</Button>
                </div>
            </div>
            </div>
        </form>
        <?php
                     if($_SESSION['role'] != "Agent"){
                ?>
                        <div class="col-sm" style="margin-top:-53px">
                         <a href="./?go=listbonus"><Button class="btn btn-success" name="export" style="float:right">Laporan Bonus</Button></a>
                        </div>
                <?php
                    }
        ?>
        <div class="table-responsive p-3">
            <table class="table align-items-center table-flush table-hover" id="dataTableHover">
            <thead class="thead-light">
                <tr>
                <th>ID Agent</th>
                <th>Nama Agent</th>
                <th>Bank</th>
                <th>No Rekening</th>
                <th>Point</th>
                <th>Nominal (nett)</th>
                <th>Mudorobah (10%)</th>
                <th>Tgl CutOff</th>
                <th>Tgl Transfer</th>
                <th>Status</th>
                <th>Action</th>
                </tr>
            </thead>
            <?php
                $sel = "select a.m_agent_id,b.nomor_agent,b.nama,bank,nomor_rekening,sum(point_count) as point,DATE_FORMAT(cut_off,'%d-%M-%Y') cut_off, sum(point_amount) as amount,sum(point_pajak) as ppn
                ,'' as cutoff,'',case when a.status = 'Point' then 'On Going' else status end as stat,tgl_pencairan,a.kode_status ,
                kode_status as kode from transaksi_point a
                inner join m_agent b on a.m_agent_id = b.m_agent_id where 1=1 and is_claim = 'Y' $filterTabahan
                group by a.m_agent_id,b.nomor_agent,b.nama,bank,nomor_rekening,cut_off,tgl_pencairan,kode_status";

                // echo $sel;
                $result = mysqli_query($con,$sel);
                while($res = mysqli_fetch_array($result)){
                $style = "";
                if($res['stat'] == "On Proses"){
                    $style = "style='background-color:#3ab449;color:white'";
                }else if($res['stat'] == "Payment"){
                    $style = "style='background-color:#18BC9C;color:white'";
                }
            ?>
                <tr <?php echo $style; ?>>
                    <td><?php echo $res['nomor_agent']; ?></td>
                    <td><?php echo $res['nama']; ?></td>
                    <td><?php echo $res['bank']; ?></td>
                    <td><?php echo $res['nomor_rekening']; ?></td>  
                    <td><?php echo $res['point']; ?></td>
                    <td><?php echo number_format($res['amount']); ?></td>
                    <td><?php echo number_format($res['ppn']); ?></td>
                    <td><?php echo $res['cut_off']; ?></td>
                    <td>
                        <?php echo $res['tgl_pencairan']; ?>
                
                    </td>
                    <td><?php echo $res['stat']; ?></td>
                    <td style="text-align:center">
                    <?php
                       if($_SESSION['role'] != "Agent"){
                    ?>
                        <a href="./?go=point_detail&id=<?php echo $res['m_agent_id']; ?>"><button class="btn btn-danger">Proses</button></a>
                    <?php
                        }
                    ?>
                        <a href="./?go=listpoint_detail&id=<?php echo $res['m_agent_id']; ?>&cut_off=<?php echo $res['cut_off']; ?>&kode=<?php echo $res['kode'] ?>"><button class="btn btn-warning">Rincian</button></a>
                    </td>
                </tr>
            <?php
                }
            ?>
            </table>
        </div>
    </div>
</div>
</div>
<?php
        if(isset($_POST['export'])){
            // if($_POST['randcheck']==$_SESSION['rand']){
                $date1         = $_POST['date1'];
                $status        = $_POST['status'];

                if($_SESSION['role'] == "Agent"){
                    $id = $_SESSION['m_agent_id'];
                    // $filterTabahan = $filterTabahan." and a.m_agent_id = '$id' "; 
                    // $filter_id = " and a.m_agent_id = '$id' "; 
                }

                if(strlen($date1) > 2){
                    $filterTabahan = $filterTabahan." and DATE_FORMAT(cut_off, '%Y-%m-%d') = '$date1' ";
                }
    
                if(strlen($status) > 2){
                    $filterTabahan = $filterTabahan." and kode_status = '$status' ";
                }

                $id = str_replace("'","@",$filterTabahan);
                // echo $id;
                echo "<script>window.location = './pages/point/export_point.php?q=$id'</script>";
            // }
        }
    ?>