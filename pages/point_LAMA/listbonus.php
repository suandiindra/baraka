<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">List Bonus</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">List Bonus</li>
        </ol>
    </div>

<?php
    $filterTabahan = "";
    $filter_id = "";
    if($_SESSION['role'] == "Agent"){
        $id = $_SESSION['m_agent_id'];
        $filterTabahan = $filterTabahan." and a.m_agent_id = '$id' "; 
        $filter_id = " and a.m_agent_id = '$id' "; 
    }
    if(isset($_GET['key'])){
        if( $_GET['key'] == "q345ewq"){
            $filterTabahan = " and tgl_pencairan_bonus is null ";
        }else if($_GET['key'] == "px81M"){
            $filterTabahan = " and tgl_pencairan_bonus is not null ";
        }else{

        }
    }
    
    if(isset($_POST['lihat'])){
        if($_POST['randcheck']==$_SESSION['rand']){
            $status        = $_POST['status'];
            if($status == "Payment"){
                $filterTabahan = " and tgl_pencairan_bonus is not null ";
            }else if($status == "Belum Payment"){
                $filterTabahan = " and tgl_pencairan_bonus is null ";
            }
        }
    }else{
        // echo "adsada";
    }
?>
<div class="card" style="margin-bottom:20px;margin-top:-25px">
    <div class="card-body">
        <form action="./?go=listbonus" method="POST">
        <?php
                    $rand=rand();
                    $_SESSION['rand']=$rand;
            ?> 
            <input type="hidden" value="<?php echo $rand; ?>" name="randcheck" /> 
            <div class="container col-12" style="margin-top:0px">
            <div class="row" style="margin-bottom:20px">
                <div class="col-sm">
                    <label>Status</label>
                    <select class="form-control" name="status">
                        <option value=""></option>
                        <option value="Payment">Payment</option>
                        <option value="Belum Payment">Belum Payment</option>
                    </select>
                </div>
                <div class="col-sm" style="margin-top:32px">
                    <Button class="btn btn-success" name="lihat" >Lihat</Button>
                <?php
                    if($_SESSION['role'] != "Agent"){
                ?>
                    <Button class="btn btn-danger" name="export">Export</Button>
                <?php
                    }
                ?>
                </div>
            </div>
            </div>
        </form>
        <div class="table-responsive p-3">
            <table class="table align-items-center table-flush table-hover" id="dataTableHover">
            <thead class="thead-light">
                <tr>
                <th>No</th>
                <th>ID Jamaah</th>
                <th>Nama Jamaah</th>
                <th>ID Agent</th>
                <th>Nama Agent</th>
                <th>Bank</th>
                <th>No Rekening</th>
                <th>Tgl Tiba</th>
                <th>Tgl Transfer</th>
                <th>Status</th>
                <th>Action</th>
                </tr>
            </thead>
            <?php
                $sel = "select
                nomor_jemaah,nama_jemaah,bank,nomor_rekening,tgl_tiba,tgl_pencairan_bonus,c.nomor_agent,c.nama,jemaah_id
                from transaksi_point a
                inner join jemaah b on a.jamaah_id = b.jemaah_id
                inner join m_agent c on c.m_agent_id = b.m_agent_id
                where 1 = 1 and tgl_tiba is not null and is_claim <> 'X' $filterTabahan $filter_id";
                $i = 1;
                // echo $sel;
                $result = mysqli_query($con,$sel);
                while($res = mysqli_fetch_array($result)){
                $style = "#ffff";
               
                $stat = "Belum Payment";
                if(strlen($res['tgl_pencairan_bonus']) > 3){
                    $stat = "Payment";
                    $style = "#48A868; color:white";
                }
            ?>
                <tr style="background-color:<?php echo $style; ?>">
                    <td><?php echo $i ?></td>
                    <td><?php echo $res['nomor_jemaah']; ?></td>
                    <td><?php echo $res['nama_jemaah']; ?></td>
                    <td><?php echo $res['nomor_agent']; ?></td>
                    <td><?php echo $res['nama']; ?></td>
                    <td><?php echo $res['bank']; ?></td>
                    <td><?php echo $res['nomor_rekening']; ?></td>  
                    <td><?php echo $res['tgl_tiba']; ?></td>
                    <td><?php echo $res['tgl_pencairan_bonus']; ?></td>
                    <td><?php echo $stat; ?></td>
                    <td style="text-align:center">
                    <?php
                       if($_SESSION['role'] != "Agent" && strlen($res['tgl_pencairan_bonus']) < 3){
                    ?>
                        <a href="./?go=detail_wisatawan&id=<?php echo $res['jemaah_id']; ?>&trans=0"><button class="btn btn-danger">Proses</button></a>
                    <?php
                        }
                    ?>
                    </td>
                </tr>
            <?php
                $i += 1;
                }
            ?>
            </table>
        </div>
    </div>
</div>
</div>
<?php
        if(isset($_POST['export'])){
            
            // if($_POST['randcheck']==$_SESSION['rand']){
                $status        = $_POST['status'];

                if($_SESSION['role'] == "Agent"){
                    $id = $_SESSION['m_agent_id'];
                }

                if(isset($_GET['key'])){
                    if( $_GET['key'] == "q345ewq"){
                        $filterTabahan = " and tgl_pencairan_bonus is null ";
                    }else if($_GET['key'] == "px81M"){
                        $filterTabahan = " and tgl_pencairan_bonus is not null ";
                    }else{
            
                    }
                }

                if(isset($_POST['status'])){
                    if($status == "Payment"){
                        $filterTabahan = " and tgl_pencairan_bonus is not null ";
                    }else if($status == "Belum Payment"){
                        $filterTabahan = " and tgl_pencairan_bonus is null ";
                    }
                }

                $id = str_replace("'","@",$filterTabahan);
                // echo $filterTabahan;

                echo "<script>window.location = './pages/point/export_bonus.php?q=$id'</script>";
            // }
        }
    ?>