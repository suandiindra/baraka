<div class="container-fluid" id="container-wrapper" style="margin-top:-20px">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Point & Bonus</li>
        </ol>
    </div>
    <!-- <div>
    <div class="card h-100" style="margin-bottom:15px">
        <div class="card-body">
            Selamat Datang kembali <b>Sdr. <?php echo $_nama; ?></b>
            <div style="float:right; color:green">   
                <div id="txt"></div>
            </div>
        </div>
    </div>
    </div> -->
    <?php
        $filterid = " and a.m_agent_id = '$_user'";
        $sel = "select sum(point_count) as point,sum(point_amount) as amount from transaksi_point a where 1 = 1 $filterid and kode_status = 'WT1' and is_claim = 'Y' and tgl_pencairan is not null";
        $sel2 = "select sum(point_count) as point,sum(point_amount) as amount from transaksi_point a where 1 = 1 $filterid and kode_status = 'WT1' and is_claim <> 'X'";
        $sel3 = "select count(*)*100000 as jml from transaksi_point a
        inner join jemaah b on a.jamaah_id = b.jemaah_id
        where 1 = 1 $filterid and tgl_tiba is not null and is_claim <> 'X' and tgl_pencairan_bonus is null";

        $sel4 = "select sum(point_count) as point,sum(point_amount) as amount from transaksi_point a where 1 = 1 $filterid and kode_status = 'WT3' and is_claim = 'Y' and tgl_pencairan is not null";
        $sel5 = "select count(*)*100000 as jml from transaksi_point a
        inner join jemaah b on a.jamaah_id = b.jemaah_id
        where 1 = 1 $filterid and tgl_tiba is not null and is_claim <> 'X' and tgl_pencairan_bonus is not null";
        // echo $sel5;
        $res = mysqli_fetch_array(mysqli_query($con,$sel));
        $res2 = mysqli_fetch_array(mysqli_query($con,$sel2));
        $res3 = mysqli_fetch_array(mysqli_query($con,$sel3));
        $res4 = mysqli_fetch_array(mysqli_query($con,$sel4));
        $res5 = mysqli_fetch_array(mysqli_query($con,$sel5));
    ?>
    <a href="./?go=listpoint">History Pencairan Ujroh & Bonus</a>
    <div class="row mb-3"  style="margin-top:20px">
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card h-100">
                <div class="card-body">
                    <div class="row align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1">Jumlah Point</div>
                        <div class="h5 mb-0 font-weight-bold" style="color:red"><?php echo floor($res2['point']/2) ?></div>
                        <div class="mt-2 mb-0 text-muted text-xs">
                        <span class="text-success mr-2"> Setiap 2 calon jamaah adalah 1 point</span>
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-bookmark fa-2x text-primary"></i>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card h-100">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-uppercase mb-1">Nominal Ujroh</div>
                    <div class="h5 mb-0 font-weight-bold" style="color:red"><?php echo "Rp. ".number_format($res['amount']) ?></div>
                    <div class="mt-2 mb-0 text-muted text-xs">
                    <span class="text-success mr-2">Nominal Ponit Nett di potong 10%</span>
                    <!-- <span>Since last years</span> -->
                    </div>
                </div>
                <div class="col-auto">
                    <i class="fas fa-suitcase fa-2x text-success"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card h-100">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-uppercase mb-1">Bonus</div>
                    <div class="h5 mb-0 mr-3 font-weight-bold" style="color:red"><?php echo "Rp. ".number_format($res3['jml']) ?></div>
                    <div class="mt-2 mb-0 text-muted text-xs">
                    <span class="text-success mr-2"><i class="fas fa-arrow-up"></i> Diberikan pada agent saat jamaah kembali ke tanah air</span>
                        <!-- <span>Since last month</span> -->
                    </div>
                </div>
                <div class="col-auto">
                  <a href="./?go=listbonus&key=q345ewq"><button class="btn btn-danger">List Detail</button></a>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="row mb-3"  style="margin-top:20px" >
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card h-100" style="background-color:#395697">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:white">TOTAL PENDAPATAN UJROH</div>
                    <div class="h5 mb-0 mr-3 font-weight-bold" style="color:yellow"><?php echo "Rp. ".number_format($res4['amount']) ?></div>
                    <div class="mt-2 mb-0 text-muted text-xs">
                    </div>
                </div>
                <div class="col-auto" style="color:white">
                    <i class="fas fa-gift fa-2x"></i>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card h-100" style="background-color:#5EBA7D">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:white">TOTAL PENDAPATAN BONUS</div>
                    <div class="h5 mb-0 mr-3 font-weight-bold" style="color:yellow"><?php echo "Rp. ".number_format($res5['jml']) ?></div>
                    <div class="mt-2 mb-0 text-muted text-xs">
                    </div>
                </div>
                <div class="col-auto">
                  <a href="./?go=listbonus&key=px81M"><button class="btn btn-danger">List Detail</button></a>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
