<?php
    session_start();
    $_user = $_SESSION["m_agent_id"];
    include("../../utility/config.php");
    include("../../utility/fungsi.php");
    $where = str_replace("@","'",$_GET['q']);

    // echo $where;
    
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Data Point.xls");
    $mydate=getdate(date("U"));
    $tgl = "$mydate[weekday], $mydate[month] $mydate[mday], $mydate[year]";
?>
<table>
    <tr>
        <td><h3><b>PT. BARAKA INSAN MANDIRI</b></h3></td>
    </tr>
    <tr>
        <td><b>Laporan Data Calon Jamaah</b></td>
    </tr>
</table>
<table border=1>
<thead class="thead-light">
    <tr>
        <th>No</th>
        <th>ID Agent</th>
        <th>Nama Agent</th>
        <th>Bank</th>
        <th>No Rekening</th>
        <th>Nominal</th>
    </tr>
</thead>
<tbody>
    <?php 
        
        $sel = "select
        c.nomor_agent,nama,bank,nomor_rekening,count(*)*100000 as tot
        from transaksi_point a
        inner join jemaah b on a.jamaah_id = b.jemaah_id
        inner join m_agent c on c.m_agent_id = b.m_agent_id
        where 1 = 1 and tgl_tiba is not null and is_claim <> 'X' $where
        group by c.nomor_agent,nama,bank,nomor_rekening";

        // echo $sel;
        $result = mysqli_query($con,$sel);
        $i = 1;
        while($res = mysqli_fetch_array($result)){
    ?>
    <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $res['nomor_agent']; ?></td>
        <td><?php echo $res['nama']; ?></td>
        <td><?php echo $res['bank']; ?></td>
        <td><?php echo "'".$res['nomor_rekening']; ?></td>  
        <td><?php echo $res['tot']; ?></td>
    </tr>
    <?php
        $i =  $i + 1;
        }
        
    ?>
</tbody>
</table>
<?php
    echo "<script>window.location='../../?go=listbonus'</script>";
?>
